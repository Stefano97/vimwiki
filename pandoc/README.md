# PDF conversion

This folder contains all the yaml files guiding [pandoc](https://pandoc.org/)
conversions for the different courses.  When a particular lecture has been
added, be sure to add the path pointing to markdown file in the scope called
"input-files" and the path pointing to the folder in which the markdown file is
contained in the scope "resource-paths". In this way pandoc finds both the
source files and the images automatically.

You can add your name to the scope "author" as well if you want to get credits.

Conversion is a one line command and has to be run in the root of the
repository.

```{shell}
pandoc -d pandoc/yaml_file.yaml
```

Take a look at the content of this folder. You can feed the files called like
the Professor or Class you're interested in.

## Example

I want to have the PDFs of Cerutti's Class:

```{shell}
pandoc -d pandoc/cerutti
```

or, equivalently,

```{shell}
pandoc -d pandoc/cerutti.yaml
```

To compile every Class in the folder:

```{shell}
pandoc -d pandoc/*
```

All the outputs are gathered into the root of the repository in PDF files.
