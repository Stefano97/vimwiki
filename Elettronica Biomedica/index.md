# Elettronica biomedica

| Lezione                 | Argomento | Sbobinata | Difficoltà | Slides comprensibili |
| ---                     | ---       | ---       | ---        | ---                  |
| [01](01/01.md)          |           |           |            |                      |
| [02](02/02.md)          |           |           |            |                      |
| [03](03/03.md)          |           |           |            |                      |

[Ripasso C](Elettronica/ripasso_c)
