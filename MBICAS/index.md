# Computer Aided Surgery

| Lecture               | Topic        | Revised | Difficulty | Comprehensible Handouts | References | Days  |
| :---                  | :---         | :---    | :---       | :---                    | :---       | :---  |
| [01](Baroni/01/01.md) | Introduction | Yes     | Normal     |                         | 01         | 08.04 |
| [02](Baroni/02/02.md) |              |         |            |                         |            | 12.04 |


| Day   | Link                                                                                                |
| :---  | :---                                                                                                |
| 08.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=1dcb09b6333c423a8791fe80201937a4 |
| 12.04 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=36957ed0ef3841258fb5df37c85cdb31 |
# Esamination procedure

Oral exam.

# Methods for Biomedical Imaging


| Lecture                       | Topic                                 | Revised   | Difficulty | Comprehensible Handouts | References     | Days                      |
| :---                          | :---                                  | :---      | :---       | :---                    | :---           | :---                      |
| [01](Baselli/01/Lecture.md)   | Multi-Dimensional FT                  | Yes       | Normal     | ?                       | 0, 1           | 22.02                     |
| [02](Baselli/02/Lecture.md)   | Filtered Backprojection               | Yes       | Hard       | Yes                     | 2              | 23.02                     |
| [03](Baselli/03/Lecture.md)   | Reconstruction from projections       | No        |            | ?                       | 3              | 25.02                     |
| [04](Baselli/04/Lecture.md)   | Numerical methods (ART and OSEM)      | No        |            | ?                       | 4              | 01.03                     |
| [05](Baselli/05/Lecture.md)   | Analytical reconstruction methods     | No        |            | ?                       | 5              | 02.03                     |
| [06](Baselli/06/Lecture.md)   | MRI Scanners                          | Sbobinata | Hard       | ?                       | 6, 2 Video MRI | 04.03                     |
| [07](Baselli/07/Lecture.md)   | MRI Physics                           | No        |            | ?                       | 7              | 08.03                     |
| [08](Baselli/08/Lecture.md)   | MRI Bloch equation for NMR            | No        | Hard       | ?                       | 8              | 08.03 (22:11 - end)       |
| [09](Baselli/09/Lecture.md)   | MRI Bloch equation for RF excitation  | No        |            | ?                       | 9              | 09.03                     |
| [10](Baselli/10/Lecture.md)   | MRI signal detection and demodulation | No        |            | ?                       | 10             | 09.03                     |
| [11](Baselli/11/Lecture.md)   | MRI characteristics                   | No        | Very Hard  | ?                       | 11             | 15.03                     |
| [13](Baselli/13/Lecture.md)   | MRI contrast                          | No        | Very Hard  | ?                       | 13             | 15.03                     |
| [12](Baselli/12/Lecture.md)   | MRI signal localization (1)           | No        | Very Hard  | ?                       | 12             | 16.03                     |
|                               | MRI signal localization (2)           | No        | Very Hard  | ?                       | 12             | 18.03 (start - 40:26)     |
| [14](Baselli/14/Lecture.md)   | MRI Resolution, SNR and artifacts     | No        | Hard       | ?                       | 14             | 18.03 (40:26 - end)       |
| [15](Baselli/15/Lecture.md)   | MRS and Chemical shift imaging        | No        |            |                         | 15             | 22.03                     |
| [16](Baselli/16/Lecture.md)   | MRA: Angiography                      | No        | Hard       | ?                       | 16             | 23.03                     |
| [17](Baselli/17/Lecture.md)   | fMRI: functional MRI (1)              | Yes       | Hard       | No                      | 17             | 25.03                     |
|                               | fMRI: functional MRI (2)              | Yes       |            | No                      | 17             |                           |
|                               | fMRI: functional MRI (3)              | Yes       | Very Hard  | No                      | 17 (15-end)    | 29.03                     |
| [18](Baselli/18/Lecture.md)   | DWI and DTI (1)                       | No        | Very Hard  |                         |                | 29.03                     |
|                               | DWI and DTI (2)                       | No        | Very Hard  |                         |                | 30.03 (missing recording) |
| [19](Baselli/19/Lecture.md)   | Image registration                    | No        | Very Hard  |                         |                | 30.03                     |


| Day   | Link                                                                                                |
| :---  | :---                                                                                                |
| 22.02 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=208a17dfd66b477c81992176cf3ef446 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=24f15f38e6f14fea85a17f3ce83821fe |
| 23.02 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=e1f8d5ea94684ea8ba7becbd6ae7b1e1 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=f8b25759df74447299802ab5408af09b |
| 25.02 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=68515bb04c9d4b01aa0f3887a011f17c |
| 01.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=c564c6a9270842998464dbbe50bbfe56 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=69665ef43ab4445b83bbda83350ecb7d |
| 02.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=9b7c0400912944a5bfdd74cbc1f975a9 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=a21a96a843fc4039adc7cabf6f056e3b |
| 04.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=2ed844651de043a1b8119626e8b04236 |
| 08.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=1ba28e9369fc48fda2ef4c353a049749 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=6a44708f4f4542c6b86a6f4459c2286b |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=21d5facf285c427b9e98be09c128dfd5 |
| 09.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=72873da84aaa49dba15fc9cd70d4fe4e |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=76c23221323641ffab3ee208cb5c3c3e |
| 15.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=16c22c8d688848069c36d96c839aa5eb |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=bb66c63d51c84d3a918569d8556d2090 |
| 16.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=4e07792ade4d41139d0f3956074158c1 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=aba1d8727bc94b2f9e2cc8f06fb9447a |
| 18.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=80148f5ea76545ada269cf2173752ce7 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=ada15b3818464ac0bacfa7be8b76e317 |
| 22.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=186adb474b8c460f9366688ef2d89512 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=9b100bd2bbd24ea0807803780a4baa2b |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=4e5f03f24bbc40a09cd1f6613911b7b6 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=8f8f871069e34e4787fcccbe50352987 |
| 23.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=474924895dfc48739b01deeef3258aa6 |
|       | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=823b35b563ae4681bc6f979ba248517b |
| 25.03 | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=dbd1ebadbc5a4e1bb3eb6d1777c5796c |
| 29.03 |                                                                                                     |
| 30.03 |                                                                                                     |
| 11.03 |                                                                                                     |


## Examination procedure

Oral exam.
