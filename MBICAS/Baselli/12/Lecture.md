# Lecture 12


## Part 2

### Visual demo of phase encoding

we can look at the belt as if it's the spin system on one axis. the rotating at
the larmor frequency. in the rotating frame the belt is like it's not moving.
what happens if i switch on the gradient on the z axis? i am subtracting one
part on the left hand and adding on the other hand. left hand spins are slowing
down while right hand are sped up. so we are creating a phase differences that
are proportional to time. that's why the position in the ???. if i switch off i
will keep the phase shifted like freezed. this phase encoding is related to
position even if it was worked in the time domain.

1. switch on at a given time and get the first armonic
2. double the time and get the second harmonic and so on

with this trick i can match the basis function with the harmonics i want. i can
go on with the resolution as much as i want but there's a physical limit. on
small sample we could go on to have finer details. the physical limit --> the
thermal diffusion is working and i will have a mixing of phases caused by
brownian motions of the particles.


in the varying length pendulum: small length --> high larmor frequency.  big
length --> low larmor frequency.


the clinical limit of resolution: there's a tradeoff between resolution and
SNR. we are  limiting the number of harmonics to the resolution that is really
needed to keep a sufficient SNR.
