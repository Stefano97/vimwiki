# Lecture 3

## Slide 1

Let's recall the common filtered back projection. Let's see that there were
several assumption in FBP:

1. Working on slices in 2D images xy which is the most natural way in, for
   instance, the CT scanners but nowadays we have a lot of opportunities to
   have good 3D reconstructions and an extension to the volume is really
   important.
2. Idealization of the problem: considering rays as if they had no dimension.
   Actually in practice we talk about "thick rays" at best, so cylinders or
   tubes as called in our references. Ideally we have uniform tubes but
   conversely the resolution and contrast and attenuation of x-rays especially
   in emission tomography are strongly dependent from the distance between the
   emitting point and the detector. We can improve our reconstruction
   considering the sensitivity functions.
3. A continuous collection of data for practically all the possible values of
   theta and all translations t which obviously is not possible. But anyway
   it's not difficult to discretize the problem after having the analytical
   solution and so applying the numerical methods on the workstation of the
   scanner. The same can be said for the planar space which analytically is
   continuous with $`dx`$ and $`dy`$, conversely we have pixels with dimensions
   $`\Delta_x`$ and $`\Delta_y`$.
4. This 2D approach is normally giving a distance from slice to slice called
   $`Delta_z`$ which is much greater than the resolution which we have in the
   plane which is actually imposed by the Delta of the translation $`Delta_t`$.

We can deal with the discretization of the problem. This discretization in a
two-dimensional grid is not that trivial since we deal with orthogonal grid of
pixels while, conversely, we are using thick rays which generally have an
inclination $`\theta`$ and we have to do a lot of work to compute these
coefficients $`a_{ij}`$ and this problem is much the same we will see in
numerical methods and in discretized application of analytical methods like
FBP. Actually to compute those $`a_{ij}`$ the concepts are the same as those
that are applied in computer graphics where you have objects which are made of
voxels but the scene can be watched by several directions as the objects are
moving virtually inside the computer and we have to represent this movement on
a 2D screen and the basic approach in computer graphics are those of:

1. voxel-driven methods: starting from each voxels and we see how it is
   projected onto a flat screen.
2. Ray-driven: we start from one ray which must be represented on the screen
   and so we stick to the grid of the screen and we back project the ray and
   see how it crosses the voxel.

In these numerical problems we do have the classical ray-driven and
voxel-driven approaches (in case we have to write some software about
projections and back projections).

In this discrete context the integrals we had we dealt with in the analytical
problem are substituted by sums. We start from the projection of
representation. A projection is just a sum of the values $`f_{j}`$ (image we
have to reconstruct) times the coefficients $`a_{ij}`$. These $`a_{ij}`$ are
strongly sparse because it often happens that one ray has nothing to do with a
voxel.  *considering the Figure 03.01* if, like in this example, we consider
the voxel in light blue as inside the shown thick ray i, the weight $`a_{ij}`$
would be greater than one, conversely if we consider a voxel outside this ray
the value will be zero.

We will deal with the ordering of voxels (or pixels) and the ordering of
projections along vectors of 1D.  It's not that intuitive to understand that
when we are in a numerical problem it doesn't matter how we number the pixels
(it is arbitrary: i.e. i could enumerate by rows or by columns the pixels of an
image. The important thing is to be congruent with the choice). We can put the
voxels or pixels into 1D vector. The same can be done for the sinogram,
ordering row by row into one big vector with a single index i.

We don't need to maintain the geometry of the problem inside the algorithm.
only in the end we have to put the values we got in the right place and get our
image but while doing the number crunching we forget of the geometry and we use
vectors of measures and vectors of unknown values.

## Slide 2

Generalizations we must consider. The non idealities which we have dealt with are considering:

1. **Extension to 3D** in respect to 2D.
2. **Geometry of scanning** is not necessarily that considered in FBP ideally
   but, as we have seen for mechanical reasons, it's much more easier to have a
   single x-ray emitting focus of the x-ray tube rotating only around the
   object and so it's clear that fan-beam in 2D or cone beam in 3D is actually
   the practical geometry we have to deal with. As we will see for the fan-beam
   actually there's anyway an analytical solution which will be the starting
   point for an extension to the cone beam by an approximated analytical method
   which is the working horse in 3D reconstruction and it's called Feldkamp
   (FDK) algorithm.
3. In a lot of cases we don't have the whole set we considered in the
   analytical problem of FBP. Suppose that we have a C-arm moving around the
   patient and that we have to do some reconstruction in a very fast time (i.e.
   during the transit time of some injected contrast in the vessels): we can't
   complete the whole 180 deg which is the minimum we need to consider
   analytically the problem. As said 360 deg is even better but we can extend
   180 deg to 360 deg imposing a symmetry. Often we have partial sets but we
   have many reasons to have partial sets:
	- Big improvement in reducing the exposure to x-rays of a patient and
	  it's clearly that if we have an undersampling of directions, those
	  which can create those streak artifacts which are horrible to be seen
	  using directly the FBP, those artifact due to undersampling of
	  directions can be better dealt with by means of numerical methods.
4. *Space variant sensitivity and resolution recovery*: The scanner can be
   doing a non homogeneous measurement. So the measurement that we have is
   space variant. For instance the mehanical collimator creates a huge non
   uniformity because resolution is decreasing with distance. so if we are able
   to include in the algorithm that specific charactersistics of the scanner we
   would be able to make what is called resolution recovery, since we really
   know the physical way the image are blurred. we can recover the resolution
   beyond the purely analytical blurring we have seen in the FBP by doing as if
   we did a back projection without filtering.
6. Account for **statistical properties of the noise**: the transmission x-rays
   are giving data which represent the level of the exposure of the detector
   during the acquisition times. So it's a continuum of energy and the
   superimposed noise can be considered more or less Gaussian. The solution of
   algebraic problems with Gaussian noise is perfect just with Least Squares
   Method which is approximated by algebraic reconstruction techniques (ART).
   Conversely in SPECT and PET (nuclear imaging) we can really counting the
   event of decay on each LOR, so the noise which is superimposed is a
   Poissonian noise (aka noise of rare events). The SNR of nuclear images is
   dependent on the number of events we are able to collect. it's quite common
   to make a volumetric reconstruction to have a number of collected event
   which is in the order of 10^6. A Poissonian distribution when N is
   increasing is going to overlap a Gaussian distribution. Conversely for fewer
   events on average (7, 6, 12 events) we have noise that is the Poissonian
   distribution which gives us a number that is random around the average we
   would like to recover. The difference of this discrete distribution: suppose
   that the average number is 11.5. we have a great probability to count 11
   events, 12 events, 10, 9, 13, 14 and so on with the typical shape of
   Poissonian distribution. we will never count exactly 11.5 events because
   they are integers (Poissonian is an integer distribution). Even if the whole
   number of events was 10^6 when we go dividing the total number by the single
   measurements or by the single voxel we will find statistics which are very
   poor. We really perceive the effect of the Poissonian distribution of noise.
   As a rule with CT we consider Gaussian noise, conversely in Nuclear Medicine
   great improvement are obtained if we consider the real Poissonian
   distribution. However as a tendency by going to higher and higher
   sensitivity of x-ray sensors and to lower and lower doses, even in x-rays in
   very special applications (which might become the rule because the lower the
   dose, the better for the patient) we start to perceive the Poissonian
   distribution which is given by counting single x-ray photons. The evolution
   is having the count of x-rays also in transmission imaging. Numerical
   methods will allow us to deal with Gaussian or Poissonian noise. So there's
   a great deal of generalization which can be done on top of FBP which is
   still the working horse in imaging.

## Slide 3 [riparti da qui]

first model:

our model is a discretization of a continuous model. the image we want to reconstruct is a continuous function in 3dimension but the field we are moving in is descrete.
we are considering projections indicated as g in this part. the formula is expressing the ith measure g we got. instead of using a dirac pulse in 2D, the real one is the sensitivity of the scanner. we must discribe for each point of measure how it is sensing each position xyz. this is the concept of an integraiton kernel. 

## Slide 4

in transmission ct we have a thick ray almost omogeneous along its axis. we have actuallu non linearities such as beam hardening (xray spectrum is losing more the low energy of the spectrum. these are attenuated more than the higher frequency and this gives artifacts). this is a non linear problem and object dependent. usually a posteriori methods are used for correction of beam hardening. in ct scanners the h is more or less an ideal tube. that is not true for SPECT --> the idealization is an ideal ray as seen exactly in one hole.


PET has a sensitivity function as well. because rays are hitting orthogonally when in the center while diagonally when away from the center. there's not a uniform distribution but we need to consider in which part of the sensor the ray is hitting. we need to take that into account to reconstruct the resolution.

## Slide 5

how to solve descrete to continuous model:

1. solving a numerical problem which crunches I and gives J (values of intensities inside the images) --> discrete-discrete
2. analytical solution to the problem: this requires as if we had infinite measures (continuous-contiuous). here we do that in one shot, differently from 1 which is an iterative algorithm that is keeping on improving the solution iteration after iteration.
3. discrete to continuous model further. if we want to have a continuous soulution we want to squeeze more unknows from the data than the data themselvs (?). how to have more unknowns than the answers --> with regularization. we are imposing regularity constraints and this way the problem is solvable. smoother solutions is better than ones jumping up and down with noises.


it permits a further generalization of voxels. when we want to reproduce the image we need not to stick on that when we are reconstructing the image. the discretization of the image can be more general. generalizing concept of pixels by basis functions. we decompose the image as sum of he basis funcitions that are continuous by themselves, they can be any shape we want (radial shape gaussian). let's think a pixelated face --> using square grid is not necessarily the best solution but we can use bell-shaped to reconstruct better images (like a smoothing filter applied in the image reconstruction).

J is the number of unknowns that could be lower than the one we want to represent so that's why we want to interpolate with basis functions.

the generalization of the basis function is not changing the problem


*Referred to the first formula*: approximation of the real image we can't reconstruct. general representation is ftildej times the basis function.

## Slide 6

*Referred to the first formula*: each projection we measured can be seen as
crossing ray voxel times the basis function (?) $\tilde{f_j}$ is the values of
intensity of each basis function.

*Referred to the second formula*: basis function for that unknown value
integrated with the sensirivity funcition of that direction.  $a_{ij}$ are
dependent on scanner and how we want to represent the image. they are not
object dependent so this means that aij can be computed only once.  the more
are the physical details introduced in the space variant the less sparse the
system matrix A will be (see next). 


*Referred to the Linear Problem*: the problem reduces to a linear problem
$\tilde{f}$ times system matrix is giving the projection. the unknown is
$\tilde{f}$ while anything else is known.

generally number of measures I is greater than J, the unknowns 


there are problem in storing the system matrix in memory because of the big
dimension. so we need iterative methods.

A trick is that the matrix A is very sparse.

## Slide 7

object function to minimize for instance with LSM.

## Slide 8

what if we used the discrete measures that we have obtained?

objective function is the leas squared: P is a linear operator that gives us
from the continuous image, given the sensirivity functions, the values of
projections.  the minimal norm of differences between these 2 vector (image -
actual measurement). this is unsolvable because f is continuous while g is not.
we need regularization and to do that --> moore penrose: let's choose the
solution with the least energy.  Ho un numero grande di misure. un campo reale
ha infiniti valori, non numerabili (non esiste qui campionamento). se le
incognite sono non contabili rispetto ai dati che ho posso imporre una
regolarizzazione. fra le possibili immagini che mi hanno dato quelle proiezioni
scelgo una cosa che varia gradualmente che ha minima varianza, questo e quello
che dice la soluzione di moore-penrose. The solution of penrose is given by a
discrete-discrete problem n which we are seing as the weighted sum of basis
functions which are again the sensitivity function. qj are unknown weights . by
doing the same process we did ahead.  inverting the algebraic problem is
solving it.

we can consider the sensitivity functions as natural pixels. basis functions
are naturalization of pixel. the sensitivity of scanner is ...


il vettore g trattino risulta essere quello ideale mentre gtilde era quello
ottenuto applicando la formula.  In definitiva penso che qui intendesse che
l'operatore p generi il vettore g tilde.
