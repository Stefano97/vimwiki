# Slides 10 - Signal detection and complex demodulation


# References and notes

[MRI video - Basic Physics](https://www.youtube.com/watch?v=djAxjtN_7VE)

Radiofrequency (RF) has a big wavelength (in the order of 10^2 cm).
Larmor frequency (aka larmor precession): the rate of rotation is directly proportional to the strength of the strength of the local magnetic field.

[MRI video - How MRI Works Part I](https://www.youtube.com/watch?v=TQegSF4ZiIQ)
