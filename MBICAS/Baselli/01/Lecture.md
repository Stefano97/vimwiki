# Lecture 1

## Slide 1

### Recall of monodimensional FT to allow extension to multi-dimensional (2D...ND) FT

- FT decomposes a 1D signal into sum (aka expansion) of sinusoidal functions
  (aka harmonics).
- It is hugely applied in the analysis of linear systems and periodical
  phenomena.
- Why it is important:
	- FT + CST (Central slice theorem) → they permit the resolution of
	  the problem of *reconstruction of the image from projections* (aka
	  Inverse Radon transform).
	- Describing raw data in MRI scan (the k-space), thus providing the key
	  for MRI image reconstruction. Remember that *Harmonics in time are
	  converted into harmonics in space*.
- The most important concept to keep in mind (when dealing with images) is the
  one related to *Current Phase* of an harmonic, equivalent to the one of
  Amplitude related to 1D signal.

## Slide 2

- Definition of FT: $`F(\omega) = \int\limits_{-\infty}^{+\infty} {f(x)\cdot
  e^{-j\omega x} dx} = F_{1D} \{f(x)\}(\omega)`$. In the first formula: $`x`$ is
  the distance in our case, the exponential is representing the harmonic. In
  the second notation the braces are used to spot what is the argument of FT.
- The second notation reported is showing that we have no more the dependence
  on x of the FT but just the dependence on $`\omega`$, the radian frequency.
- Radian frequency $`\omega = [\text{rad}/\text{cm}]`$ is used instead of
  frequency, so beware of the reference because we'll say frequency but we are
  referring to radian frequency instead.

## Slide 3

- Euler's equation: $`e^{-j\phi} = \cos(\phi) + j\sin(\phi)`$

## Slide 4

## Slide 5

- PSF → Point Spread Function: a system's impulse response. It represent the
  intrinsic blurring of the image.
- OTF → 2D-FT of PSF.
- MTF → modulus, absolute value of OTF.

Look at the following image

![Geometrical interpretation of t formula](Pictures/picture1.png)

To understand what's behind the formulation of t.

t is the direction in which phase $`\phi = \omega\cdot t`$ has fastest increase.
This means that in other directions there are lower 1D "perceived" frequencies.

## Slide 6

Representation of the 2D-frequency $`\omega`$ in the 2D-FT domain is given by a vector
$`\omega`$ in the reference system $`\omega_y/\omega_x`$ oriented with the same angle $`\theta`$
we saw in the harmonics domain.
The components of the vector $`\omega`$ are not 2D-frequencies but just coordinates in the
2D-FT space.
It's really important to note that the "perceived" periods are proportional to the
actual harmonic's period in the same way the two "perceived" frequencies are related
to the harmonic.
Performing the operation of putting the vector omega on the 2D space for every direction
is equivalent to obtain the FT of the whole image.

## Slide 7

Considering the position vector $`\bar{r} = |x, y|`$ the definition of local phase can be
obtained:

$`\phi = \bar{\omega}\cdot \bar{r} = \omega_x\cdot x + \omega_y\cdot y =
\omega\cdot (\cos(\theta)\cdot x + \sin(\theta)\cdot y) = \omega\cdot t`$

It's easy now to extend the notation to N-dimensional space.

## Slide 8

Are two different components orthogonal?

- Of course if we are considering the same direction t the orthogonality
  condition is exactly the same as in 1D, having different omega. If we
  represent them in terms of sine and cosine → in quadrature one to each other.
- how about two frequencies which can be equal in frequency but with a
  different direction?  By multiplying two harmonics in space tells us that
  much like it happens in the composition of crest/valleys in one dimension, we
  have ++ +- -+ -- so the product in values in the inner product are bouncing
  up and down the zero so the overall integral is zero. This is happening also
  in the tartan in two dimension.  There is orthogonality whenever the
  components of the bi-dimensional frequency are different.  They must be
  exactly the same in bi-dimensional plane to get a scalar product = 1 if they
  are not scaled by some coefficient.

[Orthogonality](https://math.stackexchange.com/questions/474398/waves-of-differing-frequency-are-orthogonal-help-me-understand):
we have to consider the inner product to prove orthogonality as
$`\langle\phi_1,\phi_2\rangle = \int_0^{2\pi} \phi_1(x)\phi_2(x)\ dx`$

## Slide 9

Property of orthogonality is really important because it allows to compute 1D integral
along each axis to obtain the multidimensional FT. This results in the application of FFT.
