# Lecture 4


## Slide 1

ART --> used for xray and gaussian distribution of noise OSEM --> poissonian
distribution of noise


update --> any change of the solution from step n to step n+1 applying the
iteration --> the whole number of steps to obtain a change.

(???)

number crunching (?)

single proj at a time --> in the ART method

I --> number of projections

## Slide 2

by utilizing all the data in a single iteration --> maximization of the
regularization

we want a tradeoff of fastness of convergence and regularity of convergence.


in updates we want to maximize the orthogonality of the data within the single
data in the subset and between one subset and the others (???)

orthogonal data --> each piece is giving something different from the others
(new information at any piece).

geometrically --> the information within a direction of projection of parallel
beams is giving us the span so there's a dependency of 2 nearby rays but if we
move one ray aside we would have very different information.  A smart way would
be to put together 2 projections that are of orthogonal directions because they
would contain different information.

a good idea is to go from subset 0 to 90 to 180... 1, 91, 181.  The stopping
rule is usually based on seeing if the object function is really changing a
little bit so if there's an improvement of the cost function.


that's an empirical law. algorithms are pretty fast.

one step of the reconstruction technique will mean 1 backprojection.  number of
iteration in subsets are not very big.

## Slide 3

clinical image was a standard (144 projection --> 12 subsets = 12 updates for
iteration). the clinical practise is using just 2 iteration.  Sometimes it's
better to have a good standard used in any occasion rather than keep on
optimizing (?)

## Slide 4

*An historical note*: ART was invented by polish mathematician in 1930. at that
time there was not the problem of reconstruction from projection. only in the
60s Hounsfield invented the CT. it's weird but H used exactly the numerical
method because at that time Ram/Lak was not found.

if we had the filtered projection it's not the best to use the ART. then why is
it important? to deal with exception that were not taken into account with
analitical solution (incomplete scans or limitation in time or movement of
detectors). we need to correct artifacts. we want to interpolate the non
linearities given by the presence of metal.

## Slide 5

ART --> let's start from the beer's law of absorption.  instead of fj there
should be mu but it's the same

The wij is the same as aij we have seen so far. here it's called so because of
the reported in depth material.


the idea of the 1930s guy. let's consider one single line.  row of weights
times vector of unknowns (values inside each pixel).  f has dimension J.  This
is giving a linear constrain that i sdefining an hyperplane of dimension J-1.
we will jump from hyperplane to hyperplane.


example of 2D, practically useless but academically really simple.

## Slide 6

f(0) --> image reconstructed at iteration 0.  let's consider the constrain
number 1 and project the image to see the error. we compute update number 1,
then number 2 to obtain a complete iteration

here the hyperplanes are straight lines basically.

the idea is clear, we need to fix the update rule more properly.


vectorial notation

normalization of pi by wi and multiplication by the unit vector.

OF --> projection of the first guessing orthogonally to the constraint

by making the difference between OF and OA we would find the difference to
remove to annihilate the error

## Slide 7

in this way of writing the update rules we can see that the updated value is
equal to the previus minus the correction. a weighted summation of correction.

$`f_n*w_i`$ is the simulation of projection $`p_i^{(n)}`$: how the image would look
like. comparing simulation with the data is what we are doing to compute the
corrective factor.

wij is aij (system matrix) and very sparse.

## Slide 8

the importance of orthogonality. if the data were completely orthogonal we
would do exactly 2 updated to arrive to the point.

## Slide 9

redundancies are good, of course but we have to deal with it.

why these projection are not crossing exactly in a single point --> because of
noise


how do we deal with redundancies?

average vector of the 3

## Slide 11

the trick is trying to improve the expected behavior with the information of
data and ... (???)

1. compute of expectation of likelihood
2. compute the maximization


here we step back on the usual notation aij. the unknowns are called lambdasij
which is the simbol used for emission.

The algebraic problem is seen from the single detector i.  aij is the
sensitivity of detector i wrt voxel j

radioactivity has a poissonian distribution.

poissonian is strange because it has only a single parameter, the expected
value of counts and this is both the average of the counts but also the
variance of the counts. it sounds odds that we can put equal between mean
(amplitude) and variance (squared amplitude) values but we can do that because
we are dealing with natural numbers in this case.

## Slide 12

by increasing the number of statistic (lambda) we are obtaining distribution
that are similar to gaussian.


maximal likelihood criteron is based on the bayesian criterion.


posterior having collected data and having account the outomes for that data.
prob(lambda) --> prior probability prob_y(..) --> likelihood of the data

## Slide 14

the computation of the ln(produttoria...) Ã¨ troppo computational reqiuring,
usiamo questo trucco.

## Slide 15

this is what i have to maximize

## Slide 16

this update rule is multiplicative and not additive like the other one. we
can't start with radioactivity equals to 0 because otherwise all the products
are going to give us 0.

Osem is the standard when it comes with the nuclear imaging, no longer the
filtered backprojection.

$y_i^{(n)}$ is the simulation of projection --> sum of ray projected of the
weights times the emision at time n.

the error here is percentual, not additive.
