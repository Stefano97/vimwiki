# Lecture 2

## Slide 1

If we want to reconstruct from projection we have to use the mathematical
concept of Radon Transform (RT).

*Radon transform*: set of projection through the object which can be collected
by external measures.

Another recall. The filtered back projection. The way it's presented is allowing
us to do the passage in 3D.

Radon transform is defined as the transform which collects in 2D the
various projections which you can derive in an image of 2D. Of course we must
correctly define this operation of projection. The inversion of the RT which
can be solved is the tomographic image reconstruction. 

Tomographic because it virtually shows us a slice inside the body. "Tomo" means
"to cut" from Greek as if the body was cut and we were looking at the cut body.
Of course this is not done in reality and in procedure is non invasive. It's
simply a reconstruction done thanks to the fact that rays are passing through
the body and we can have three major applications. 

- *Transmission tomography* (CT, it. TAC tomografia assiale computerizzata).
  The quantity which we are representing is the density of tissue versus the
  x-rays which are passing through the body. The attenuation of x-rays passing
  through is governed by Beer's Law. The transmitted ray intensity I is
  attenuated versus the initial intensity I0 by a factor which is the
  multiplication through the whole ray L by the local $`\mu(x,y)`$ (local
  linear attenuation --> depending on the density of the tissue at that
  position x, y. High in bone, lower in soft tissues, in air the attenuation is
  almost 0) values of mu could be given in $`[\text{cm}^{-1}]`$, However the
  specific used units are called Hounsfield and relative to the water and air,
  so by convention the 0 is not 0 HU (related to attenuation of air) but it's
  moved to density of water (which becomes 0 HU in our reference) and the
  attenuation of air is -1000 HU. The resulting law is a geometrical reduction.
  To have a projection we just have to compute the optical density (aka
  logarithm of attenuation). The projection is the integral over l of the mu
  over dl.
- *Emission tomography* (SPECT or PET).


Usually when we are omitting the adjective we are referring to CT.

The detector is a film, for example. We have to continuously move the beam and
calculate the projection.  Clearly it's a great breakthrough of what's
happening inside the head after traumas. The first scanners were developed by
the support of the disco house of EMI (disco house of the Beatles). There was
no way to look inside the head in case of hemorrhage in the head, the only way
was the traditional x-ray which needed to use Iodine contrast because a
displacement of the brain due to hemorrhage could be recognized only indirectly
by the displacement of the vessels so we needed to inject contrast which is
quite invasive, particularly after head trauma. That's why Hounsfield gained
the Nobel for this.

If we had a planar projector (so traditional x-ray) but conversely to get a
tomographic reocnstruction we have to get around the body and be able to
reconstruct the tomographic image thanks to the recomposition into a back
projection of the projection taken into many direction. The information carried
by a single projection is somewhat different to that of the image we want to
reconstruct because it's an integration (a sum of everything) that put
everything together in a specific direction --> If we look just from one side
we don't know where the attenuation is happening (beginning, middle or end part
of the trajectory L)! That's why we are rotating around the body and look from
many directions. In this way we can recover the punctual information and place
correctly the value of linear attenuation $`\mu`$ inside the image. Clearly because
we are doing the projection in different direction we can express them in polar
coordinates as seen in the next slide. $`\theta`$: direction of the projection, t: the
displacement with respect to isocenter of the scanner.

So X-ray attenuation is given by Beer's Law (Dutch pronunciation). 

$`\dfrac{I}{I_{0}} = e^{\int_{L} {-\mu(x,y) dl}}`$

With: I: Measured x-ray intensity,
$`{I_{0}}`$: X-ray without attenuation,
$`\mu(x,y)`$: Local linear attenuation (depending on the density of the tissue),
L: the overall length of the photon pathway.

By performing the logarithm of the attenuation (previous formula) we obtain the
actual projection as the integral value along a line.

$`p_{L} = \int\limits_{L} {\mu(x,y) dl}`$

With: $`p_{L}`$: projection.

## Slide 2

Hounsfield scanner (first generation) was something very slow because there was
a single x-ray source moving aside with various values of t together with a
single detector, gathering all the parallel projections. Machine then rotated
of an angle $`\theta`$ when projections in one angle were finished.

Nowadays we are using **third generation concept**. The x-ray tube has a
collimation which is no more just a single pinhole to have a single detector
hit but it provides an entire fan of rays that is sufficiently wide to cover
the whole region of interest so the whole body which is inside the gantry. A
single rotation of the x-ray tube with the arch of detectors on the opposite
side can cover an entire slice. This is the mechanics which was winning in the
end of the day.

A fourth generation was also considered, in which the concept was to move only
the x-ray tube and not the detectors because we had an entire ring of
detectors. This solution gave only modest advantage at great expense: we can
calibrate the detectors while running the machinery because the coupling
between x-ray fan and the detectors is continuously changing and so we can make
comparison when the fan of rays passed through the ring of detectors. We can't
do that in 3rd generation scanner because the leftmost part of the detectors is
always coupled with the left part of x-ray tube. So we can't calibrate
detectors during the operation of the scanner. This is not a big problem once
the detectors are sufficiently stable we need just to make a calibration once
per day with phantoms or with empty machinery and that's it.  

**Phantom**: it.  fantocci.  These are just dummy pieces of body which are well
calibrated with density, contrast and shape. They are put inside the scanner,
so we can control the result very well because we know the ground truth we have
to measure, so it's very helpful when it comes to calibration. Any scanner of
any type must be periodically calibrated and how often and how depends from the
nature of the imaging we are doing. 

Forget about the fourth generation, it introduced just poor advantages.
Conversely having an arch of detectors did permit the next improvement which
was that of repeating many arches so to gain also the z dimension
(longitudinal, along the rotation axis). Nowadays they have gained 128 or 256
repeated arches along 5 cm.  In a single rotation we can get many slices in a
volume which is 5cm thick!  Nowadays the rotation is 4 revolution per second so
in 1 second we get 20cm if we want to go fast and we don't want overlap. This
makes a scan very fast.  The speed of a scan must be compared with respiration
and how long can breath be hold for an ill patient. Gaining the third
dimension: the resolution along xy plane which was a lot good (1mm) for
tomographic images. However it was not that good along z axis before . We did
not have a slice every mm (the voxel was not isotropic so $`1mm \times 1mm
\times 1mm`$ but it was $`1mm \times 1mm \times 5mm`$) which meant that the
axial sections were very good (original ones) but if we did a reslicing in
coronal or sagittal direction we saw very well the small staircase between one
slice and the other. Reslicing was not very good.  Nowadays that we are gone
volumetric with the trick of having many arches all together this was a great
improvement and get a first reconstruction is axial but we can then slice as we
want on any plane we need to look at particular anatomical features with the
quality of isotropic voxels. The revolution was the fan beam.

**Typo**: fan-beam

The technical problem with rotating x-ray tubes and detectors stays in the fact
that we must electrically connect a lot of power, high voltages to produce
x-rays, signals to the detectors. If everything is rotating this is an
electrical problem and it was the invention of slip rings which are sliding
metallic contacts along the drum which is rotating inside the gantry that
resolved the problem. The drum, which is very heavy, has the advantage to keep
constant rotation ??? only the obvious rotation of a rotating body but not
angular acceleration and this means no mechanical stress to the structure, no
vibration and further precision of positioning.

While rotating one could not grant that electricity could always power them
through wires. The solution is given by slip rings: contacts that allow
rotation without losing electrical continuity.

CT is always the reference space because the localization is very precise not
only in relative terms but in absolute terms --> in MRI we reconstruct with
very good resolution the positions but they are just relative positions. Our
eyes don't care about absolute position but we care only about relative
position and relative contrast because we use our eyes to examine an object. Of
course we use our eyes to navigate through our world but the needed resolution
is much less.  We have two systems: one for examining the level of details of
an object (infero-temporal) and a system which is to navigate which is much
faster but with much less resolution (dorso-parietal pathway).  This to say
that localization is a different problem with respect to comparison. X-rays are
good for both with the same precision because they do triangularization, what
an architect would use to localize a point in the space. X-rays are going
absolutely straight, that's why we can perfectly localize in absolute terms the
position of objects inside the body by triangular rules. If we have MRI and CT
we need to bring the MRI space into the CT space to have the precision of
localization. CT is the one used for navigation in CAS, not MRI. MRI gives just
more information about the contrast of different soft tissues but the precision
of localization needed for navigation is given by CT. Precision of localization
is the same as the resolution. This is not valid for other system (MRI).

We stay in the mathematics of parallel beams as rays were collected in parallel
but this is the basis to solve next fan-beam and we could easily put the rays
in polar space as if they were collected in parallel (like in first generation
machine) just by the so-called rebinning because also in the fan-beam we can
define the angle theta (different from the angle of x-ray tube but of that
specific ray. The so-called azimuthal angle) and the displacement from the
isocenter of the scanner as usual. We can rebin and obtain the data with a
little bit of interpolation because the sampling are not uniform in that case
but we can rebin everything as if it was sampled in parallel.


In CT, the tomographic image maps the linear attenuation $`\mu(x,y)`$ with the
local density of body tissues $`f(x,y)`$.

## Slide 3

Nuclear imaging gives us the other ways of tomographies.  To get emission from
a body we need to make it radioactive and this is done in a very selective way
by the injection of specific radiotracers. Radionuclides are isotopes which are
not stable but they are decaying to another nuclei nature and they are emitting
some radiation (gamma photons --> SPECT; or positrons which annihilates with
the antiparticles which are the electrons emitting gamma rays in 2 opposite
directions --> PET).  One of the most used radionuclides is Technetium (Tc,
especially in it's metastable form, Tc-99m) which, by decaying, is emitting a
gamma photon with a lot of energy. Scintillators are used to detect these
attenuations. These scintillators can stop gamma rays and convert their energy
into a bunch of visual band photons (in the form of blue light). We have a
flash of light when a gamma photon is stopped but since the energy is so big
the thickness of detectors must be big and the resolution we can achieve is
limited essentially by the dimension of the scintillator crystals. In x-ray we
can much more resolution because also in that case solid state detectors which
are the most used nowadays deal with energy which are lower, in the order at
maximum of 70 keV, so we can get a better resolution. In the upper side the two
photons that are emitted after positron emission are 511 keV per photon so we
can't do without a section of 4mm x 4mm x cm. This means poor resolution for
this technique. Nuclear imaging is giving us selective information but normally
poor resolution. It's huge what we can do with emission imaging. A lot of
advancements in research were done through radiotracers. At the very beginning
the radio chemistry was used to investigate biological problems, even earlier
than the existence of scanners. We had to get in experimental animals some
radiotracers, we waited the positioning of the radiotracers along metabolic
pathways we wanted to evidence and we kill the animal, make slice of it and put
them on photographic films and the emissions were letting the print on the
photographic films. So the experience of analyzing biology by radiotracer had a
long history ahead of gamma cameras and PET. With the construction of scanners
we can extend this methods to non invasive applications and humans. It's
clearly not fully non invasive. Also x-rays are ionizing emission that are
traveling through the body and can harm us and we can limit the dose of x-ray.
Also here we have to limit the exposure to radioactivity of each patient but
what we never have here is the problem of toxicity of the molecules we are
delivering. We may understand that a lot of times something that is very
selective biologically is also very toxic because it's something which is
biding some activity which is very important to vital processes. For instance
Fluorodeoxyglucose is very toxic because it gets inside the cells in
mitochondria which are processing the glucose but it's not processed by them so
it's accumulating there and poisoning the mitochondria, however the sensitivity
is so high to the radioactive emission that the quantities can be very low and
there's no toxic effect, even if the efficiency of capturing the emitted photon
is pretty poor (1/10^4 - 1/10^5). On our side we have the power of Avogadro
number which is 10^23. By a mole we would have 4x10^23 potential guns shooting
a gamma ray outside the body. We can really play with pico or nano mole of
radiotracers.

Radiotracers are particular molecules that are active metabolically and marked
by radionuclide and must have been tested to be used on humans to be labeled as
radiopharma.

**i.v.**: abbrev. for in vivo.
LOR: abbrev. for Line of Response

SPECT (emission of one single photon): both the decay process and the
directions of emission are fully random processes, any radioactive nucleus is
decaying whenever it wants independently from the other ones and also the
direction of the emission is fully random. Once emitted, the photon travels
with different directions, influencing detection. Some of them can be
scattered, some can be absorbed (stocked inside the body) but in the end those
who arrive towards the detector must be distinguished depending on the line of
response (LOR) on which they are traveling. This can be done only in a
mechanical way by the collimator. We don't have lenses for x-rays and gamma
rays, we can't deviate them but just stock them or let them go. Only
collimation is allowed. The small holes and very thin, much then the reported
ratio in here of parallel holes are permitting us to have on detectors rays
which are classified in their x and y position. If we have a measure here in
red, since we don't know from which point of L the photon which arrived to the
red hole were emitted what we are measuring here is exactly an integral but now
it's an emission integral where $`\lambda(x,y)`$ is the local emission value, so the
value of radioactivity. So the image which will be reconstructed from this
projections which are integral measurements is the map of radioactivity of the
slice inside the body which in turns correspond to the map of concentration of
the radiotracer. For instance a tumoral region which is absorbing and has a lot
of metabolic activity will be a hotspot with a lot of concentration and
radioactivity because in the minutes after injection and before the scan, it
has absorbed and accumulated a lot of radiotracer (Tc in SPECT or Fluoro... in
PET).

### Notes

Positrons are electrons antiparticle, meaning that they have the same mass, the
same charge (but opposite sign) and the same spin.

## Slide 4

Positioning: from bottom to bottom to top of the image. Collimator,
scintillation. Localization of flashes in scintillation of crystals is not done
by single detectors with that resolution but we can exploit the fact that there
is from each scintillation point a fan which is affecting an array of
photomultiplier that is much bigger than the resolution but by computing the
ratio of visible light that was received by a photomultiplier tube with respect
to its neighbors (if a PMT is receiving 50% of light intensity and the one
nearby is receiving the other 50% this means that the scintillation has
happened in the middle of the two). The Anger logic (Anger is the guy who
invented the Anger or gamma camera, that gives snapshot for gamma rays) this
can give us projection. We can understand that gamma camera is intrinsically
three dimensional so it starts with parallel detections on an entire planes and
the question of doing a SPECT is that of rotating the gamma camera around the
body. So it's intrinsically volumetric. we don't need to pass from 2D to 3D.
The great problem of gamma camera is that resolution is increasing linearly
with distance and also the attenuation is increasing linearly with distance.
This is seen better on the collimator: this to be absolutely detected for an
ideal ray, this small hole should have an infinite aspect ratio (zero cross
area or infinite length). If it is not but has a limited depth of course it's
not an ideal ray but a cone, which is opening from this hole and so this
accepts a whole cone and the resolution is decreasing with the distance. We
have a sensitivity function which has a cone shape. There's also the problem of
attenuation as seen in *Fig. 02.5*. Here we clearly see that it's much better
to acquire a SPECT considering the whole 360 degrees, even if mathematically
two projections which are opposite by 180 degrees should be exactly the same
but really they are not. Indeed when we do not make the volumetric
reconstruction but we are doing a whole body gamma camera just flat which is
good enough without the need of rotating we can see how is different the image
of this normal guy seen from the front and seen from the back. The other
picture is showing metastasis in the bones and this is one of the main
diagnostics which should be done with somthing with a tumor because nowadays
would be a shame not detecting tumor metastasis because they can be cured very
well by radiotherapy since the bone is thick and thus stop the energy of
radiotherapy very selectively, so we can really cicatrice very well this even
if this was just a palliative cure it avoids a lot of troubles and pains to the
patient so it's very important.

## Slide 5

To do the collection of parallel projection on several planes as we can see all
together we just have to go around with 1 or more gamma camera. In this slide
there are tomographies reconstructed in the heart and the ones reconstructed in
the brain. We can notice that the resolution is quite terrible compared to that
of CT but the information we get is huge. For instance we can see the Dopamine
reuptake. consider that they take Dopamine and labeled it by a radionuclide,
they injected it and were able to look the work of basal ganglia using
Dopaminergic inside the brain. The biological information contained in here is
really huge and incredible.

SPECT is obtained by rotating two or more gamma cameras

### Notes

Reuptake: it. ricaptazione. Quel processo mediante il quale il
neurotrasmettitore che si trova nello spazio sinaptico viene riassorbito a
livello della membrana sinaptica. 

## Slide 6

*PET*: Also here we have the emission of gamma rays and we can also use the
gamma camera with mechanical collimator much like the former case as well but
we can do better: the scanner can exploits the fact that the emission of the
positron, after a small positron range in the order of mm, depending on the
radionuclide, the positron gets an electron and annihilation happens.
Annihilation energy is exactly 1022 keV that is divided in two and for
reasons of conservation of the motion quantity these two photons are in random
direction but one exactly opposite to the other, having the same energy. If they
stay inside the tomographic plane (aka ring of detection) we can position the
LOR just by connecting the two detectors which were activated simultaneously (i.e. the
difference of time of flight of the two photons, whatever is the place on this
LOR is within the order of few nanosecond). With huge electronics we can detect
and register all the contemporaneities between two detectors and register one
event on this LOR. Again we don't know what is the place where the decay
happened but we know that this is on this LOR and it's also known as electronic
collimation to compare to the mechanical collimation seen before. It's much
more precise than mechanical collimation but the problem is the huge energy
involved (511 keV) so we need really big scintillating crystals otherwise these
photons could pass through and go away. We then count one after the other
the events happening on the LOR. As a result we have again the integral measure
along all the LOR, collecting again projections so we have the very same
problem of reconstruction from projections. 

What is the difference between CT and emission tomography? In CT we have a
continuum of energy which is detected. So we can consider a Gaussian noise. In
emission tomography the noise is that of rare events both in SPECT and in PET.
We are counting and the number of count is subjected to a different kind of
noise which is poissonian noise which can benefit of a different type of
reconstruction methods but when we go on the FBP the kind of noise can be
completely neglected and we can go on in the same way both for CT, PET and
SPECT. Here we can see very huge images of fusing a CT which gives these big
anatomical details and the PET which gives the hotspot. This is the case of
tumoral lesions which are detected very well and localized thanks to the PET
and localized very well thanks to CT. Registration is so precise because both
the scanners are inside the same machine so the patient is not moving on the
bed inside the gantry and so it permits this very good alignment.

The emission of two collinear and opposite gamma rays permits electronic
collimation by their temporal coincidence, thus localizing a LOR. Emission is
measured to reconstruct the local image intensity.

### Notes

*Typo*: Fig 02.9 A. That's the one referring to the annihilation.

The most used positron emitting radionuclide is Fluorine 19.


## Slide 7

$`\hat{\xi_{\theta}}=\cos(\theta)\hat{i} + \sin(\theta)\hat{j}`$

With: $`\hat{\xi_{\theta}}`$: unit vector in t direction, orthogonal to the
projection ray.  $`\theta`$: angle between detectors' line and x-axis (it.
angolo tra la retta su cui giacciono i sensori e l'orizzontale t).  $`t`$:
displacement with respect to CT scanner isocenter.

Representation of projections (and plane in 3D) is given by this unit vector
dot (prodotto scalare) with r. 

Scalar product = t (which is the displacement).

The interpretation of this scalar product is the projection of the vector r on
the detectors' line t.

![Geometrical interpretation of projection ray](./Pictures/projection_ray.png)

In the reported picture I've drawn the position vector $`\mathbf{r}`$ in orange, the
unit vector $`\hat{\xi_{\theta}}`$ in blue and the projection on the
detectors' line in yellow.

A point of the Radon Transform can be represented by utilizing an integration
kernel built on this equation of the line. When we are out of the line in xy
plane we can say that the Dirac pulse with argument line equation is different
from zero. When the argument is = 0 the value of the Dirac pulse is infinite
but the integration gives 1. We want to extend the Dirac pulse on a plane. The
integration along a line can be seen in the plane with an integration kernel on
the only integration line. It's much more flexible but not intuitive in the
images.

Integration line is the set of points which satisfy $`x\cdot \cos(\theta) +
y\cdot \sin(\theta) -t = 0`$.  This defines a blade (it. lama, non spada) in xy
space which is the space in which we want to reconstruct.


So basically when the point is belonging to the projection line it is collected
by the integral, otherwise it's discarded.

The two coordinates (theta, t) are used to refer to a projection point.

### Notes

Scalar product can be seen as the sum of the respective product of the
coordinates of the two vectors, without considering the angle between them.

Detectors' line: la retta su cui giacciono i sensori.

## Slide 8

To solve the ideal problem we should consider the sinogram. In scanners we
store raw data of projections in files called sinograms. They are stored
orderly and each row is representing the inclination theta of projection and at
each inclination we have the level of the projection at each displacement t.
It's a strange way to give a rectangular representation of a polar coordinates.
This is the kind of sampling that we have though, a matrix of values evenly
sampled along theta and t (i.e.  256 values along the displacement t).

Why sinogram? If we have a single contrasted object in an image it will appear
as a sinusoid, then the maximum amplitude of the sinusoid is when the radial
position of the object is coincident with respect to the detectors' line.

By theoretical point of view it would be sufficient to have just 180 degrees in
theta but as we have seen in the emission tomography we have differences when
rotating (the front portion was different from the back one) so that's why we
should consider 360 degrees of projections.

*Referred to the examples at the end of the page*: non ho capito bene questi
primi due punti. riascoltare la registrazione.

### Notes

A fixed point in the image space is represented in the sinogram as a
cosinusoid. This reasoning is taken into account the reported picture: let's
consider a fixed point in the image space described with an angle $`\Phi`$
between its position vector ($`\mathbf{r}`$) and the horizontal axis. The
projection of $`\mathbf{r}`$ onto the detectors' line is performed by
multiplying the modulus of the position vector and the cosinus of the angle in
between ($`\beta`$ in the picture).

![Representation of a fixed point in a sinogram.](./Pictures/fixed_point.png)
$`\beta = \theta - \Phi`$

$`\bar{t} = \cos(\beta) = \cos(\theta - \Phi)`$


## Slide 9

First we need to define the Central Slice Theorem (CST) in 2D.

CST says that 1D-FT of projections given an axis t and a fixed angle theta
gives a line of the 2D-FT (so the FT of the whole image) along a central slice
(or central axis). The demonstration is shown below.  We can demonstrate it for
a single axis: one of the orthogonal axis is more immediate but by rotation we
can extend the concept to any inclination of theta. We are here considering the
projection for the condition we are asking ourselves what can be the value of
the 2D-FT on the $`\omega_x`$ axis where $`\omega_y`$ is zero. This means to null the
term depending on $`\omega_y`$ in the computation of the 2D-FT. The exponential is
just a function of x and not y and can be brought outside the first integration
which is done on y and so what we find is exactly the projection along
direction y (so for theta=0). We found exactly the relationship we wanted to
find! The 1D-FT of all the projections all along the direction theta=0 is
giving us the values of the 2D-FT along direction $`\omega_y=0`$. This is valid for
any direction.

The steps are the following:

1. Collecting all the projection along t.
2. FT all the values of integrals (so projections) in 1D.
3. obtain all the value of $`\omega_t`$ in all the plane of 2D-FT.

So we can see that, to represent this relationship, it's much more natural to
represent the Radon Transform (the collection of the projections) not in the
technical way which was the sinogram but in an analytical way, which is the
natural space of the RT that is obviously polar, in which I'm keeping each
value of integral that is put at its specific theta and t.

By this mathematical reasoning we can put the detector in the middle of the
scanner, passing through the isocenter of the scanner. This is not physically
feasible but by mathematical point of view we can do that. In this way we can
talk about virtual detector line.

The striking result we can derive from this is that if from the RT we can
derive all the values inside the 2D-FT (i can do the trick for any of the
values of theta) i will end up with filling all the Fourier space of this
image, so at this point i know the solution for the inverse transform, so from
these values in the F space I can invert the FT and get the image again. So I
can now be sure that, from these values of projection I have detected, I will
be able to reconstruct the image. I have demonstrated that the inverse Radon
Transform does exist.

## Slide 10

This can be solved by the FBP. Look at the [animation](https://humanhealth.iaea.org/HHW/MedicalPhysics/NuclearMedicine/ImageAnalysis/3Dimagereconstruction/index.html) to know how it
looks. The process of numerically constructing the FBP by back projecting from
one projection to the other. The solution is here described:

1. Get the sinogram data and filter them with a ramp filter (that is a HP
   filter): it amplifies proportionally to the value of $`omega_t`$ along the
   direction t at each inclination $`\theta`$. monodimensional filter: a FIR in
   one dimension, on the single row for a specific theta in the sinogram by the
   convolution of the ramp filter by the sinogram data at fixed ... This
   operation is necessary because we need to enhance the high frequency that
   would be conversely blurred if we did simply a back projection.
2. we can pass to the back projection: we assign the same values back to a whole
   line $`L(\theta, t)`$ because what we know about that line is the integral
   sum and we make it uniform along all the line. Summing all the directions we
   will be able to reconstruct the image.

If we do a simple back projection we would obtain a blurred image: i would lose
a lot of resolution because of the reason that is at the basis of the
demonstration we are about to make. So before back projecting it's necessary to
ramp filtering. The solution is very simple and it's this filtered back
projection.

In pratica vado ad effettuare il filtraggio delle proiezioni con un passa alto
per effettuare il deblurring e salvo la verisione filtrata del sinogramma. La
risposta all'impulso del filtro a rampa dipende dalla coordinata spaziale t
perchè teta rimane fissata.  In secondo luogo si sovrappongono le varie
proiezioni filtrate nello spazio dell'immagine andando a sommare i contributi
di ogni proiezione, avendo come accortezza di considerare costanti le intensità
appartenenti alla stessa LOR 

## Slide 11

1. The demonstration starts with this trivial writing: the image is the IFT of
   FT of the image.
2. Switching coordinates from orthogonal to polar. As we know whenever we
   change integration coordinates, we must consider the change of signs of each
   integration element and which i have to correct by the Jacobian, otherwise
   the integral is not working properly. The Jacobian of the passage from
   orthogonal coordinates to polar coordinates is just proportional to
   $`\omega_t`$ so the radian position in space $`\omega_x \omega_y`$. We have
   transformed the integration from orthogonal to polar (on each radius from 0
   to infinity over $`2\pi`$).
3. In this integration we can notice the Radon Transform of the image
   $`R\{f\}(\theta, t)`$, so all the projections in polar coordinate. Using the
   CST, we can convert from 2D-FT of the image to 1D-FT of the Radon Transform.
   In this way we have the integration only wrt $`\omega_t`$.
4. This passage is introducing the ramp filter which is the modulus of
   $`\omega_t`$.  This is done by considering a symmetrical integration:
   instead of integrating from $`[0, +\infty)`$ we prefer to integrate from
   $`(-\infty, +\infty)`$. This integration in the negative part of values is
   in the opposite direction as the former integration from $`[0, +\infty)`$.
   When I am moving from $`(-\infty, 0)`$  I'm moving in the opposite direction
   of $`\omega`$, so I must change the sign and so I get the modulus of
   $`\omega_t`$. Now I have the demonstration! Ahead of doing the back
   projection that is represented by this sum I have to filter the data at each
   theta by a ramp filter which has this frequency characteristics (similar to
   that to a derivative --> amplifying proportionally to $`\omega_t`$. But it's
   not derivative because it is absolutely symmetrical and it does not have a
   phase shift of +90 degrees. It's a symmetrical fir filter that is not
   changing the phase but only works with the amplitudes.)
5. We have the multiplication of the 1D-FT of the ramp filter by the 1D-FT of
   the projections (which are the data in the sinogram), I can convert this
   multiplication considering the IFT in the original domain of the data (i.e.
   projections). So I get the convolution of the projections times the ramp
   filter.
7. Back projection is represented by our usual kernel $`x\cos(\theta) +
   y\sin(\theta)`$.

The important point is that the shape in the frequency domain of the filter
does come out from the two concepts: passage to polar coordinates (with
Jacobian) next the symmetry between integration along the positive radii from
origin in the frequency domain which means to introduce the modulus of the
famous ramp filter

### Note

I think there's a typo in the second point of the demonstration: it should be
$`d\omega_x\cdot d\omega_y = \omega_t d\omega_t d\theta`$. The Jacobian
determinant is computed from the matrix here reported.

$`\omega_x = \omega_t\cos{\theta}`$

$`\omega_y = \omega_t\sin{\theta}`$

$` \mathbf{J} = \begin{bmatrix}
\dfrac{\partial{\omega_x}}{\partial{\omega_t}}&\dfrac{\partial{\omega_x}}{\partial{\theta}}\\
\dfrac{\partial{\omega_y}}{\partial{\omega_t}}&\dfrac{\partial{\omega_y}}{\partial{\theta}}\\
\end{bmatrix} = \begin{bmatrix} \cos{\theta}&-\omega_t\sin{\theta}\\
\sin{\theta}&\omega_t\cos{\theta}\\ \end{bmatrix} `$

The result of the determinant is $`\omega_t`$, so this is the coefficient that
we need to multiply in order to perform the variable change.

## Slide 13

The geometrical demonstration of the FBP. It's much more intuitive than the
analytical one. 

The *Fig. 02.14, image on the left* is representing a projection.
First of all we must understand what kind of blurring we are introducing if we
project an object and next we back project it, so what is the PSF (Point
Spread Function) of these operation of projection and back projection. In this
way we know what kind of filter we need to correct this blurring. If we project
a point this would be a Dirac pulse: PSF means how a Dirac pulse is blurred
into a bell shape (in it. PSF è la funzione di sfuocamento di un singolo punto:
come un singolo punto ben contrastato, un impulso di Dirac in 2D, si allarga a
causa della perdita di risoluzione o blurring). 

*Fig. 02.15, image on the left*. What would be the PSF of this operation? If I
do the projection from a single direction, due to the CST and the property of
the FT, this blade which is the result of projecting and back projecting from a
single direction will have a FT which is still a blade.  **Demonstration**:
consider the image on the left. The blade is uniform in its direction, having
only zero-frequency content. So we need to have something in the Fourier Space
which has a Dirac pulse along the direction of the 2D-blade on the left, which
is an orthogonal 2D-blade. *Back to the figure on the left* Conversely,
considering the orthogonal direction (with respect to the direction of the
2D-blade) I will get a Dirac pulse in the image space, which is containing all
the frequencies in the Fourier space: it's uniform in this space. That's why
the FT of a 2D-blade is an orthogonal 2D-blade.

What happens when I consider many blades of projection and back projection?  I
will get a *star of blades* both in the image space and the Fourier space. What
would happen if the number of blades (so the number of projections) is going to
$`+\infty`$ as we were considering for analytical methods differentials that
were infinitesimal? When the projections are tending to $`+\infty`$ I will get
a star of blades which is continuous. The density of a star of blades that is
diverging radially is decreasing with $`\dfrac{1}{\text{distance}} =
\dfrac{1}{\rho}`$: the density in the middle is larger and conversely in the
periphery of this the density of summing up these blades would be 1/distance,
both in the image domain (PSF) and in the Fourier domain (FT of PSF which is
MTF or Modulation Transfer Function). The PSF, since it is uniformly applied in
the whole image because the projection and back projection operations we did
which were the only one we have done and the FT are all linear space invariant
operations so this is the general PSF of any part of the image, which must be
corrected. How can we correct them? We need a correction of the frequency
content which is inverse to this. So $`\text{MTF}^-1`$ which is the deblurring
filter. The deblurring filter of $`\frac{1}{\omega_t}`$ is something that
increases as $`\omega_t`$ but, due to CST and the symmetry of the filter, doing
this filtering on the blurred image (it can be done) is not very useful. This
would imply to back project onto the image space and then filter. It would be
way better idea to filter out ahead of back projection and we can do that thanks
to CST: we will apply a section, a central slice of this cone which is exactly
the ramp filter.

## Slide 16

We need a digital implementation of the filtering back projection. This must be
kept in mind. We will talk about continuous methods talking of the FBP and its
extensions in 3D to distinguish them from the numerical iterative methods but
where they are continuous analytical methods but the implementation is in the
end digital. The difference is not that much between anaylitical methods and
numerical ones. The main difference is that analytical methods are "one shot",
not iterative. Conversely the numerical methods are improving the solution
little by little through iterations. The real difference when implementing is
not between continuous and discrete but between "one shot" analytical
implemented in numerical solution and iterative numerical solution.

To digitalize the ramp filter we need to discretize. Clearly the discretization
is exactly that of the image we have, so how frequent, what is the size of the
detector that this dimension of each space sample and so if we have a number of
2B which is the sampling frequency of detectors per cm, the Nyquist frequency
which is the half of the sampling frequency due to the Shannon theorem is the
maximal frequency we can represent. The DFT will not be a ramp but will be
periodical with periodicity -B +B.

Ram-Lak filter: truncating at +B. This is stressing as much as possible the
ramp filter, recovering resolution as much as possible.

In digital FIR filter as we know it's very common to limit the bandwidth so one
way it is to just window it. We should do that and go up with the ramp just for
some frequency and then go down because enhancing too much the high frequency
would enhance also the noise. In case we want some more regular smoother
reconstruction at the expense of less resolution, the menu of the workstation
of the scanner will let us choose the Hamming windowed filter. This is very
limited in bandwidth so very smoothing and limiting the resolution. 

An intermediate solution is not done by windowing as we normally done in fir
filters but by the Shepp-Logan filter just trivially take an arch of a sinus
and we know that $`\sin(x)`$ is almost close to x near 0 and later it
saturates, so it limits a little bit the amplifications of high frequency.

## Slide 17

*First formula*: Depending on the filter we can see the formula of the inverse
transform of the ramp shape (it is a sinc combined with $`\text{sinc}^2`$ with
zero average). In the end of the day the number of samples we need to implement
the Ram-Lak or Shepp-Logan or Hamming are very few: this is a very efficient
operation and we can allow filtering on the raw data in one dimension and next
we back project, sum up everything and we get the image.

## Slide 18

The analytical rays do not exist and we have to consider thick rays. In the end
of the day when we make the back projection we will use the intersection between
pixels and thick rays with the dimension of the detector (here indicated as
$`a_{ij}`$) which we will discover that they are the same as those we were
using in numerical methods. So in the end of the day the difference when we go
in the Workstation program is not that much between ART (algebraic
reconstruction technique) and FBP implementation.  This is worth to understand
physically. The computation of these coefficients which are a lot, given the
dimension of the images, is quite a nightmare. There are several techniques to
approximate these areas with the average length through the pixel, the area,
the distance and so on but it's not a matter of this course. 

*first formula*: the formula of the discretized projection.

In the reconstruction of images the use of the back projection coefficient is
the most heavy part of the algorithm.

## Slide 19

We are going to discrete problems, here we are recalling something from
bachelor. We are sampling things, so limiting the bandwidth to the Nyquist
frequency. We recall that the discretization of the image space does imply a
discretization also of the Fourier space obviously in orthogonal coordinates
and the number of samples are the same in both the domains (image and Fourier
domains). $`M\cdot N`$ points in the image means $`M\cdot N`$ values in the
Fourier Transform. Actually we could get just half of the plane of the Fourier
transform because these are complex values but symmetrical in the Fourier
domain through the symmetry between the positive and negative frequencies. The
balance of the degrees of freedom in the two transform is maintained.

The sampling is not even when we get the various directions but there's this
radial combinations and the number of samples which are needed over 360 degrees
(aka the angular resolution) is something proportional to the size of the field
of view (FOV) because we need that these $`\Delta`$ at the border of the image
is equal or less than the resolution we want in $`\Delta_x`$ and $`\Delta_y`$.
By solving this very simple problem we get the number of samples we need with
$`\Delta_{\theta} = \frac{2}{N}`$.

## Slides 20

If we have too few angular sampling we have streak artifacts, we see the stars.
These are dramatic in cases of very contrasted objects much like metals inside
the mouth in dental radiography. This is typical streak artifacts we see when
using the FBP in emission tomography which are expanding the activity we have
reconstructed outside of the body and we can see that FBP is very good but it
does not take into account the physical limitations of the scanner because it
is an analytical solution that makes a lot of symmetry assumptions and
completeness of data assumption and this is justifying the effect that we need
to ???. Such artifacts and limits in the FBP will justify why in the next
lectures we will dedicate some time to numerical methods specific for the
transmission and emission tomography.

# References

Slides: Filtered back projection 2021.
