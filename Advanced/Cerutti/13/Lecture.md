# Lecture 13

## Slide 1

This is a conclusive sequence of slides. It's related to ethical problems in
advanced health technologies and the role of ethical issues. We don't have to
forget that being a Biomedical Engineer means coping with human subjects and
also the risks of coming in conflicts with the subject's human rights in order
to have all the ethical aspects respected. That's why the ethical problems
in our area are very important. This is a sequence of slides presented in a
conference on this topic (Professor is also the Chair of Ethical
Committee of two important clinical centers in Milan: European Institute of
Oncology and Cardiological Institute of Monzino). It's not usual for a
Biomedical Engineer to be the Chair of Ethical Committee: generally they are
Physicians or Ethicists, people having a more traditional background for this
mansion. In this case it was important, at least in these two Committees, to
give the Chair to BME. This is to remark the importance that Biomedical
Engineering could have more and more in the future regarding this issue. It's
mandatory having a Biomedical or Clinical Engineer in this Committee.

## Slide 2

Two points that Biomedical Engineering has to be involved to pay a greater
role on:

- Ethical aspects are certainly involved in clinical trials. All clinical
  trials should be monitored by an Ethical Committee who evaluates all the
  ethical implications involved in there. And because a clinical trial is
  mandatory before inserting a new product into the market it's necessary to
  make clinical trials that should be ethically admissible (not harmful for the
  involved subjects). They should be tailored to the objective to evaluating
  the correctness and the efficiency of the procedures to be taken into human
  subject.
- Ethical aspects are involved in the phase of development of hardware and
  software regarding biomedical technologies and, above all, the most critical
  ones are directly connected with the patient.

## Slide 3

European regulation of clinical trials. The vision is to have an harmonisation
of the standards to be used on the clinical trial. The regulation is n.536 of
2014 . It entered into force on 2019. There was a period in which Europe issued
directives as European Laws, now Europe issues regulations as European Laws.
Why this change? Because the conception of directives was ... It's explained on
the next slides.

Read

*First paragraph*: This is a simplification in respect to the directive regime.
Now there's a regulation regime and with this simplification that was
indicated in here.

*Second paragraph*: it's a clarification that was needed in order to substitute
the directive regime with the regulation regime, in which everything should be
more clearly explained and with some clear mandatory jobs which are indicated
to manufacturers.

*Third paragraph*: If one company wants to introduce in the market a new
medical device, It needs to go over these ethical issues. These issues shall be
approved by an Ethical Committee and, possibly, at European level as the
requirement for safety and performance of medical device have an European
Legislation (???). The European Citizens need to be notified in both the case
of approval or rejection of the authorization of the Ethical Committee at
European level.  EMA: European Agency.

A passage from the directives regime to the regulation regime has all these
kind of implications.

## Slide 4

It is mandatory for all of us who make research and for all the people that
have to use devices which are connected to humans or animals that all this
equipment should be compliant with the ethical aspects and must be approved by
an Ethical Committee (EC). This is a very important step that need to be
fulfilled.  No experimentations can be done without the approval of the EC.
All the important hospital and universities have an EC, even in Politecnico.
All over the branches of Engineering face ethical problems.

## Slide 5

The reinforcement of this requirement of having ethical issue which has to be
respected had some historical issues and they derive from Nuremberg Trial to
Doctors, just after WWII.

After WWII many Nazi officers were condemned because they made some experiments
on subjects without respecting any form of human rights. Because of the fact
that they used people illegally and without their consent, they were condemned
in this Trial.

## Slide 6

Medical experimentation made in period of WWII in Germany were related to these
elements. These are all the issues that emerged during the Nuremberg Trial.

## Slide 7

After the acknowledgement of horrors that were performed without any reasons,
in May '47 a code made of ten points was made establishing the basic principles
of all the EC now working in companies, institutions and in all situations in
which humans could be subjected to non ethical procedure.

1. Voluntary consent, well-informed.
2. There should be a reason to conceive this experimental protocol.
3. It should be based on previous knowledge. It generally comes from knowledge
   obtained on previous animals or on other experiments on humans that already
   obtained the permission to be carried out.

Read the following points...

## Slide 8

- BME: BioMedical Engineering
- MD: medical device
- IVD: in-vitro diagnostic

There's a strong need of new regulations on MD and IVD MD. The new important
regulations on MD are the ones on MD themselves and the one on IVD MD.

These two rules establish the requirements to be fulfilled for MD and IVD MD
to be allowed to circulate freely in EU. This is the main objective of these.

Up to these dates the already present directives apply but, later, the new
regulations are.

## Slide 9

The reason for the reformulation of medical device standard came from serious
incidents connected to deficits in medical devices. The regime of directives
allowed the occurrence of dangerous situations, like silicon-gel mammary
prosthesis explosion when some of these patients were on the airplane, another
example is metal to metal incongruences of hip prosthesis: some reactions to
this demonstrated that the directives were not too precise to prevent this
incidents. These two accidents have influenced a lot the European Public
Opinion.

## Slide 10

The MD and IVD MD. This is a very pictorial example of medical devices and IVD
MD and possibility of classification according to Class I, II, III, IV
of IVD MD.

## Slide 11

The new regulations of MD in a nutshell.

1. The big difference of regulations in respect to directives. A stricter
   ex-ante (before entering into the market) control for high risk devices by a
   pre-market scrutiny mechanism with the involvement of a pool of expert at EU
   level. There's a stricter control before entering into the market. Many
   aspects of safety and performance are analyzed from a scientific point of
   view before entering into the market.
2. *[The reinforcement...]* Notified bodies: the bodies which, according to
   National Legislation, are in charge of verifying all the processes of the
   equipment fabrication. The inclusion of a Notified Bodies and the
   reinforcement of the check that all the notified bodies we have in Italy are
   professionals and could act for the benefit of all of us is an important
   aspect to be verified.
3. *[The inclusion...]* Sometimes we have some devices which are used for
   aesthetic purposes (i.e.  system of small surgery of reparation of small skin
   deficits). Even aesthetic devices which present important characteristic
   related to the man/machine interaction has to be considered as inserted into
   the regulation. Even some devices which are not intended for diagnosis or
   therapy and that, theoretically, could not be classified as medical equipment
   because they have only an aesthetic purpose, if the technological
   characteristic of it is the same of the electro-medical equipment, must
   respect the regulations of medical devices.
4. *[The introduction of...]* Introduction of new risk classification for IVD
   MD which is significantly changed with respect to the previous one of
   directives.
5. *[Improved transparency...]* The trend is to have a great transparency with
   the possibility of tracing a system based on a Unique Device Identification
   Number or Identification code.
6. Read as is
7. Read as is
8. *[The strengthening of...]* This is a typical task of manufacturer who has
   to make surveillance in the post-market steps of their product and it is
   part of their job and it must be done and in the new regulation this is an
   aspect which is well remarked.
9. Read as is

Shortly speaking the regulations are more powerful and serious instrument for
checking in reality all the safety and performance aspects of medical devices
being more severe from one side and giving more responsibility to the
manufacturer but in a clearer way in which you have these tasks and if you have
a disclosure of these tasks the responsibility is yours.

## Slide 12

Summary of recent regulatory mechanisms.
The three reported in here:

1. This is for drugs
2. Already mentioned 745 for MD.
3. Already mentioned 746 for IVD MD.

The first does not account for second and third. Second and third don't recall
ethical problems. That's why the regulation 679 of 2016 is compulsorily active.
This is the very famous GDPR (General Data Protection Regulation) 

Read.

This is not necessarily concerning only medical data but also master (it.
anagrafici), biometric, genetic data. It is clear that the health data have to
be manipulated respecting all the requirement specified in this GDPR. All
possible violations need to be notified to checking authorities (data
protection officers).  Accountability (it. responsabilità) to various level is
important for GDPR and for a complete implementation we will have a national
law plus advice from privacy authority (in Italia abbiamo una Legge Nazionale e
un Consiglio dall'Organismo della Privacy che conclude il discorso della GDPR).
La GDPR è una regulation che riguarda la protezione generale dei dati personali
che vengono utilizzati per vari scopi, che possono essere di tipo medico ma
anche di tipo anagrafico etc.  I dati di tipo medico sono quelli più sensibili
e quelli su cui si è prestata una maggior attenzione.

## Slide 13

Described with the previous slide.

## Slide 14

Read as is

## Slide 15

Criteria for the composition and the operation of EC which must take a decision
in every Health System, Hospital or Company in which some of these aspects need
to be considered. Read as is At least a couple of people who could be trained
in our areas (clinical engineer and expert on medical devices) are to be
inserted compulsorily in EC, according to this law. This is a way to sensitize
the various stakeholders to the fact that we are important as well in this field.

## Slide 16

Multidisciplinary composition of EC, just an example.

## Slide 17

Encountered problems on-the-field:

1. *[Compromise between scientific development and patient's benefit]*:
   Reaching this compromise is important.  An EC should be at disposal to the
   scientists working on new topic on a new important implementation. On the
   other hand it's not a good excuse when we are performing and advance
   scientific research and we use human subjects without respecting their
   rights. This should be banned by the public opinion and the scientific
   community. Lo sviluppo scientifico non deve produrre malefici per il
   paziente e pericoli per la sua vita.
2. *[Compromise between privacy and security problem vs advantage of
   data-sharing]*: We have a lever with two weights to be considered. The first
   is the privacy and security issues: they must be respected. It is a
   fundamental aspect in the area of biomedical engineering. But there's also
   the advantage of data-sharing: I should not have big constraints when it
   comes to sharing the data with other colleagues with whom I'm doing a
   research. I have to consider that, instead of saying "this is Mario Rossi"
   we should say "this is patient number 20" so performing data anonymization.
   In a separated table, whose access is reserved to fewer people, there is a
   connection between "patient 20" and "Mario Rossi".
3. *[Trials vs. placebo]*: it is a delicate problem because i.e. we want to
   test a new drug against a placebo. In this way we can evaluate if patients
   who were treated with a drug had really an improvement in their situation
   with respect to patients to whom placebo was given. This is a typical way to
   evaluate the effect of drugs but there's also an ethical problem to
   consider: if we have a drug which is innovative we have to be sure that it's
   not detrimental for the subject and that has a therapeutic effect. On the
   other hand we have to clearly inform the population that there are chances
   to being put in the control group (so to receive placebo). Non è solo un
   problema etico sulla fase dell'informativa del paziente ma ci deve essere
   anche nella fase di designing del protocollo sperimentale.
4. *[Informed consent]*: the subject must be well aware of what are the
   implications of his/her participation to this trial. Generally this consent
   is read by the subject and then signed. The e-consent (aka electronic
   consent) is a matter of study and would simplify a lot all the technical
   procedure.
5. *["Precision medicine" vs "Protocol-based medicine"]*: Two concepts of
   medicine. *Protocol-based medicine*: the traditional medicine based upon a
   series of intervention to be made on the patient in order to care their
   pathological situation. It produces very often some side effects (in the
   same organ or in other compartments). *Precision medicine*: choice of one
   simple drug or one single intervention that could be obtained with progress
   of medicine. In other words to indicate which are the therapeutic
   intervention which could be very important for the pathology producing very
   small, negligible side effects. The goal is to go towards the precision
   medicine, instead of the traditional protocol-based.
6. *[Ethical dilemmas]*: Is it useful for that patient to use mammary
   prosthesis with the risk of breaking of prosthesis itself? What is the
   advantage with respect to the care or the possible therapy of the patient?
   Invasive therapies to CNS: is it correct to introduce a pacemaker into the
   brain for caring epileptic attack? Which are the pros, and the cons? Is the
   patient suitable to be treated with this advance and dangerous invasive
   procedure?
7. *[Preliminary think of the Patient]*: We should not do like Formula 1 teams,
   which sometimes put more importance on the car itself than the one on the
   pilot. We are in contrary situation in which we have to think preliminary of
   the patient and then on the other circumstances. The pilot is more important
   that the car itself but sometimes, as we know, in the very though racing in
   Formula 1 care they care more to cars than to pilot.
8. *[To find good tools of analysis...]*: The network traffic on Google is
   really high. Very often, questions concerning flue, cares (traditional and
   alternative), therapies were searched in Google. Sometimes by searching in
   Google we can predict the onset and the diffusion of an epidemic. At least
   it was demonstrated in some paper.
9. *[Fundamental messages...]*: With ethical consideration we could really find
   out new ways to build medical equipment, medical devices which are more
   people-oriented, better carried on by the patient or could be better in
   which the man/machine interaction could be easier, more efficient, granting
   a better reliability on finding the pathology.

## Slide 18

Big Data in Health environment: important and unique role of BME. Data are
obtained from EHR. If we have a subject in Intensive Care Unit data are
obtained both from EHR and vital signs which are online and monitored during
the course of hospitalization. All this big amount of data constitute the Big
Data in health environment. Our role is their correct treatment. They are well
different from the other kinds (i.e. when compared to the ones of Amazon,
Facebook, etc.). 

Software is considered a MD, even if it seems strange, when it has a medical
application (diagnosis, therapy or rehabilitation). Thus it must be treated by
trained personnel. Software in our area must be tested as a medical device and
must be compliant with regulations of medical devices.

## Slide 19

These are examples of how equipment and software could interact. This is a
wearable device which records ECG tracing in 24 hours (through electrodes),
respiration and movements (through accelerometers). Various output are reported
in the tablet's software. These data could be displayed in real time on tablets
or smartphones (transmitted via Bluetooth).

## Slide 20

Another example of recording the ECG signal on a smartphone via a smartwatch.

## Slide 21

We can track the resting heart rate for fitness purpose thanks to externally
connected devices or to embedded sensor technology.  Cell phone with PPG
transducer + MEMS (micro electro-mechanical system) which basically implements
the accelerometers. MEMS is consisted by a suspended mass by four piezoresistive
beams at the center of the sensor chip. 

Read as is

Everything can be embedded in a small device which constitute this MEMS
accelerometer.

## Slide 22

Contactless medical devices: Videorecording of a face of a subject in which
there are fiducial points. They are regions of the face in which PPG activity,
related to the changing of the volume of the blood depending upon the
circulation which are visible in form of darker and lighter portion of the
videorecording.

These are the required steps to obtain HR from the videorecording:

1. Establishing fiducial points (i.e. forehand, vascularized area of the nose
   or cheek).
2. Enhancement of the color of this videorecording which will present an
   oscillatory trend with a trend similar to the one reported in here. This
   signal is called Video BVP (video blood volume pulsogram). We can easily
   spot the maximal value of this signal (max PPG). It's impossible to see the
   dicrotic notch in the signal coming from the videorecording (differently
   from traditional PPG). 
3. Through this kind of processing we can easily determine the maximum and the
   minimum of PPG, then a cardiac interval, finally the Heart Rate.

So from video in input we are able to obtain HR as output.

## Slide 23

Another topic that regards big data. There are 5 approved studies reported on
this slide. 

Read as is.

It's now possible to conceive big cohort studies enrolling several thousands in
it, having the possibility to reach quite high sensitivity.

## Slide 24

For example this is a Digital AF study with an electronic informed consent.
This project was presented in European society of Cardiology Conference.
Subjects were enrolled through subscriptions on local newspaper article. With
this wide band possibility of advertisement, more than 92k subjects were
recruited through newspaper call. Then they just follow the procedure, download
the app and so on, to make the two measurements per day in a week. These
measurement are performed in a defined day time or with onset of particular
symptoms as indicated by the protocol. There's the step of instant feedback
centralized review summary report.

Read as is

A very strange way of recruiting but the numbers are very high and unreachable
with alternative methods.

## Slide 25

Artificial Intelligence technique applications between correct and incorrect
information. Just a provoking title of fake news.

## Slide 26

Having to deal with such huge amount of data and of patient we are able to
predict with great statistic results which is the hospital mortality, the
unplanned readmission probability in the Hospital and the prolonged length of
stay and final discharge diagnosis. High accuracy on the basis of what's
contained in EHR. This could help defining all the different solutions for the
patients.

## Slide 27

Concluding with some phylosophical statements. Towards an "Integral Ecology"
from Encyclical of Pope Francis.  Integral ethics: the triad is constituted by
man, animal and nature and has to be maintained for the wellbeing of all.

Read as is

> First book that God wrote were not the Holy Texts but the Cosmos.

## Slide 28

There's an EC in Politecnico di Milano.

Read as is

## Slide 29

IRCCS: "Istituto di Ricovero e Cura a Carattere Scientifico".

Monzino Institute: There's a law of 2012 which establishes the EC and there's a
DM (Decreto dei Ministri) aged 2013.

Read as is

AIFA: Agenzia Italiana del FArmaco.
EMA: European Medicines Agency.

Ethical aspects are really important and when we design anything for processing
human data we always should have in mind that the implication of doing it is
for the health of Human Health.

This is **parallel** to the material for passing the exam because the exam is
not about it. Questions will be more on the core we have treated in the course.

# References

Slides: "Ethics"
