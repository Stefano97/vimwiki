# Lecture 2

## Slide 1
## Slide 2

## Slide 3
Adaptive filtering is different from traditional approaches since they adapt to the ongoing signal. 
It is different from kalman and wiener, which are statistical filters. 

Adaptive fitlers require a period of adaptation since information is detected from signal (no a priori information required). Faster adaptations might not be precise enough, but it depends on algorithm used and signal under study. 

## Slide 4
It is useful to reduce noise, enhance SNR when a priori information is not available. It performs well when signal is non stationary. (frequent in practice)

Example. In a stroke we have a change from the situation before and after it and the adaptive filter is able to understand when this change happen.

## Slide 5
The structure of the filter can be: 
- transversal (trasversali) 
- lattice (a traliccio)

Adaptive algorithm can be non recursive or recursive, while the correction criterios is based on the minimisation of the quare of the output error. It is the difference between signal and signal without noise. Correction criterion: output error (outout signal - output estimation).

## Slide 6
signal to be corrected (any biomed signal)
eta is the noise
w vector: series of parameters that weight input signal in such a way that output is the corrected signal. Adaptation happens by adjusting these parameters so to minimise M.S.E. 

## Slide 7
sequence of signals. j is the descrete time expression

## Slide 8

## Slide 9

To find minimum M.S.E. we have to compute value of the weighting vector W such that gradient is 0. 
p_j is the cross correlation between d_j input signal and x_kj reference inputs derived from the signal itself. 

## Slide 10
stationary case: adjustment descending along surface until minimum is reached
non stationary case:  minimum is drifting and the algorithm has to adapt the weights such that they track minimum. 
W_opt = inverse of autocorrelation matrix - corss correlation signal and noise 
the w vector is the same of h in the wiener hopf equation

## Slide 11
\mu controls stability and rate of convergence of the gradient. In reality we do not have Nabla_j but we perform its estimation using LMS. 

## Slide 12
In this slide there are the exquations to obtain the estimated nabla with respect to W of the error evaluation used to update the weights. 
## Slide 13
Convergence can be reached when \mu is smaller than the largest eingevalue of the correlation matrix R and greater than 0. 

To be able to capture all information in the signal we just initialize it. We just need to perform test after otpimisation is completed. We use a priori information to understand the quality of the approach, but still there is no need to use precise model and equations (only an estimate is required). 
## Slide 14

## Applications of Adaptive Filtering (Cohen Paper) 

A. Elimination of powerline interference (50-60 Hz sinusal noise)
d = s + A cos (\omega _0 t + \Phi)   where A, \Phi are unknown and s is ECG without interference

Through adaptive filtering and tracking of \omega_0 close to its nominal value two reference inputs are taken and shifted of 90 degs. Widrow demonstrated that adaptive filter has notch effect on \omega_0. 

Note that a band stop filter is computationally comparable to the adaptive filtering. Here adaptive filtering might be more conveninet for real time applications. 

B. Heart-transplated patient
He might require to separate activity of the new heart from the old heart via catheter in atrium (close to SA node of old heart). Traditional ecg can be used istead, but it is fundamental to recognise and enhance recoring of new heart (depolarizations). 

C. Decreasing presence of high frequency given by electrosurgical equipement 
When using high frquency generators in electrical knifes to avoid muscolar contraction it is essential to preprocess ECG and EEG signals to maintain their information content and fitlering is not performed when not needed. When using a reference electrode we could obtain an activation of the filtering of the knife when it is on. Many systems use adaptive filtering. 

NOTE. When coosing right approach consider ease of use and avoid overcomplicate algorithms when possible. 

# References

Slides: "02 Adaptive filtering", "03 Adaptive filtering Cohen Paper"
