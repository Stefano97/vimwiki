# Lecture 8

## Slide 1


## Slide 2

these methods are very much used for signals that have a time duration which is very long and so also non stationarity could be a proble,. they are very suitable


what is behind this second statement is that certainly the electrical conduction system as we have studied in other courses presents a fractal structure. we have a main branch with different sub branches and every sub breanch resemble the main one. the structure and its morphology (geometry) is the ssame. The fractal are typical of healty situation. if one scale is ill destroied all the other scales remain.

these signals can look like periodic (quasi periodic) and we can say that they have not an erratic behaviour but in reality even HR is not rigolously constant but it has some variability that is due to the fact that heart rate is trying to maintain the equilibrium in the system. there must be a great controlling system and the way it works is that it's using a lot of informations. the variability is the manifestation on this control over heart rate. and this is true for any other signal alike.


if heart rate is variable it's good. its variability on beat to beat basis is a sign of well being. this means that the mechanism that vary the heart beat is functioning correctly. most of the variability is lost for older people.

in sforzi intensi si verifica un aritmia sinusale fisiologica negli atleti allenati, viene alterata la serie di RR.

## Slide 3

Di solito la respiration si campiona alla frequenza cardiaca.


they contain the information about autonomic nervous system.

## Slide 4

medicine is moving from a situation in which we are interested not only in pathology of organs (why and how an organ becomes ill) to the study of the pathology of controlling system. this is a really important trend. Some of the pathologies of the organs are related to pathologies to control system. These pathologies are called dynamical diseases. we can develop hypertension after many year of ??? so the controlling system have a time span of few years. myocardial infarction can happen in a sudden way while a cronic degeneration of tissues due to hypertesion may take several years.

## Slide 5

- *1990*: first time that this group of cardiologist demonstrated that the value of the variance of the HRV was a predictor parameter of mortality of patient surviving Myocardial infarction. a milestone paper because it's demonstrated that if you have MI the variability of HR you can discriminate who is going to survive the MI and who does not!!
- *1987*: it was demonstrated that power spectral parameters are important markers for the simpathovagal interaction. after MI we have unbalanced sympathovagal if the high frequency component is restored it's a sign of getting better.
- 1996: alpha is the slope of the exponent f. its indicating the power law in the loglog scale. alpha is a predictor element for monitoring post miocardial infarction development of the patient.  

## Slide 6
## Slide 7

1. not only there's the concept of non linear system in which we have some sinusoid (amplitude, phase and frequency) that gives rise to the signal.
2. we saw that the heart has a fractal geometry. why? it's important if there's a block in a conduction system of the heart that there are other system equal to the main one that can substitute that part that has damaged. if we work on the ECG we can measure some fractal parameters even from the signal itself. HR time series can show fractal ... read 

In case of pathology the fractality is decreased or completely broken. non sono solo dei parametri importanti per il paziente ma di monitoraggio negli anni.

## Slide 8

*First graph*: we can study it in the short term.
LF --> simpathetic
HF --> vagal component

*Second graph*: long term. longer series of RR. 80k or 120k samples. it's interesting to see how the system is according to the power law or not. it should have a slope that is the parameter alpha we were seeing before. this is reporting the fractal property of the system. if it's a straight line then the system has this fractality. fractality is defined by the self-similarity properties.

## Slide 9

short time vs long time measurements

short:

PSD made by FFT or estimated by AR modeling

## Slide 10

delay maps of the patient on the right. the succession of RR values makes in such a way that the points are in the bisector with a good variability on a beat to beat basis. in normal subject we can see that it's not a straight line at all

in heart failure we have that the points are more near to the bisector.

in severe heart failure we have a concentration of values around particular areas. severe forms of heart failure might produce a variability system that is different from day to night

## Slide 11

overview of the various system

starts from the top.

reconstructing the attractor from the data (we don't have access on the state-space variables) if there is an attractor, of course. it's possible to do that.

the choice of tau is very well summarized.

we can eliminate noise that are superimposed through filtering in space state.

choice of m embedding dimension (number of coordinate to reconstruct the attractor) and the descroption of the m choice.

once this is done we can proceed in measuring the different parameters that are significant to us. 

## Slide 12

A --> z equation of lorentz
B --> chaotic attractor, the trajectory changes but localized in a specific area.
C --> progressing m we are reducing the nearest neighbor percentage because they were false nearest neighbors.
D --> correlation dimension. the flat region is providing the dimension of D2

F --> 3 is the best value of m to estimate the attractor.

## Slide 13

when we want to filter noise that could be biological or non biological if we use a model generation mechanism typical of fractal approach we can't use LP or HP digital filters because we can cut some of the signal. every filter reasons in a different way (state space or in frequency domain). to proceed with the preprocessing (and eventually post-processing for enhancing some information). it works in this reported way: we have the points belonging to the attractor. we can sum the points in geometrical figures covering all the points inside the attractor. er have many gigures and centroids. this figures is accounting for 20-30 points for example. when progressing in it we are covering all the points inside the attractor. what we can say is that we have to consider each domain (1,2,3) and define some directions to which project all the points in each domain. PCA. we consider the true information is 3-4-5 principal components, define them and project the points in these domains. if everything was done effectively we can maintain the basic information and the noise is decreased. we repeat this for all the subdomains.

nu --> number of point belonging to each subdomain

riascolta perchè è fondamentale

## Slide 14



## Slide 15

*graphs on top-left*: the attractor is hidden by the noise. after filtering we can recover the beauty of the attractor itself. then we can see the time series before and after filtering.


*graphs on top-right*: here the attractor is different from the one of before. it is located in the bisector.

## Slide 16

*Graph on top*: D2 dor reduced and normal Ejection function. during day the signal should be more chaotic (more complex mechanism unterlying).

*Graph on bottom*: alpha slope in normal is around 1 with modest variation from date to night.
above all there's no variation form day to night. 

## Slide 17

don't consider the sign of alpha of this slide

normal --> 1
the slope is increasing in hypertensive --> 1,2
heart failure --> 1.29
transplanted --> 1.67

alpha slope is an indicator of progression of specific pathologies. it's introduced in the task force of cardiologists.

===

Task force to which the professor participated. one of these recognized parameters is the alpha 1/f slope parameter recordered over one single day. if we take the power spectrum in log log scale we will see a constant value and around 1. this slope in 24 hours is an important parameter about the variability on a beat to beat basis. It's interesting that PSD is used to understand the rhythms of HF and LF components ???.


the short term power spectral analysis is very much used and explained in the before courses.


The normal sinusal variability is changed with the various pathologies.

## Slide 18

the trend in time of HRV shows characteristic of self-similarities. we can enlarge the signal and see that the behavior is repeating at different scales.

===

another parameter of interest is the so-called temporal autosimilarity: we have the rr interval computed in the 24 hours then we have a magnification of some minutes with a certain pattern. if we magnify again in time obtaining one minut duration we can really see that the temporal trend has similarities. it repeats the same behaviour and it is similar with a specific fractal. if one signal possess this it has a fractal characteristic. in normal sinus rithm we have a power low spectrum, plotting the alpha-slope . this is close to 1 in the normal subject.

## Slide 19

lambda --> scaling factor
h the exponent of tehe scale.

everytime we have a signal like this we have that the statistical distribution of x is equal to the one of the scaled signal.


===

innovative parameter is the ???

the statistical properties are not varying when varying the time scale in fractal signals. this is summed up in the first equation.

## Slide 20

BP --> brief period (minutes or hours)
LP --> long period (24 hours)

alpha is always the gamma coefficent. attenzione alla confusione con questo alfa e un altro che non ho capito
???

## Slide 21

we start from the RR series generally calculated on long period then we can make the integrated series so we sum up all the samples deprived from the mean value. we then measure the trend of this signal y. we find linear segments in every portions and compute the root mean square error between original point y and the trend one. this difference is the way how we could capture the dinamicity and the degree of dinamicity of the time error and we can plot the root mean squared error. questa retta che caratterizza la dinamica delle espressione è un'ottimizzazione che segue l'andamento di sistema. 

===



## Slide 22



## Slide 23



## Slide 24

here all the alpha should be gamma.

## Slide 25



## Slide 26

comparison between health and transplanted patients.
here there's the logaritm of the function F(n).

## Slide 27
## Slide 28
## Slide 29
## Slide 30

# References

Slides: "Biomedical Signal I Non Linear"
