# Lecture 5

## Slide 2

_Non-linearity_: to go deeper in the complexity of the signal. Biomedical
signals are really complex (not linear and not easily describable) with respect
to telecommunication signals that have a more specific bandwidth.

Biomedical environment is not stationary. Moreover stationarity is not a
physiological rule.  The system bioengineers are dealing with is erratic,
dependent from initial condition and on the external environment

_Non-stationary_ condition on the contrary is the rule. A complex system is
unpredictable. Let's make an example: EEG is the epiphany or manifestation of
the functioning of the CNS. The predictability of this kind of signal is very
low.

_Bifurcation_: we may describe this dynamics through coefficients which are
plotted in the state variable hyperplane and we could see that in some cases
all these parameters does maintain in a reasonable confined set of values and
there is a strong divergence in some other causing a sort of bifurcation: the
parameters are not constant around a certain mean value but at a sudden it
moves due to the pathological or physiological event.


We are not using the tools belonging to the linear digital signal processing
because generally non-linear digital signal processing are more complex but
there are many methods which uses these approaches. We confined the non-linear
approach to particular cases of non-linear parameters using a quite common
approach in biomedical series, and also economical series which are similar to
the first one (if you can find a recurrence in the second one we can buy or
sell in a specific period.  If a recurrence is found in the first type of time
series, this will help the doctor with the diagnosis and treatment of the
patients.  A problem in another field could be "how to predict earthquake"? If
we make analysis of features before the event we can really understand if there
are recurrences. We don't approach the problem with the typical tools
("armamentario") but we are staying in smaller area of fractal systems and
Lyapunov exponents [LE] (→ providing measurement of chaos).

_fractal dimension_ → geometrical figures that have no finite dimension.


## Slide 3

The reported graph is representing the heart rate in bpm. Physiologically it's
not constant on a beat to beat basis.

Congestive heart failure (CHF): characterized by a low frequency component that
is quite regular compared to the physiological condition.

Mean value of signals, belonging to physiological and pathological patients, is
basically the same. Sometimes they even share the same variance but there's a
difference between the two signals.


## Slide 4

This slide is presenting the summary of the lecture.

_Invariants_: parameters of functional systems.

The invariants are:

- Fractal dimension
- Lyapunov exponents

_embedding dimension_ → dimensionality of the system in which we describe the
signal.  We need to choose the proper dimension and the time delay, the rate by
which we sample the signal (more frequently or a lower sampling rate?)

_Determinism test_ → we have a signal which is erratic by nature. In ECG signal
we have some parameters that we can measure on the data. Is the signal
deterministic or stochastic? The chaos which is described by a mathematical
equation with mathematical parameters that we as an engineer can use. We want
to be sure that we are in a deterministic approach and not in a stochastic
approach. The assumption is deterministic chaos.

## Slide 5

To completely study a system's dynamics we need to introduce the expression
here indicated.  $\dot{x(t)} = f(x(t))$ and we should measure the evolution of
all the state variables.

We have a lot of state variables. The more the variables the more complex the
system.

Only few of them are measured so we can't have a direct access to the
non-linear dynamical system (NDS) state.

## Slide 6

The variable $y$ is the one at disposal. This is the biomedical signal we want
to operate on. This signal is given by a non-linear dynamical system.  $x_1 ...
x_n$ are the variables that are indicating the state of the dynamic system.
(aka state variables). All together, state variables form the state vector
$\mathbb{x}$.

$y$ is given as a transformation of the state vector in input to $g(x)$.

What is the most common way to analyze it? It is non-linear so it's more
complex than linear system. Non-linearity is a synonym of more complexity.
Non-linear systems give rise to more difficult phenomena. We try to indicate in
the most simple way this system. $y$ is the manifestation of the non-linear
phenomena. We are not able to access the non-linear dynamic system. We can have
access just to the output $y$ to understand how the system is working.

How to process it?

$y$ is merely a sampled signal with period $\tau$ (aka delayed time series).
It's a time series.

It's very important to choose the best sampling period $\tau$.

## Slide 7

- _Statistics computation_: traditional approach of time series analysis. Mean
  variance and autocorrelation.
- _Models identification_: we can go ahead of this simple calculation of
  statistical analysis and try to identify some models which could give us an
  estimation on how the model is structured for simulating a behavior of a cell
  or in general of a system and to predict it properly. The values of this
  signal is related to the fact that the signal is becoming more stochastic and
  it could happen a pathological acute onset (i.e. Epileptic seizure).  Model
  identification of how the model is structured to represent the behavior of a
  system and predict it.  Once I have a model of how the signal is going on, we
  can make assumption on how its future values are going to be.

Lyapunov exponents and fractal dimension are the one characterizing the chaotic
regime.


_Attractor_: evolution of a state variable of a system which tend in the state
space expression either to a single point (1D), cycle limit (2D), or a torus
(3D),.

_Strange attractor_ is typical of chaotic regime: the system does not repeat
the previous path over and over but every time the system makes a different
path in the evolution, being "constrained" to a specific portion of the state
space.

---

_difference between strange and simple attractor_: the attractor is a concept
defined in traditional control theory. The simple attractor is a fixed point:
wherever we start from we arrive to it. All the trajectory collide to a single
point. The simple attractor of 2D will be a cycle limit. The third order simple
attractor is torus. As soon as we arrive to it we would have that the evolution
is deterministic inside this figure. The revolutionary concept of this
adjective strange. There is some unpredictability of this attractor. It's not
easily estimated but there's a component that is not predicted. It's not simple
because we are not doing the same trajectory but it stays in the same portion
of the space. This is a particular behavior.

---

How do we measure chaos? We have the geometrical dimension and the dynamical
dimension of the chaos and are here reported:

- _Fractal dimension_: Non integer portion of a state which represents the
  system equilibrium. This strange object of different kind of geometry has a
  fractal value, a non integer value. Traditionally the simple attractor with
  integer dimension (1,2,3 dimensions). Fractal has **not integer** dimension.
- _Lyapunov Exponents_: we can give a signal and represent it as the sum of
  Lyapunov Exponents and if, among them, there is at least a positive one, then
  there is a divergence in the trajectory of the attractor. If there is a
  negative LE we have the convergence of the trajectory of the attractor.

The important thing for having a chaotic regime, so to work with our
assumption, is that at least we **must have one positive Lyapunov Exponent**,
so at least one of the state variables under the form of the Lyapunov exponent
present a divergence of trajectory so there's a sign of chaos.


## Slide 8

The principal tool is the reconstruction of the state of the system in the
space of delayed output because what we have is basically this signal $y$
properly sampled with sample interval $\tau$. We are called to reconstruct what
it is called the state representation, the state reconstruction. The function
$f$ and $g$ are not known, also the dimension of the state space is not known
(how many state variables are to be considered). And if we assume that the NDS
who generated the series is on an attractor thus the attractor can be
reconstructed through the delayed coordinates method (time delay
reconstruction). The signal $y$ helps us in finding a signal which is the one
on which we work and try to process it in order to obtain the parameters that
are typical of the chaotic regime, the two we have seen before.

This signal considered in the delayed timestamps is the reconstruction going to
estimate in the space of the delayed output. The outputs are delayed and could
be shown in this multidimensional diagram up to N (number of points that we
have) and this system can be shown in a delayed output domain. Each axis is
constituted by each value. The basic approach is the state reconstruction.

If the dynamic of non linear system is situated on an attractor (the equation
that illustrates the functioning is indicated in the state-space form under the
form of an attractor, that is the orbit that attracts towards either a cycle
limit, torus or single point or a more complex attractor) thus this attractor
can be reconstructed through the delayed coordinates method.

**Important message of this slide**: The delayed coordinates methods: this $y$
signal is properly sampled with tau and we obtain a discrete signal which tells
us more information about the complex model underneath the signal. What we
assume if we want to demonstrate that we are in presence of non-linear
dynamical system and with the series is on an attractor that can be
reconstructed through this delayed coordinates method. From these values we can
reconstruct the characteristic of the generating model. A very complex model
can generate the signal which is defined by properly sampled values, like the
one on which we operate.

## Slide 9

A knowledge of the delayed output vector of dimension $n$ is given by these $n$
values of $y$ which is the sequence of the information that we have at disposal
here. These values allow us to reconstruct the system state. There's an
observability matrix and this is quite theoretical statement to which we are
not much interested to and the main thing from automatic control science that
if there is this observability matrix it means that this sampling of $y$ signal
is equivalent to the system vector state, to the information of how the first
block is developed!

We are writing it in a different way but it's the same equation as the one seen
before. The number of these samples in here is $n$.

We are not going to demonstrate this but it's possible to reconstruct the
system state with the osservability matrix. If this condition of the
osservability is satisfied, the delayed output vector is equivalent to the
system vector state.


We have $n$ values of $y$ which is the sequence of information at our disposal
that allows us the reconstruction of the system state.

## Slide 10

What is chaos? A little bit of chaos is healthy for every physiological system.
Why too strict synchronization is synonym of pathology?

Investigating the behavior of system in the state-space plane. The concept of
attractor is that if we are in the starting condition close to this yarn
(gomitolo) the system will evolve and position sooner or later inside the yarn,
in a confined area of the state space but in an unpredictable way. It's very
difficult to estimate the whole system but we can start from one realization
$y(k)$ and build up a time delayed variables ($y(t), t-tau, t-2tau ...$) and we
can have a description of the system with a different attractor that share the
same properties of the yarn in the state space. We try to stay as close ad
possible to the area to permit a better convergence to the attractor. We are
just considering the non linear system that can be described as an attractor in
the state-space domain.

How is practically used this requirement? It's obtained through the time delay
reconstruction.

Let's consider a system that is characterized by this differential equation:
$\dot{x(t)} = f(x(t))$. This is working on an attractor A belonging to $R^n$ of
dimension $d$ obtained to box-counting dimension.

Box-counting dimension: a simple way of calculating the geometrical dimension.
If we consider the state-space where the signal is expressed and we count how
many points are included into each elementary block constituting the whole
geometrical figure, we can determine the dimension. We are going to make the
dimension as small as possible and see if there is a limit to this box-count.
Though this value of this limit has a geometrical meaning and we could obtain
through this delay reconstruction some estimation about geometry. We can
measure the system output (the time series which is a scalar one) and fix an
integer $m > 0$ and define the delayed outputs. (we have $m$ values of the
output).  In other words: if we have a geometrical figure and divide it in some
squares we could calculate the number of points of the process inside each
squares by changing the dimension of these squares.


The function G can be defined as expressed in here. $m$ that is the number of
steps in this vectors. If $m$ is called an embedding dimension for the
attractor A in the cases in which the function G has a one to one
correspondence in A, in other words we can have the system with a state
variable $x_1 ... x_n$ and we can have a series of point which evolves in time
we can have a certain number of points which can constitute an attractor, like
in this case. Not always this is possible but when it happens it has important
geometrical and dynamical properties that can help us discriminate
physiological from pathological conditions. Attraction → we have that time
development of the system is in such a way that if we observe the system in the
state-space we can notice that there is not a random filling of all the point
in space but there can be some attraction in some part of the space. If the
system does evolve in time it means that it does not go away from a certain
volume.

A good model is the one based on the delayed output. Our hope is to find out a
method.  Embedding → how can I obtain a geometrical representation in the space
once we have the point and the different variables (una rappresentazione). The
question is: the system having 3 different variables (as the reported example)
and the representation in this space domain, if I have an attractor this means
that the orbit that evolves in time is evolving on a basin of attractor. What
we have access to is the $y$ signal with a delayed output and I can obtain
another attractor in another reference system. Through this reconstructed
method I can obtain another yarn in state space variable which is not equal to
the one in the state variable space but it has the same geometrical properties,
same fractal dimension (→ geometrical characteristic) and Lyapunov exponent (→
dynamical characteristic).


We start from an attractor in state-space domain and we can work in the time
delayed output and the evolution in time will provide the same attractor in
here and on the $y$ signal we can measure the non linear invariant parameters
of Lyapunov and fractal dimension that are the same of the one in the
state-space.


Knowing nothing about state variables I could simply work on a realization
sampled with $\tau$ and build up an environment of dimension $m$ in which I am
inserting this attractor without changing this invariants.

## Slide 11

Embedding means reconstruction of an $m$-dimensional space.

Takens theorem claims that there's an equivalence between the original
attractor in the state space and the one in the delayed output space. The
invariant properties are maintained.

$m$ is the embedding dimension. Simply the $m$-dimensional space through which
I'm describing the variable.


A way to capture the dynamics of $y$ is to sample it with tau steps in order to
have what's called the representation of signal $m$ under the forms of
embedding dimension. Embedding → dimension in which I include the original
signal. I have a signal and I want to define the number of dimensions though
which I want to study the signal. How many points should I need to reconstruct
correctly the system? If the system is complex $m$ should be high, otherwise
smaller. Tau is the sampling period.

We are basically dealing with a sampled signal but with non linear signal we
use to call different expression. In engineering subject it's just a sampled
signal and this discrete signal has a numerosity of $m$. $m$ is the embedding
dimension remarking that it's the number of coordinates through which we are
compressing the information about the signal. Takens in '81 is saying that this
signal whose dynamics is unknown to us in a direct way is equivalent to the
dynamics of the signal in here ($y$).


Knowing nothing about state variables I could simply work on a simple
realization sampled with tau and build up an environment of dimension $m$ in
which I am inserting this attractor without changing this invariants.

## Slide 12

Given the time series (for positive and negative values of tau) and the
embedding dimension $m$ the series $s$ is then constructed. We can describe the
signal and its evolution in time under the form of finite samples. The basic
points is that the reconstructed attractor has the same dimension and Lyapunov
exponents of the original one. The thing I want to pretend is the so-called
invariant parameters, which characterize the dynamical system, need to be the
same.

## Slide 13

Number of variable through which I'm using must be higher than [listen to
second part].

Embedding dimension must be higher than the number of dimension.

If we reduce the embedding dimension we can have a self-crossing. It seems to
be close but it's not → false neighbors.

## Slide 14

In the reported graph: $b$ and $c$ realizations are projections on the two
different planes (1-2 and 1-3 respectively) of the three dimensional
representation A. In these two-dimensional representation we can notice that
there's a false neighbor in $c$: the intersection is indicating that the
distance between the two points is zero but that's not really true when it
comes to 3D.

## Slide 15

_Takens' theorem_ historically derives from Whitney and Mané. This theorem that
is now known as Mané-Taken. If A is an attractor of dimension $d$, $m$ is an
embedding dimension if $m \geq 2d$. This is sufficient condition but useful to
find a smaller embedding dimension like $d\leq m\leq 2\cdot d$.

In real experiment $d$ is not know a priori.

## Slide 16

Lets start with a simple signal: the cardiac beat of insect. This cardiac beat
of insect is measured considering a technique based on impedance.  Measuring
the impedance in the body of the insect with respect to the cardiac beat (?).
What is obtained is a signal in which the impedance is reported on $y$-axis and
$t$ on $x$-axis of the graph. At each cardiac beat we have a deflection and
inflection of this impedance values. Through the impedance we can really test
the heart rate of the insect. If we want to display the signal into an
embedding dimension $d = 2$ we have to use state variables and plot the
impedance values of the one obtained at a certain instant $t$ versus what was
obtained in the previous instant ($t - \tau$). We have to sample the data in
order that our embedding dimension is more favorable to our needs.

For the analysis of this slide we can consider that $\tau$ should respond to a
valid criterion to better represent the signal. The scatter plot that we obtain
(instant $t$ vs previous instant $t-\tau$).

If we use $m=3$ we should see a different plot. For high embedding dimension
can not be easily accessed by eyes and there are some numerical methods to see
if there are self-crossed values or false neighbors.


If we use just two coordinates (if we have sequence of this samples plotted we
can see that in 3D the geometry of the figure is clearer and we can see points
that are true neighbors and the ones that are false neighbors. This is the best
way to find out the embedding dimension. How much can I increase $m$ not to
have false neighbor?  I keep on measuring the euclidean distance in the various
points and when, passing from $m$ to $m+1$, we don't see more difference in the
euclidean distance we have reached the maximal description of these variables.

## Slide 17

*Estimation of the space dimension with False Nearest Neighbors (FNN) method*:

1. all dinamicity of the signal is compressed in the $m$ dimensional space. The
   same operation is done considering $m + 1$.
2. The euclidean distance between one point and all the others is computed. The
   percentage of **CLOSE** points (whose distance is lesser than a given
   threshold) is computed for both $m$ and $m+1$ domain.
3. If close points in $m$ become distant in $m+1$, this means that they were
   close in the first instance because of a projection effect (the orbit was
   reconstructed in a space that is too small).
4. Repeat this procedure increasing $m$ until there's not difference in the
   number from $m$ to $m+1$.

The graphical representation of this method can be summarized in a graph in
which $y$-axis is constituted by FNN (False Nearest Neighbor) percentage and
x-axis is the embedding dimension $m$.

First graph: Well known chaotic system made by Lorentz. We see what is the
smallest m for which we have not the FNN. We can use $m=3$ in this case → we
have the possibility of correctly estimate the dimension. As we have seen the
dimension of Lorentz attractor is 2.06. So with embedding dimension 3 we have
really the possibility to correctly estimation the dimension of 2.06.

Second graph: same representation for a patient (RR series).  In a real
condition the system could degrade because of white noise.  Basically we reach
a negligible number of false neighbors with an order of 5 or 6 which is really
significant for the HRV. The presence of white noise, third graph, is inducing
an overestimation or a different estimation of the true number of the $m$,
reducing the performance of correctly estimating the dimension. We want to
clean this noise but we will see how to perform the cleaning.

There are some paradoxical situation in which maybe to find chaotic behavior a
little bit of noise could be advisable. In fact if we have some very precise
mathematical equation without any contribution of noise from outside probably
it's more difficult to characterize it through a non-linear dynamical aspect so
that it's defining a strange attractor as we have defined previously. In any
case that would be a discussion on the application that we will do in the
following lessons.

## Slide 18

How do we choose $\tau$? According to the theory: almost all the values of tau
can be chosen but from a numerical point of view we have something more than
others. Time series generated by Lorentz can be plotted into x and $y$
(original attractor). The _reconstructed attractor_ deduced from these simple
mathematical system has only the variable x sampled with a step tau. So we can
make a scatter plot ($x(t) vs x(t-\tau)$).

The slide can be interpreted as follows:

- *graph (1,1)*: representation of the actual Lorentz attractor in x-y space.
  If $\tau$ is equal to 1 we have a situation like the one on the right first
  row. It's a sort of bisector.
- *graph (1,2)*: reconstructed attractor ($\tau = 1$) in state-space domain.
- *graph (2,1)*: reconstructed attractor ($\tau = 15$) in state-space domain.
  We have graph row 2 col 1. This pattern is closer to the real one. The orbit
  is kind of constant.  Spectacle-like pattern. The dynamics and the morphology
  is pretty similar.
- *graph (2,2)*: reconstructed attractor ($\tau = 200$) in state-space domain.
  We have a sort of spectacle but much more confused and hard to say any
  attraction phenomena


## Slide 19

Several empirical methods have been proposed to identify the most appropriate
Time Delay:

- Generally it's the first zero of the autocorrelation function (ACf): I sample
  the data and don't introduce any correlation between one sample and the other
  because the correlation between the two samples is zero.
- Time associated with the first minimum of MI (mutual information) function:
  not only is the classical linear system parameter but the mutual information
  which is the analogous of ACf for non-linear parameters.

While for us it seems that it could be fundamental a correct choice of this
$\tau$ in time the experts in non-linear dynamical system say that it's not so
crucial. We would like a sampling rate that causes as small perturbation as
possible to the original series. So the rationale of the choices made before is
the fact of basically minimizing the correlation between two consecutive
samples.


## Slide 20

We don't want to confound that of a random behavior from a deterministic
behavior instead. Non-linear determinism: We want to find relationship on
signals that appears to be stochastic but in reality they are deterministic,
simply they are chaotic. In science there are not only signals which are
deterministic or signals which are stochastic but there are also chaotic
signals.

- **Stochastic**: like tossing a coin. Obtaining sequence of values which are
  purely or mainly random. We can have probability function that measure the
  fact that the signal is absolutely a roulette type sequence of numbers or a
  tricked (truccata) roulette type numbers. The stochastic nature is an
  unpredictable and random sequence of numbers, more or less defined.
- **Deterministic** signal are defined by precise mathematical laws.
  Determinism means that we could, if we found the law by which there is this
  evolution, capture the complete information.
- **Chaotic deterministic**: they are deterministic signal but they appear
  **erratic** in such a way that show that the system is more complex that we
  can think.

By looking at one of the attractor that we have leaned we can see that is
impressive that this very complex behavior could arise form such simple
equations in which the only difference is that a small non-linearity is
introduced in the system. A small non-linearity could introduce in the behavior
of the system great changes due to non-linearity itself. In linear systems we
have that by providing small changes we can only have small changes in the
output. A butterfly in Europe could cause tornado in China.

The key question is the fact that there's a kind of determinism but is it
chaotic or a normal one or there is not (only stochastic behavior)? Are we sure
that this erratic behavior is due to a chaotic system or it's not due to a
random system?

## Slide 21

To answer these question we use *Surrogate Data tests*. There is a null
hypothesis: the one we want to reject.  The *null hypothesis* is that the data
were generated by a linear stochastic process.

Surrogate data: series of random data that shares with the original data some
linear properties. I have changed the original sequence thus maintaining some
invariant properties of the series, like for example MI or ACf.

**Typo**: $x(t)$ for $t = 1, 2, 3, ..., N$

## Slide 22

Test of hp uses a discriminating statistics that is a number or a function that
quantifies some property in the time series. It can be ACf or the MIf and they
are reported in the slide.

_Rejection criteria_: it specifies for which values of the discriminating
statistic we reject the null hp.

## Slide 23

We could measure as indicated in here the percent variation of the MI.

$\overline{I_{sur}} (\tau)$ → mutual information calculated on surrogate data.
M is the number of point considered

$I_d (\tau)$: Difference between original MI and surrogate MI.

$I_m$: mean value of the original series

$\Delta I_{\%} (\tau)$: delta in percentage of mutual information.

In this way we can see that the MI in surrogate data is certainly decreased,
while if we had no information on linear importance in the data if we randomize
it we don't change too much the dynamics of the MI. Only after surrogation we
see significant changes of MI that could be a significance of the presence of
the non-linear information over the original series.

To sum up the procedure:

1. We start from a time series, we then measure $\bar{I_{sur}} (\tau)$
   (surrogate series: i.e.  randomization of the phase: maintaining the modulus
   of the series but changing the value of the phase (multiplied by a certain
   random series to obtain the surrogate signal.).
2. We measure the difference of the mutual information and the mean of the
   surrogate.
3. We measure the mean of the original mutual information and keep it as a
   reference point.
4. We measure then the percentage difference as the ratio between the
   I_difference divided by the reference value.
5. if delta is very high we have a considerable change when passing from the
   original to the surrogate data, so we have chaoticity. If the original
   signal is random, we have not changed it so much.


## Slide 24

The uses of surrogate data are methods to see whether we have chaotic series
(aka non-linear information) in the signal. The most popular methods to do that
are:

- Phases randomization: The one described before. We keep the modulus of the
  original time series and randomize the values of the phase. In this way we
  have altered the original signal without changing its properties too much.
- Amplitude adjusted Fourier Transform
- Recursive AAFT **Schreiber**

## Slide 25

How is generally produced? We start from the original time series. We generate
the surrogate data of the original time series $S(t)$. Then we make a control
of the surrogation. In the first step we consider the value of the ACf and if
ACf is high (variation higher than a reference. Depending upon the severity of
the criterion to generate random behavior or chaotic behavior which is a big
difference) it means that we have changed considerably the linear behavior, so
we have made a bad surrogation (ACf is a parameter regarding linear behavior).
We have to make a generation of surrogate data of the original series with a
different approach. When we get a low variation of the ACf (having a time
series which, for the linear aspect, is not changed with respect to the
original one. We didn't destroy the linear links. We should only change the non
linear links.).  Then we consider the variation of the mutual information. If
high variation of MI (>10%) deterministic dynamics, otherwise non deterministic
dynamics.  Obviously the mutual information should be higher than a certain
threshold value depending upon the severity that we want to provide to the
algorithm.

## Slide 26

The variation of the MI and ACf in simulated systems.

1. chaotic systems (first row on the left): they are non-linear system by
   definition. In the chaotic systems we have a MI which present drastic
   changes like the one that we have seen in the preceding patterns. The first
   one is due to the Ikeda chaotic system and the other for the Lorentz
   attractor.
2. linear systems (two rows remaining): We see that basically there's a poor
   difference of the MI and the ACf which is the one indicated in red and in
   stochastic process we can imagine that because the signal is already
   stochastic so intrinsically it does not provide any significant variation in
   the estimation of the system.


## Slide 27

Example of application on a HRV series. The preceding slide was for simulated
systems and now we are seeing the real HRV series. Surrogate data are generated
with the AAFT method in the top and with Schreiber algorithm at the bottom. The
difference is not so big as they are two algorithms that Schreiber proposed to
making a recursive estimation of the variation of the parameters. The results
are very similar. Red is MI computation with 10 bins, Black: MI computed with
50 bins, Blue is ACf. We can demonstrate that even HRV has a non linear
component that is worth being investigated through one of these approaches.
That's why this way of approaching could be quite interesting. It is
interesting to see the percentage variation of MI, presenting a maximum in the
first lags of $\tau$. All the data basically have this pattern which is quite
repetitive in heart rate variability (normal, myocardial infarction and Heart
transplanted). The difference reported should be seen considering the different
graphs belonging to one single line (i.e. from normal to transplanted heart).

Description of graphs:

- $y$-axis: *Difference (in percent)* of mutual information. Remember what was
  said in slide 22. Higher percentage means that the original data are chaotic,
  so it's worth analyzing with these methods.
- $x$-axis: time lag.
- Colors:
	- Red: % Difference in MI (10 bins).
	- Black: % Difference in MI (50 bins).
	- Blue: % Difference in ACf.

## Slide 28

These results suggest that we can reject the null hp for HRV. There is a
non-linear characteristic is worth to be studied. HRV is the marker of the ANS
behavior, so it's very likely that there is some non linear phenomenon of
control. Up to now if we measure a power spectrum parameters (LF and HF) we are
limited to observe linear parameters, because we measure it in the classical
domain in time (autocorrelation) or frequency (Power Spectrum) transformation.
The linear approach has be demonstrated that it's useful to study HRV. We can
also say that there is even a component which the non-linear algorithms have
put into evidence and which is worth to be investigated. In fact it's about 25
years that the non linear aspects related to HRV have been studied. It's not
true that basically the non-linear theory is better than linear theory because
it was lately discovered. Linear approach is very spread. In some cases the
non-linear approach could provide a better analysis. The various pathologies
could be better singled out (individuate) through non linear parameters because
maybe they affect more them. Every good cardiologist know the tools at disposal
(ECG patterns and so on) but he could also have access to the time domain or
frequency domain parameters introduced by task force on the linear aspects of
processing of HRV but also, third step, he could also have access to non-linear
parameters that could provide a different information (i.e. degree of
re-innervation of transplanted heart → how the patient's physiology was
modified after the intervention). The fun is to be able to demonstrate that
maybe a very complicate original parameter could have really important clinical
findings.

- Pathological subjects' walking: when we measure the walking of the subject we
  measure the pace of the walking steps and for example in Huntington disease
  this sequence of walking steps is different from the physiological one. It is
  like the heart beat that could be different on a beat to beat basis for
  normal or pathological patient. The step of the walking of pathological or
  normal could be different. In that sense it's a very interesting extension of
  the RR series to the step by step walking of a subject with various
  pathologies at the peripheral levels.  I EMG: it could be studied with a
  similar approach
- Other signals of cardiovascular variability (BP, BPV, Respiration
  variability). These physiological parameters could have important
  interdependencies among each others and suggesting important clinical
  findings. The important thing is to be able to demonstrate to be able to
  quantify properly these links (ANS and Respiration, ANS and renal function)
- EEG: the question marks are put there because EEG is very complex phenomenon
  with a lot of degrees of freedom. EEG could be really considered even under
  the form of studying of power spectrum. The power spectrum of EEG is a
  classical parameter that is studied in the frequency domain. I lot of work
  was done also in time domain. The question marks regards the correct
  measurement of chaotic parameters inside the EEG signals. There's a question
  mark because EEG is the result of a very complex system of millions or more
  than millions of neurons that are connected to each other so sometimes it's
  very difficult to discriminate in this very complex phenomenon the components
  which are random and the ones that are deterministic. It appears that the
  behavior of EEG is erratic but maybe it is due to stochastic behavior of this
  10^10 neurons inside our brain. Io it's difficult to disentangle the
  non-linear component from the stochastic component. Chaos for EEG signal is
  not very widely considered because for sure the background EEG is very
  complex and the possibility to look at some chaotic or non linear stuff that
  can be measurable. Not only to say that we are in low-dimensional
  deterministic chaos but that we are able to measure. These circumstances are
  not so many in the study of CNS. Typically in some pathological cases like
  epilepsy: there's a strong simplification to the complexity of the brain,
  giving rise to a rhythm that is almost periodical at 2.I Hz which is a spike
  wave morphology. In this condition we could probably observe some chaotic
  behavior in the brain and we would be able to measure it but we have to pay
  attention that it's an exceptional case in which the decreasing of the
  complexity allows us to make this measurement. In normal circumstances, even
  a drastic decrease of complexity is a problem for having to deal with a lot
  of variables and it's difficult to say if we are in presence of stochastic
  component or chaotic component.

## Slide 29



## Slide 30
## Slide 31
## Slide 32
## Slide 33
## Slide 34
## Slide 35
## Slide 36
## Slide 37
## Slide 38
## Slide 39
## Slide 40
## Slide 41
## Slide 42
## Slide 43
## Slide 44
## Slide 45
## Slide 46
## Slide 47
## Slide 48
## Slide 49
## Slide 50
## Slide 51


# References

Slides: "8 Nonlinear&Chaos"
Cerutti's Book chapters: 14
