# Lecture 3

## Slide 2

heart variability signal.  we have a patient in resting condition. after 300 ms
the patient is standing up or orthostatic position (also with the tilt test for
instance).

tacogram → response of autonomic control (increasing of simpathetic
activity). it's very visible in th epower spectrum in the lower part of the
slide

LF → simpathetic activation HF → vagal

Simpatho-vagal balance (simpathetic and parasimpathetic, the last one coincides
with vagal when dealing with heart). Stationarity is difficult to mantain in a
biological signal. generally in biology non stationarity is a sign of health.
There are at least some variability in physiological parameters. But of course
we should consider stationary signal in order to produce the analysis,
otherwise it's too complicated to deal with them.

In this analysis we are seing the 2 events of lying and standing.


## Slide 3

resuming the concept of power spectrum.

spectral estimation: there are non parametric methods (based on the fourier
transform or the autocorrelation funciton, wiener-khinchin. Direct [with the
computation of fft] and indirect [passing from the autocorrelation function in
time domain and apply the wiener-kinchin method and perform FT of
autocorrelation to obtain the power spectrum]) and parametric (before computinf
the spectrum we use a model to define the spectrum. there are many models
(autoregressive approach, bayesian approach and so on). PSD is estimated from
the model parameter we have obtained (leggi sotto))

## Slide 4

we can transform or antitransform depending on where we start.

$a_{n}$ = scalar product between signal and orthogonal functions indicated,
which in the simplest case sinusoids. orthogonal means that the squared of the
sum of the 2 is equal to the sum of the 2 squared.

## Slide 6

Sinusoids are **not localized** in the time domain because they could be
defined from $-\infty$ to $+\infty$.


The signal **stationarity** is required otherwise we don't have a simple
representation of it.

## Slide 7

if we want to see a phenomenon in a varying time window we are not able to do
that in the case on the left.


Peaks are wider on the right rather than on the left but we *can't have
information on the time when the event is occurred* by just having the
frequency information.

## Slide 8

we are not able to recognize that the signals are different when we are
considering the overall PSD of these two signals on left and right.  We need to
compute a more different analysis by changing the point of view.

## Slide 9

by working on 1000 points we could not be able to find out where the two
different peaks are arising, we are losing the time information.


## Slide 10

REPRESENTATION IN TIME **OR** IN FREQUENCY DOMAIN → this represents the
traditional Fourier approach to the problem.  REPRESENTATION IN BOTH TIME
**AND** IN FREQUENCY DOMAIN →this is interesting and an important further
improvement.  the following are 2 methods to consider time and frequency domain
together.

- Linear Decomposition of the signal:
	- STFT (Short Time Fourier Transform) → Spectrogram. A study of the
	  signal in both time and frequency domain.
	- WT (Wavelet Transform) → Scalogram. A study of the signal in time
	  domain and in scale domain (which can be compared in terms of
	  frequency but it's a bit different).
- Quadratic Energy Distribution: (→ similar to computing directly the power
  spectrum)
	- Wigner-Ville Transform.
	- Time-Frequency distributions (Cohen's classes).
- Time Variant or Adaptive Parametric Models: extending the time frequency
  observation to adaptive modeling in which we have how the characteristic in
  time and frequency domain would develop in time and frequency. ? also
  repeated in part2, circa al minuto 15.


## Slide 11

from the spectrogram.

signal is seen as a sum of gabor coefficients


remind quite well the fourier analysis and it's synthesis expression.

## Slide 12

stationarity is required when we want to compute the PSD.

STFT: segmenting a temporal window into small windows finding the PS (power
spectrum) of something i can consider stationary. We can do that with or
without overlapping. We have to choose the length of the window can by
considered stationary and in which we can consider it time invariant. We
multiply the signal by a time moving window.  So not only the STFT is going to
be a function of the frequency but also of the time lag. If the signal is not
so stationary i'm going to make some errors and we have to keep this in mind
and report it in the results.


function of tau and f.
the squared STFT is representing the Power spectrum distribution and the spectrogram.

There's a difference between passive and active orthostatism:
1. Passive (rest to tilt) → in 15 to 20 s from horizontal to vertical
   position with harnesses (imbragature).
1. Active: The patient is asked to stand up from a resting condition. this
   implies patient dependent conditions as well to be considered.

with the moving window we can measure also the

transient between having one particular psd to another one.


## Slide 13

tau is the running variable, it's a sort of delay of the sliding window.

STFT is the fourier transform of y(t) opportunamente multiplied by the window g.


The reported graph is the time-frequency plane.

this STFT is intended to put the (???).

## Slide 14

basis funcitons will not be sinusoids. these would have to maintain some
characteristics of the frequency and some of the time. Representation of the
gabor's coefficients. we have different values of frequency component. the
envelop is a gaussian expression. The window is not changed in both the domains

in time they have this kind of damped sinusoidal behavior.  in frequency domain
they are classified having a gaussian similar expression with a central
frequency centered.


## Slide 16

Gabor's atom of description are compromise of frequency and time resolution. if
we want a high frequency resolution to discriminate the frequencies (maybe in
epyleptic signal) we pay with time resolution. this is viceversa.

## Slide 17



## Slide 18

dirac → continuous
croneker → discrete

## Slide 19

1 point in time → optimal time resolution but really bad in frequency.

## Slide 20

## Slide 21

let's conside 128 points equal to 1. we have a good frequency resolution (many
points in time, this means a higher resolution in frequency).

## Slide 22



## Slide 23

so STFT is the function we use in order to discriminate the different portions
of signal in the time frequency domain.

compromise: not too high number of samples nor to low

## Slide 24

by reducing the number of samples in the time window we are able to spot more
precisely the two portions.

the less samoles the worse the frequency resolution, the better the time
resolution


these are 2 atoms of activity well distincted.

## Slide 25

another example in which we have sum of 2 sinusoids whose frequency vary
linearly with time. There are a presence of 2 sinusoids.

temporal window that should be a compromise on time and frequency resolution.
we need to define well this length in this kind of analysis. this is not done
practically in the traditional fourier approach and we consider the whole
signal.


the frequencies of these 2 sinusoids inside the signal have frequencies that
are linearly increasing

## Slide 26

by increasing the time samples in the window we have a better frequency
resoluton.

## Slide 27

acoustic signal of pronunciation of GA-BOR.

## Slide 28

on vertical we find the harmonics of ga-bor and on horizontal axis when they
are happening.

In this representation the height and the weight are constant → same
resolution, not varying in time and frequency.

time vs frequency vs spectrogram modulus.

## Slide 30

da riascoltare

something could be improved in this analysis.  in order to stay in the forurier
transform the big problem with this are indicated here.

1. STFT was born trying to satsify this need. bio signals have different
   ocmponetns. in vep we have 3 parts: first high, second mid and third low
   frequency.
2. short lasting component contain high frequency and long has low frequency
3. We have to mixup some a priori knowledge in the field with the theoretical
   aspects in the algos. Combination of methods and routines already existing
   with a priori knowledge thta someone has never considered using.
4. Do not be afraid of making mistakes. Different parametrization even in the
   limit cases and moving from it. trial and error.
5. if constant resolution of time and frequency tipical of STFT we can use
   Wavelet.

## Slide 31

The basis functions instead of being sinusoids or gabor funcitons with the
compromise between f and t resolution, wavelet aredifferent realizatrion, not
necessary sinuosids but there are different atoms. the general aspect is that
we have anon stationary signal and it's in a way that the sstatistic of the
beginning is different from the middle part and so on we have to use a
different approach. so the atom (t f atoms) in here they are different and
could have a mathematical expression quite general. we can choose our wavelet.
the jypothesis of the wavelet should be valid though. provided that it respects
the general rules of being a wavelet without introducing distoriton. Every
function is obtained by expanding and shrinking the mother wavelet. funciton of
a and tau; a is the amplitude of the wavelet, t is the temporal variable and
tau is the sequence of tume values inside a limited time mathematical function.
tau is the current time variable inside this small signal. how can we consider
a signal as a decomposition of this kind of basis function?

== in the previous lesson

i can decompose the signal not only in terms of gabor coefficients but also
with different functions. the concept of wavelet is similar because we start
from the mother and go to the daughters. the mother reproduces itself with the
same morphonogy on different time scales. it has a finite duration and a
mechanism of oscillation. it has intrinsically both time and frequency
representation. we can see that if we shrink or dilate we obtain a frequency
representation of this kind. compressing the wavelet in time we have a
dilatation in frequency (look at the image in the slide).

basically tau is the delay and a is the scale

## Slide 32

It's not true that any funciton can be considered a wavelet but there are some
rules to consider
1. zero mean ad finite duration.
2. The inverse transformation is required so the integral of the abs of window
   is 0
3. energy of the signal should be finity (integral of the squared signal).

In gergon we say "if you are looking for a submarine use a wavelet of a
submarine".

== in previous lesson

h(t) should be considered according to this conditions.

1. it has  zero mean so we are not injecting a mean value
2. integral of modulus is less than infinite → required to have an inverse
   trasformation
3. energy of the signal should be finity (integral of the squared signal).

## Slide 33

a coefficient is critical, this scale that can enlarge or shrink the mother
wavelet.

wavelet transformation is also called scaling transformation. scales are
substituion of frequency.

the grid is going to have a different shape with respect to the one we are used
to with time-frequnecy analisys.

## Slide 34

a can be augmented → compression decreases → expansion

higher scale → general view smaller scale → detailes.

## Slide 35

we have  time scale diagram.

we can now consider the region of influence of a dirac pulse

in the wavelet transform we have a better resolution for low frequency and a
worse resolution for higher frequency.  delta di dirac in wt → instead of
having the same incertainty interval of estimation in time we have different
resolution in time and we have very good results for very low scales and this
result will worsen when we increase the level of scale. scale is inversely
proporitonal to frequency so the worse sime resolution is related to small
frequency as we were saying.

in STFT we have a constant temporal resolution both if looking in time domain
and in frequency domain.

## Slide 36

WT is funciton of time and scale.

varying both in a and t.

time window duration is increasing with the increase of scale

## Slide 37

morlet → french mathematician. sometimes it's pronounced morlet as in
english.

wavelet → ondine or small oscillations.

FT → mexican hat.  damped sinusoid with a gaussian envelop.

## Slide 38

formulation fo wavelet transform in term of frequency and time depedency

## Slide 39

different fourier transform of the different wavelet.

## Slide 41

this is the tipical srft subdivision into atoms that are constant. in wavelet
transform we have basically the rectangles with different shapes. the area is
the same but the area are different. A variable time resolution for the
different frequency and the same with the other reference. the q factor is
constant that is f/deltaf.

attenzione che in WT è stata sostituita la frequenza.


# References

Slides: "04 Time Frequency"
Cerutti's Book chapters: 9
