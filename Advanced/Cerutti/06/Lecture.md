# Lecture 6

## Slide 1

as we will see it constitutes a very interstin part of application of BSP. amu biomedical signal possesses a fractal structure. There are important applications in these areas. this concept of fractal modeling are used also in some application area.

## Slide 2

- we have to introduce what's the dimension of the set. we have realized that there are manyu definition of f sets. in the world of imformation processes.
- description of elemetrary fractal sets
- fractal dimension
- how fractal dimensions and fractal sets are related

## Slide 3

1. the typical feature of fractal sets is the complex structure on an arbitrary small scale. we have complex structure on a scale arbitrary small. 
2. self similarity: signal can be described in the state space. the configuration through which we are describing is the same varying the scale of representation. by magnifing the signal itself we are obtaining the same features.
3. the dimension is generally not integer. we are acccustimed to integer dimension (point, line, surface, volume and so on). this object is characterized by non integer dimension. Many object in nature has this dimension. for example the cost line. also cauliflower. by detaching one branch we will see that the morfologu is resembling the big one and so on. of course in nature we have fractal like representation. by looking in the see the structure of the coral is fractal like. an histological part of the brain which was taken histologically we can see the morphology of the CNF with lateral branching resambling the main ones.
julia's sets or malderbrot: creating geometrical figures that can be described in this way. 

## Slide 4

in many case this fractal properties means healthy condition and non fractal means pathology.

## Slide 5

the dimension is the number of coordinate that are necessary to describe the point of a set.
line --> one dimension
surface --> 2d. we need 2 coordinates to fix the position on a plane
cube --> 3 coordinates xyz

simple objects have integer dimension

## Slide 6

aka middle third cantor set. it's a series of points in which we have a segment going from 0 to 1. the topological characteristic of this set is thtat in the successive step we consider 2 segments with the middle one removed, 1/3 for every segment. the middle third is always removed and we ceep on doing that. we can carry out this procedure up to infinite obtaining the cantor set. it's understandable that this set is done by this powder of points that will be obtained by progressing the number of iterations.

## Slide 7

c cibtaubs copy of itself. self similarity in the sens that we can obtain the whole segment before the scaling.
C has a null length. the length of the segment at k+1 is l(k+1)=... thus if s_k+1 --> 0 for k --> infinite.

## Slide 8

the important thing is that the new paradigm of interpretation of biological signals. the chaotic attractor of a time series are topological cantor sets. there are connections between the fractal systems and chaotic attractors definitions. wecan say that chaotic attractor possess a geometry that is fractal-like.

## Slide 9

we don't need to go too deep in the geometrical considerations of these examples.

we start from a segment s0 and at each iteration the middle third is removed and replaced by a triangle in which the length is equal to the third. for each new segment we do the same. we obtained a merlet. von koch curve is the set k = s_infinite.

## Slide 10

the very strange figure is in this case a idmention of dimesion between 1 and 2.

## Slide 11

how do we define the dimensions of these systems? with these 3 methods.

box counting --> very popular

how to measure fractality?
fractal derives from interrotto basically. an important concept in the modern theory and signal processing theory. these are the 3 tools to measure them.

- box counting -> express the points which belogn or are visited by the attractor during the evolution. we try to cover the figure with hypercubes in order to see how many points are in each cube of dimension epsilon. we measure the dimension for epsilon tending to 0. this is a geometrical parameter. we are starting from the state  space representation.
- correlation dimension: already encountered. a signal constituted by various x and computing the ... read the slide. dynamical property. it remarks the fact that there's an evolution. the correlation dimension is computed on developing on the signal that we are starting from
- Lyapunov dimension: how system is evolving in time. negative --> convergence, positive --> divergence. the various orbits that the system is making

da sapere queste 3 e come sono calcolate

## Slide 12

we take a cube of dimension n. we consider each side equal to epsilon. the total number of boxes is proportional to i/epsilon


d_b --> dimension of box counting

we can see how many points are on these cubes and make a variation of epsilon.

N(e) --> number of points contained in the square. it is iinveresly proportional to epsilon itself .

by plotting in log scale: for epsilon going to infinite (? il prof sembra dire più spesso che deve tendere a zero) we obtain the definition of box counting dimension as this limit.

## Slide 13



## Slide 14
## Slide 15

dobbiamo sapere come fare ad ottenere questi valori

## Slide 16



## Slide 17

correlation dimension was already found when dealing with non linear chaotic system. 

## Slide 18

if we plot in log log scale the correlation dimension we obtain that the slope of the curve in the middle part is indicating the characteristic on the dynamic on the system that we want to study.

in real system it's not that automatic to compute the slope. this is also the dimension is not equal (dc non per forza è uguale a db)

## Slide 19

we obtain the values in this plot and calculaitng in loglog scale and the slope of these points is the value of the dimension calculated thorugh the correlaiton dimension.

## Slide 20

dimension of box counter is >= correlation dimension-

## Slide 21

we can find a series of exponent in which every exponent is indicating an expansion or contraction. how the system is evolving from instant t to instant t+1 if there's a convergence or divergence of the system.

## Slide 22

k is the maximum of m so that the Sm is greater than 0.
very important formula. 

## Slide 23

henon map has a fractal dimension that is non-integer


n is the number of degrees of freedom of the sistem

## Slide 24

1. chaos is dynamical system that is extremely sensitive to initial condition and can provide erratic behaviour. fractal is a system defined by a certain geometry and that has a certain similarity. when sudying chaotic system one of the behavior has to be the fractal dimension.
2. basin of attraction can have fractal boundary

there are connection between fractal and chaotic sistem.
all chaotic systems have a fractal geometry.

## Slide 25
## Slide 26
## Slide 27
## Slide 28
## Slide 29
## Slide 30

# References

Slides: "9 Complexity&Fractals"
Cerutti's Book chapters: 13
