# Lecture 1

## Slide 2

A digital filter can be described in a block diagram as seen in this
slide with an input u(k) and an output y(k).

there can be 2 kind of implementation:

- Non recursive: non dependent from the delayed output of the filter but only
  on the delayed input to the filter.  $`y(k) = \sum\limits_{i=0}^{M-1}
  {h(i)\cdot u(k-i)}`$

- Recursive: dependent from the delayed output of the filter.  $`y(k) = u(k) +
  a\cdot y(k-1)`$ → equation of first order recursive filter


## Slide 3

Statistical filtering is based on the concept of models of interaction between
noise and signal.  The simplest way to measure signal/noise interaction is the
following $`y(k)=x(k)+v(k)`$ → prejudice of additivity.  The sources of noise can
be really different from the network interference @50-60Hz or due to electrodes
positioning and so on.  *Time-invariant system* → the function describing it is
not depending directly on the time but on a time dependent variable. a system
is time-invariant if a delay in the input is causing the same delay in the
output.  *stationarity of a process* → mean and variance are not time-dependent

The important quantities to keep in mind are the mean value, the variance and
the second order momentum.  $`S = \sigma^{2}+E[x]^{2}`$

The noise is a stationary process uncorrelated with itself and with the signal
x.

## Slide 4

We want to find a way to estimate the value of x from the dirty signal,
corrupted by the noise v. The trick is to find the correct coefficients in
order to correctly obtain the input.

## Slide 5

Non recursive estimator: let's consider M samples each having a coefficient
$`h(i)=\frac{1}{M}`$.  The expected value is $`E[x] = \frac{1}{M} \cdot
\sum\limits_{i=1}^{M} {y(i)}`$ by some computation (look at the slides) we
obtain as a notevole result that the mean of the squared error is dependent on
the variance of the noise divided by the number of samples.  This means that by
increasing the number of samples we reduce the error but we increase the
computational load. we need to find the tradeoff between these two condition.
As a secind thing we can see that the expected value of the prediction is the
same of the signal → unbiased estimator.

## Slide 6

Recursive Estimator: obtaining the equation of the output g(k) (see the
slides). it's depending from a first factor that is a geometric series
multiplying x and the second part that can easily be computed.  If we consider
m really large → $`|a|^m \ll 1`$. this mean that the second part can be neglected
and g(m) is $`g(m) = x/(1-a)`$.  The expected value is $`E[x]=(1-a)\cdot g(m)`$.
By substituting g inside the above mentioned expected value we obtain the
ultimate formulation.


## Slide 7

The source of error of this recursive estimator are the following:

- noise v
- $`a^M`$ is never going to be exactly zero.

we can define $`\gamma`$ as the noise to signal ratio, $`\varepsilon = a^M`$.  By computing
the mean of the squared error is exactly equal to the variance of the error
divided by the noise to signal ratio.

## Slide 8

Just two example showing the tradeoff of the parameter M.

## Slide 9

Optimal estimator: optimal in the sense to minimize the mean square error. so
it doesn't exist a better filter than this.

non recursive optimal estimator → Wiener filter recursive optimal estimator →
kalman filter

Wiener filtering: non recursive optimal filtering.  what are the steps?
1. prejudice of additivity of x(k). x is the information inside the dirty
   signal y. it is not essential. the hypothesis is that signal and noise
   interact in some way.
2. x and v are stationary and uncorrelated. characteristic maintained in time
3. M is sufficiently elevated. if m is small we don't have good performance,
   theoretically up to infinite but it's not possible to  implement it because
   there's an implementation limit.

The optimization is performed by putting the derivative of the mean of the
squared error wrt the filter's coefficient to 0.  By looking at the various
contribution there is the autocorrelation and the correlation between x and y.


## Slide 10

Ultimately we can say that to consider this optimized filter we should start
from a priori knowledge, otherwise there's is suboptimal solution.

## Slide 11

Application:

- $`y(k) = x + v(k)`$. v(k) is a white noise (uncorrelated with itself and with
  the signal).

All the coefficients of the filter can be computed as shown in the slide. by
increasing the signal to noise ratio (reducing $`\gamma`$) the mean of the
squared error is tending to $`{\sigma^2}/M`$.

## Slide 12

- $`y(k) = x\cdot k + v(k)`$ → straight line with x as slope.

In the estimation of the line with 2 points:

- autocorrelation $`P_{y,y}(i,j) = E[y(i)\cdot y(j)] =
E[(iX+V(i))\cdot(jX+V(j))] = i\cdot j\cdot E[x]+E[V(i)\cdot V(j)] =
i\cdot j\cdot S + \sigma_V \cdot \delta (i,j)`$

- crosscorrelation: Pretty easy to compute, just follow the slide

if noise to signal ratio is 0 → least squared

## Slide 13

Extensions:

1. Estimate other parameters, for example the amplitude of the sinusoid.  Link
   to matched filters →  i have an expression of the coefficient on the basis
   of frequency $`f_1, f_2, f_3`$ and expression of alpha as indicated in the slide.
   this last depends on the second order moment S.

Wiener filter is allocated in such a way that we could take into account the
traditional iir,fir and match filters in some condition. (le formule che
appaiono sotto non sono importanti).

## Slide 15

Sometimes it's not so easy to develop a Wiener-hopf of a phoenomenon but
specific problems are more difficult and maybe we don't have a priori
information. we have to pay attention to the easiness of filter. we can use a
different approach considering the frequency domain (bode-shannon). mostly used
especially in biology.

BODE-SHANNON: if the spectra are both rational the optimal implementation is
the one reported below.

H is the transfer funcition of the filter (the frequency response of the
filter).  phi: power spectral density (ft of the autocorrelation funtion)

the 2 power spectral densities shall be known a priori.

The difference with the Wiener-hopf approach is the fact that the equaiton is
much simpler.

What is more used from a practical point of view and relatively to biomedical
singals is the Wiener filter in frequency domain and so this is shown in this
slide. We consider that Bode-Shannon has introduced the concept of analysing
Wiener filter in the frequnecy. What they have demonstrated is that if the
spectra of the signal and of the noise  are both rational the implementation of
Wiener filter is the one reported on the slide.

$`H(\omega)=\dfrac{\Phi_{XX} (\omega)}{\Phi_{XX} (\omega)+\Phi_{NN} (\omega)}`$

with: $`H(\omega)`$: frequency.
response of the filter, $`\Phi_{XX} (\omega)`$: FT of the autocorrelation
function of X. We could also say it's the PSD (remember that the FT of
autocorrelation is the PSD).  $`\Phi_{NN} (\omega)`$: FT of the autocorrelation
function of n.

The unusual thing is that the Wiener filter in frequency domain is made of real
entities because the two PSDs are real. This leads to H as being real as well,
so non complex. It's a limitation of this kind of approach but if we accept it
this is a good estimation of the value of Wiener filter in frequency domain.

Wiener filter are always stable and with zero phase because there's not an
imaginary part.

The two PSDs needs to be known a priori. We require this information to compute
this kind of filter. If we want to apply this tool to signal processing we need
to have an a priori knowledge of these two PSD.

## Slide 16

In the evoked potential case we can consider different kind of stimuli (visual,
somatosensory and auditory).  Methods:

1. Wiener classic: all the power spectral density are a priori known. I can
   compute the expected value in the frequency domain and then, by applying the
   IFT i can obtain the signal in time domain.
1. A posteriori: sweep (realizzazione). i make the average of the sweeps and
   then compute the power spectral density.  i do the PSD of the sweep and then
   do the mean of the PSD and obtain the results that i am interested in
1. A posteriori: the PSD of the noise is taken at the i-th sweep

Many applications are in the area of EP. This EP case is different. EP is when
we submit a subject to a stimulus in clinics (visual like pattern reversal with
checkboard keep on changing on the screen, acoustic and somatosensorial). The
model by which we study the evoked potential case is the fact that we record
the signal y(t) which is the sum of the evoked potential itself (what is
related to the physiology we are studying: visual, acoustic, somatosensory
response) and the noise (EEG: most of the case we place an electrode on the
skull so we record also the EEG where we put the electrode and it's not
necessarily related to the stimulus and probably also other noises like
muscolar movement, movement of the skull, interface between skin and
electrodes, direct current source).

The typical model is just the sum between signal and noise. The processing that
i can make on y is that of enhancing the information related to the signal x in
respect to the noise which is strongly decreased.

### Method \#1

it's typical to define $`\Phi_{xx}`$ and $`\Phi_{nn}`$ known a priori (because
it's required by the Wiener filter). we know a priori the FT of autocorrelation
function of both noise and signal. We have a recording of the Y signal (FT of
evoked response) and multiply it by the $`H(\omega)`$ to have an estimation of
x. By IFT, we reconstruct the time signal estimation x $`\hat{x}(t)`$.

This is very theoretical and depends on the mathematical definition of Winer
filter.

### Method \#2

The so-called E.P. a posteriori case. Introduced by De Weerd. Simple idea that
was kind of smart and that became popular in the processing in Wiener filter in
frequency domain. We should have, rather than a priori, it's more probable that
we have some a posteriori estimation, based on the experience. The method to
have $`\Phi_{xx}`$ estimation is a syncronized averaging --> if we have 100 sweeps
or better we have a subject and this is required to do an EP analysis, instead
of providing one stimulus to obtain one response, we provide 100 stimuli
obtaining 100 responses. We make the averages syncronized with the stimulus.
The frequency of stimulation is not kept constant but it's random because we
don't want to introduce stimulation frequency artifacts. We average the 100
responses after the stimulus and obtain that the synchronized averaging is a
good estimation of $`\Phi_{xx}`$ (because the averaging is decreasing the
random presence of noise superimposed and it reinforces the presence of
information).  The best way to obtain an estimation of $`\Phi_xx`$ is to make an
averaging. So after the averaging we obtain the average of the response and we
then have to compute the power spectral density of the response and we obtain
that $`\Phi_{\bar{yy}} = \Phi_{xx} + \frac{1}{N}\cdot \Phi_{nn}`$.  Then we do
the PSD of each sweep and then average of single PSD and we obtain the average
of the PSDs --> $`\bar{\Phi}_{yy} = \Phi_{xx} + \Phi_{nn}`$.  In the end of
this procedure we have two equations in two unknowns and we can determine
easily the $`\Phi_{xx}`$ and $`\Phi_{nn}`$ using the physical properties of EP and the fact
that we calculated 2 branches of mathematical expression.

sweep (realizzazione).

## Slide 17

### Method \#3 E.P. a posteriori case

- For single sweep (Walter) we take the i-th sweep. if we had 100 sweeps we
  could determine the i-th response which is obviously x + noise relative to the
  i-th sweep. From this we obtain the H expression which is quite similar to the
  one seen before.
- For averaged set: just small differences that other researcher have
  introduced in the Wiener filter in frequency domain and we arrive to Doyle's
  proposal that is indicated in the slide.

### Method \#4 Time-variant case

It is supposed to be superior to the time-invariant one: if we have some
dynamics inside the single response we are able to capture them with this
method.  the preceding methods supposed that there were no dynamics inside each
single sweeps.

This method considers E.P. as characterized by shorter latency peaks which are
described to the right, characterized by high frequency contribution that is
valid at very early time period. And, at the same time, it's also characterized
by longer latency peaks with lower frequency component (as indicated in the
image to the left --> the frequency is smaller with respect to the one on the
right). Higher frequencies are the preliminary portion of the time while the
slower latencies are characteristics of last period of time. 

So for example we can have N75: the latency at around 75ms which are
characterized in the ??? of time and vice versa we have also the P300 peaks
which are characterized by lower trend and are located 300 ms after the
stimulus.

We can find out that the equation defined in the slide (last one) is not just a
function of omega but also as a function of time because for example the
characteristic of certain frequencies should be limited to particular time
duration and other are characteristic for longer time duration. So we have the
same characteristic in which we have not only we have the function of frequency
but also that of time and so we can reconstruct how the various frequencies are
allocated in time. So we could build up easily a time-frequency diagram of the
evoked response.

H is not only function of omega but also time. (similar as doyle but with time
variance) 

## Slide 18

### Method \#5: single sweep with prestimulus estimate

Very clever way of processing evoked potential: sometimes the major criticism
that is done to the grand average is that if we have a patient and we want to
see his response to a flash or acoustic stimulus we are compelled to make a
very long procedure in order to have an improvement of SNR because, in order to
have an interpretable clinical result, neurologist generally need to have a
higher number of sweeps and this requires time (i.e. for acoustic EP we are
required to provide 1000 clicks that could last even 1 h and it could affect
physiological or pathological response). So there's a need to reduce the number
of sweeps. Having an estimation done with a reasonable number of sweeps is a
challenge that has intrigued most of the researcher in the area in the last
years. 

This method says that we can consider that the i-th response of the evoked
potential is given by these two: the signal (whose information always the same)
and i-th response-related noise. For example we can have the Fourier transform
of the transfer function that is the ratio between the PSD of the sincronized
averaging (--> this means that we are producing 100 stimuli and compute the PSD
related to the responses of the patient. we put these in the equation reported
at the numerator and at the denominator) and instead we put the prestimulus
part of the i-th stimulus. Same that if we have one i-th stimulus constituted
by pre and post stimulus basically the noise in the post-stimulus period is the
same in the pre-stimulus period: there are evidences that if we produce a
visual stimulus to the brain there would be $`10^10`$ (number of neurons in
brain) probably only few hundreds or few thousands are the ones interested by
the visual or acoustic stimulaiton. the other will remain uneffected. We can
see that the ongoing EEG before the stimulus and after the stimulus basically
should be considered the same. This is the phylosophy behind this method: to
estimate the noise superimposed in the post-stimulus phase going to take it in
the pre-stimulus because in pre-stimulus there is only noise, in post-stimulus
this noise remains if we accept this hypothesis. Then in the poststimulus there
will be also the evoked response. So that could be a method to decrease the
presence noise and improve the presence of signal.

Problems with Wiener filter:

1. The Hypothesis is a priori. Wiener is a traditional a priori filter. Even if
   there are some a posteriori application of it, it is an interpretation of
   people that came after him and decided to induce some changes in order to
   improve the performance of the filter itself. The basic definition of Wiener
   filter is a priori filter. Both the signal and the noise are stochastic,
   uncorrelated processes. If we could see the visual response to brain we
   could see a deterministic signal, not really a stochastic signal. The burden
   if we are obliged to observe if we want to apply it.
2. M should be elevated in order to have statistical significance of what we
   are measuring. M should be known a priori. if M changes all the computations
   need to be redone. Think of the Wiener filter in time domain: if we define a
   number of input data, a number of EEG channel where to detect the
   information and we want to change this number we have to restructure the
   matrices of our tasks and to redo all the calculations. there is not, at
   least in the classical form, a recursive. Wiener filter is typically a batch
   approach (i have 1000 data, i apply Wiener filter to 1000. if i change it i
   have to compute again).
3. Computation of autocorrelation of y and the matrix inversion (MxM) should be
   a problem.

Wiener is a son of his time. <Read the slide>. The first criticism to do to
Wiener is that, in fact, it is a stationary stituation in which we have this
bulk of data and we develop the calculation on this bulk of data. there's no
adaptation, no dynamics in it and we are invited to consider just stationary
phenomenon inside this temporal window.

===

with a single sweep. higher number of sweeps is better to get a higher SNR. if
we are able to reduce the number of sweeps is a challenge --> i can consider
the ith response of evoked potential as the sum of signal we want to enhance
and the ith noise. i compute the power spectra of the signal and sum it to the
visual stimulation. the others will remind unaffected. we can see that the EEG
before and after the stimulus are actually the same. Basically we are
approximating the poststimulus FT of autocorrelation function with the one of
the prestimulus with this method.

The objection can be that the stimulus itself could change, not only the signal
which does for sure but also the noise.


## Slide 19

We can see some example of Wiener filter applied to simultation of EP. 

After Wiener filtering, application of Wiener-Hopf equation we have a pattern
like that (first column, second row) in which we have still a disturbed
response but we could have a reasonable improvement.

This (first column, third row) is another example in which the pattern is moved
wrt 0 to the right and the filter generates a pattern of that kind (first
column, third row) with some noise but also with a big improvement with respect
to the first one.

So the stimulus which presents the short duration or we have instead, in the
right part of this slide, we see that the signal is constituted by a white
noise and a kind of a sinusoid which is a kind of a steady-state evoked
potential (EP in which is maintained and not transied). It is a continuous
stimulation with a pattern like that. with the Wiener filter application we can
see a cleaning of the signal as shown in this slide.

Also here (second column, third graph) with a different SNR we can obtain the
original signal with the pattern we want to detect. Under the first graph we
have the filtered version with Wiener. here we obtained the filtered one as
indicated in here wrt the teoretical one. So we can compare the experimental
result (at lower amplitude) and what we obtained after filtering (higher
amplitude) so what we have simulated with the equations.


## Slide 20

Wiener filter applied to Evoked Potentials.  Here are reported the plots of the
first 5 sweeps for channel A. The vertical bar indicates the stimulus onset at
$`t=0`$.  So we can obtain, after Wiener filtering, the response indicated in
the graph on the right.  The filtering method used in this case is the one with
pre-stimulus estimation (i.e. take what we find before the stimulus and
consider it as the estimation of noise also after the stimulus). The main
objection can be that the stimulus itself could change, not only the signal
which does for sure but also the noise. This could be a criticism that we can
accept if it is so this method could not be applied but this is applyed by many
researcher. We suppose that the stimulus does not affect the noise. This is a
criticism to this approach but we know that for every technique there are
advantages and disadvantages cons to apply it. There is not an absolute
statement to say if a tool is not good or good. Under some hp the tool is good,
under other it's not.

It's also possible to substitute each sweep with the average of a small subset:
for instance if we have like 300 sweeps we can consider as new sweep the
average of 5 old sweeps. in this way the superimposed noise is a little bit
smoothed out. We can than consider a single sweep in reality the averaging of 5.

# Slide 21

*Left column* These are evoked potentials. Each of the reported sweeps are
representing, in reality, the partial average of 10 consecutive sweeps

- *first graph*: with Wiener filtering.
- *second graph*: without Wiener filtering.


*on the right*: traditional synchronized averaging (no Wiener involved here).
From the bottom sweep up: averaging of the first 10 sweeps, then of 20 and so
on, till including all the 100 sweeps. We can see how the averaging of 100
sweeps is really improving the highest SNR.

# Slide 22

We started from Wiener filter in time domain, this approach gave rise to
Wiener-Hopf equation and to the application we have remarked.  This Wiener
filter in time domain is not really much used in the biomedical field because
it requires, generally, a lot of a priori information that are not really
available and difficult to obtain. While in the frequency domain, with
Bode-Shannon, it's easier and automatic: we should have an estimation of the
PSD of signal and the PSD of noise and if they overlapped or not. So this
approach is more diffused even for biomedical signal and in particular for
evoked potential. So this is what Wiener filter could produce in biomedical
signal processing analysis. 

We need to pass to a more sophisticated filter, the Kalman filter, which starts
from Wiener filter and the basic difference is that it is based upon different
a priori estimation and it has a recursive formulation. And this last one is
the advantage because in reality we can really update the filter according to
the need that we have. Wiener had the problem that if whe chose a M, if we
wanted to change it to M+1 we had to redo all the computations. Instead with
Kalman filter we will see that the formula of updating will be recursive and it
will be easier to pass from M to M+1. So recursive formulation is indoubtly
important and convenient.


If we want to induce a recursive formulation of the Wiener filter (i.e. if we
wanted to use Wiener filter in a recursive form) how would we change the
formula that we have already defined? <Read> 

*At step k*:

$`\gamma = \dfrac{\sigma_{\nu}^2}{\sigma_{X}^2}`$

$`p_e (k) = E[(x-\hat{x})^2] = \dfrac{\sigma_{\nu}^{2}}{k + \gamma}`$

$`h(i) = \dfrac{1}{k + \gamma}`$

$`\hat{x}(k)=\sum\limits_{i=1}^{k} {h(i)\cdot y(i)}=\dfrac{1}{k + \gamma}\cdot
\sum\limits_{i=1}^{k} {y(i)}`$

*At step k+1*:

$`p_e (k+1) = \dfrac{\sigma_{\nu}^{2}}{k + 1 + \gamma}`$

$`h(i) = \dfrac{1}{k + 1 + \gamma}`$

$`\hat{x}(k+1)=\sum\limits_{i=1}^{k+1} {h(i)\cdot y(i)}`$

with: $`\gamma`$: ratio between the variance of the noise and the variance of
the signal, $`p_e (k)`$: quadratic error, computed at step k, $`h(i)`$: Wiener
filter i-th coefficient, $`\hat{x}(k)`$: signal estimation at step k

By putting together the two consecutive steps we can make some consideration
about the quadratic error ratio:

$`\dfrac{p_e(k+1)}{p_e(k)} = \dfrac{1}{\dfrac{k+\gamma+1}{k+\gamma}} =
\dfrac{1}{\dfrac{k+\gamma+1-1}{k+\gamma} + \dfrac{1}{k+\gamma}}
=\dfrac{1}{1+\dfrac{1}{k+\gamma}} = \dfrac{1}{1+\dfrac{p_e
(k)}{\sigma_{\nu}^2}}`$

So at every iteration the quadratic error is decreasing. We can write the
quadratic error ratio also in this way:

$`\dfrac{p_e(k+1)}{p_e(k)} = \dfrac{k+\gamma}{k+1+\gamma}`$

The recursive formulation of Wiener filter is here reported:

$`\hat{x}(k+1)=\sum\limits_{i=1}^{k+1} {h(i)\cdot y(i)} =
\dfrac{1}{k+1+\gamma}\cdot \sum\limits_{i=0}^{k} {y(i)} +
\dfrac{1}{k+1+\gamma}\cdot y(k+1)= \dfrac{1}{k+1+\gamma}\cdot
\sum\limits_{i=0}^{k} {y(i)} \cdot \dfrac{\hat{x}(k)}{\dfrac{1}{k+\gamma}\cdot
\sum\limits_{i=1}^{k} {y(i)}} + \dfrac{1}{k+1+\gamma}\cdot y(k+1) =
\dfrac{k+\gamma}{k+1+\gamma}\cdot \hat{x}(k) + \dfrac{p_e
(k+1)}{\sigma_{\nu}^2}\cdot y(k+1) = \boxed{\dfrac{p_e(k+1)}{p_e(k)}\cdot
\hat{x}(k) + \dfrac{p_e(k+1)}{\sigma_{\nu}^2}\cdot y(k+1)}`$

It can be written in a more compact way:

$`\hat{x}(k+1) = a(k+1)\cdot \hat{x}(k) + b(k+1)\cdot y(k+1)`$

It's easy to demonstrate that $`a(k+1) + b(k+1) = 1`$, so the filter can be
rewritten as follows:

$`\boxed{\hat{x}(k+1) = \hat{x}(k) + b(k+1)[y(k+1) - \hat{x}(k)]}`$

So here's the recursive formulation of Wiener filter.

With the Kalman Filter the formula od the updating is recursive, so it's easier
to pass from M samples to M+1. Kalman filtering is based again on a priori
information, like Wiener filtering.

## Slide 23

The gain $`b(k+1)`$ is a time-variant factor. it's multiplying the difference between the true value and the estimation at the previous time instant. This is very important because I can see that each time instant has an information that is equal to the estimation at the preceding time instant and a term that could be zero if there's no innovation in the information or if the b value is equal to 0 (meaning that we are in a stationary situation). When we want to use Wiener filter in a recursive way generally we have this innovation thta is new samples induce new information to the known i have obtained up to instant k. I obtain new information produced by new sample and i properly weight it with the regularization factor. There could be a very strong innovation with a risk of having an overflow of the values: this b is something that maintain the difference reasonable. It's interesting because we have arrived to a recursive formulation which is very important in the application. In this slide representations of this recursive formulation are reported. [continua da qui].

# References

Slides "1 First part",
Bozic Chapters: 6, 7
