# Lecture 12

## Slide 1


## Slide 2

- *first box*: the traditional one is the one processing information coming
  from exams (blood analysis, imaging, signal coming from patients), clinical
  records, sequences of DNA. Integrating information at different levels is the
  concept that we have already described for the 4M method. 
- *second box*: every advance technology has a cost and must be sustainable for
  the needs of the patient.  Tech development is important because advanced
  technique always come.  To care and to take care is the expression that is
  used to discriminate he technology that cares and takes care of the patient.
  This technology could be closer to the patient and to the patient's needs. 
	- sustainability: we should have the economical resources to deal with
	  huge amount of data. And we should really be able to enhance the
	  information content inside these data. Sometimes it happens to be
	  lost or overwhelmed by data and we need to be aware of that. 
- *third box*: having some preliminary information about the possible evolution
  of ???

## Slide 3

It's very easy to record biosignals and parameters from biosignals. The need is
to be able to reliably record these parameter everywhere, every time (24h
possibly) but in this way we have a huge amount of data. The challenge is to
have innovative sensors comfortable to wear and less sensitive to noise because
these recorded are not made in clinics so there could be noise that disturb it. 

Read as is

If environment is not under control we should have ad hoc algorithm to gather
information.

Cloud processing: if we have many parameters to extract we have to, in most of
the cases, which are the most important info to be processed on-board and what
can be computed on the server.

===

Holter recording should be sufficient for measuring long time ECG.

## Slide 4

Intelligent sensors used to monitor certain activities. We are not only interested in clear diagnosis of disturbances but also the activity states which is related to healthy condition but bases on different parameters to measure.

We have very efficient synthesized or very old stile transducers inside a t-shirt which embodies them.

===

Sensors are very important to monitor the state of the activity (a physiological state.). We have a more or less invasive interaction with the patient

## Slide 5

Intelligent sensors: we can see devices that can be put in contact to monitor the body.

## Slide 6

PPG: photopletismography. the signal can be carried on also in cellphones which we can position properly a finger on and a camera can detect the changes in this signal, having the estimation of the volume of blood changing under our skin and have an estimate of the blood pressure.

## Slide 7



## Slide 8

FDA --> food and drug administration in US
IEC --> ??? like an European equivalent

## Slide 9



## Slide 10
## Slide 11
## Slide 12
## Slide 13
## Slide 14
## Slide 15
## Slide 16
## Slide 17

arousal ---> significant displacement from the baseline. the number of arousal are correlated with the deepness of sleep.

These values (sensitivity, specificity, accuracy) are good even if we are not measuring at hospital but with the correct equipment.

## Slide 18

Another project with collegues of europe. We can have the history of the subject's statistic.

## Slide 19

All these amount of data can be processed. We can use lots of parameters working with HRV with ECG leads, we can compute the power and then with the classificator we can classify the sleep profile. We can discriminate if results are good or not.

HMM --> hidden Markov method

## Slide 20

There are recordings of ECG in 2,3 leads inserted in the textiles and sensors for respiratory movement (strain gauges or piezoelectric sensors).

## Slide 21

Stress is given for example by mental math calculation.

a.u.: arbitrary unit.

## Slide 22



## Slide 23
## Slide 24
## Slide 25
## Slide 26
## Slide 27

Software is a medical device when it has a medical purpose. Not only the equipment but also the software itself.

## Slide 28
## Slide 29
## Slide 30
## Slide 31
## Slide 32
## Slide 33
## Slide 34
## Slide 35
## Slide 36


# References

Slides: "Roma2017"
