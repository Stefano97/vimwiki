# Cerutti

| Lecture                     | Topic                                  | Slides   | Revised     | Difficulty | Comprehensible | Days                      |
| :---                        | :---                                   | :---     | :---        | :---       | :---           | :---                      |
| [01](Cerutti/01/Lecture.md) | Wiener & Kalman (1)                    | 1 - 18   | Yes         | Normal     | yes            | 22.02                     |
|                             | Wiener & Kalman (2)                    | 15 - 29  | No          | Normal     | yes            | 24.02                     |
|                             | Wiener & Kalman (3)                    | 29 - 36  | No          | Normal     | yes            | 01.03 (start - 01:00:25)  |
| [02](Cerutti/02/Lecture.md) | Adaptive filtering                     | Complete | No          | hard       | ?              | 01.03 (01:00:25 - end)    |
| [03](Cerutti/03/Lecture.md) | Time-frequency & Wavelet (1)           | 1 - 37   | No          |            |                | 02.03                     |
|                             | Time-frequency & Wavelet (2)           | 1 - 41   | No          | Easy       |                | 03.03                     |
|                             | Time-frequency & Wavelet (3)           | 29 - 70  | No          |            |                | 08.03                     |
|                             | Time-frequency & Wavelet (4)           | 71 - 77  | No          |            |                | 09.03 (missing last part) |
| [04](Cerutti/04/Lecture.md) | Quadratic Time-frequency               |          | No          |            |                | 09.03                     |
| [05](Cerutti/05/Lecture.md) | Non-linearity and Chaos (1)            | 1 - 15   | Yes         | hard       |                | 10.03                     |
|                             | Non-linearity and Chaos (2)            | 6 - 24   | Yes         | hard       |                | 15.03                     |
|                             | Non-linearity and Chaos (3)            | 10 - 40  | No          | hard       |                | 16.03                     |
|                             | Non-linearity and Chaos (4)            | 38 - end | No          | Normal     | yes            | 17.03                     |
| [06](Cerutti/06/Lecture.md) | Complexity and fractals                |          |             |            |                | 22.03 (start - 01:58:46)  |
| [07](Cerutti/07/Lecture.md) | Fractal and non linearities (1)        |          |             |            |                | 22.03 (01:58:46 - end)    |
|                             | Fractal and non linearities (2)        |          |             |            |                | 23.03                     |
| [08](Cerutti/08/Lecture.md) | Biomedical Signal I Non linear (1)     |          | No          |            | yes            | 24.03                     |
|                             | Biomedical Signal I Non linear (2)     | 17 - end | No          | Very hard  | No             | 29.03 (start - 01:14:13)  |
| [09](Cerutti/09/Lecture.md) | Biomedical Signal II Non linear (1)    |          | No          | Very hard  | No             | 29.03 (01:14:13 - end)    |
|                             | Biomedical Signal II Non linear (2)    |          | No          | Very hard  | No             | 30.03                     |
| [10](Cerutti/10/Lecture.md) | Multiscale                             |          | No          | Very hard  | No             | 30.03                     |
| [11](Cerutti/11/Lecture.md) | Brain - Multimodal                     |          | No          | Hard       | No             | 31.03 (start - 01:10:00)  |
| [12](Cerutti/12/Lecture.md) | Technologies for future healthcare (1) |          |             |            | Yes            | 31.03 (01:10:00 - end)    |
|                             | Technologies for future healthcare (2) |          |             |            | Yes            | 07.04                     |
| [13](Cerutti/13/Lecture.md) | Etichs                                 | Complete | Yes         | Easy       | Yes            | 12.04                     |
| [14](Cerutti/14/Lecture.md) | Multimodal magnetic resonance          | Complete | In-progress | Easy       | Yes            | 13.04                     |


| Lecture   | Link                                                                                                                          |
| :---      | :---                                                                                                                          |
| 22.02     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=a295540e965d44d79c53acfdc630c4f5                           |
| 24.02     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=6a4c23dfd76a4797b4ea31fbf12c4d50                           |
| 24.02     | https://polimi365-my.sharepoint.com/:v:/g/personal/10746314_polimi_it/EUcFRoCJI7pBn0EAZHQN0_oB9o05QikYTxn8GXvOlTjTsA?e=H16rrb |
| 01.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=7ac3b57d2ee8439ab345fa28afea09c1                           |
| 02.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=ea522b62f64d47968fa1808fc980ca33                           |
| 03.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=8af53383883b489e90986af56fbaaeab                           |
| 08.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=1a166e295c8b4afe9953db17b5cff6ce                           |
| 09.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=14b5000ca4cb40599d4ba4014ff66d91                           |
| 10.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=60527866e4f84c388c1fae66fe8412f7                           |
| 15.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=4c34e1ae799f409896170e34516ee20a                           |
| 16.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=b593512988a148a38866da6563186848                           |
| 17.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=55e7124dfce840f4bf8a6579d82f6857                           |
| 22.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=6aa7b08073e445e6ac9ec09fde68d277                           |
| 23.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=575545000d5247369cc706ffc0ec5dbe                           |
| 24.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=a93d1681828d4f1f8972f8335c176134                           |
| 29.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=c21aa897225741cab147481311315729                           |
| 30.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=fc5497a5dd874361b3a92f60e8f85156                           |
| 31.03     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=3619a5fee98d4857a37c62351b75f263                           |
| 07.04     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=c73582edc43242af8a90b18891359104                           |
| 07.04     | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=5f7a1dbf0df84e41b5e6a72f552d513b                           |
| 12.04     |                                                                                                                               |

## Examination procedure

Oral exam. The first term is probably going to be written. Verifica in itinere:
written instead of oral exam. Multiple choice. The next exams are being held to
a modality that still needs to be defined.

# Barbieri

| Lecture                 | Topic | Revised | Difficulty | Comprehensible | References |
| ---                     | ---   | ---     | ---        | ---            | ---        |
| [01](Barbieri/01/01.md) |       |         |            |                |            |
| [02](Barbieri/02/02.md) |       |         |            |                |            |
| [03](Barbieri/03/03.md) |       |         |            |                |            |


## Examination procedure

Multiple choices probably, as mentioned by Prof. Cerutti.
