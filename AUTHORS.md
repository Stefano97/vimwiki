# Project Contributors

This project was started in 2021 by [Luca Andriotto](https://www.github.com/andriluca) at Politecnico di Milano.

The following people have contributed documentation to the project (alphabetical by name) and are considered co-authors:

- [Luca Andriotto](https://www.github.com/andriluca)
