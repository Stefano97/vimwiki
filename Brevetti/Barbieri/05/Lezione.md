# Lezione 5

## Slide 1

Procedure di brevettazione: vedere l'iter dal deposito della domanda di brevetto
alla sua concessione vedremo la Procedura Nazionale, Internazionale e Statunitense.

## Slide 2

Prima di iniziare ad andare in dettaglio sulle procedure un mini ripasso e un
utile glossario sui termini che potremmo trovare se si esaminano brevetti:

- *Patent Application*: domanda di brevetto, quindi ancora in fase di esame.
- *Granted Patent*: brevetto concesso. ha superato tutte le fasi di esame e
  quindi è concesso da un ufficio nazionale.
- *Applicant*: chi richiede la concessione del brevetto (una persona fisica o un
  azienda, ente università)
- *Assignee*: titolare del brevetto, della privativa
- prosecution: prosecuzione ossia una serie di passaggi che portano la domanda
  di brevetto alla concessione
- *Office Action*: tutte le azioni ufficiali che l'esaminatore richiede (i.e.
  modificare le rivendicazioni, modificare la descrizione della domanda di
  brevetto).
- *Enforcement*: poter azionare una domanda di brevetto o un brevetto concesso
  contro un potenziale contraffattore. Si intendono quindi tutte le procedure
  che servono per azionare una domanda di brevetto.
- *Claims (rivendicazioni)*: una delle sezioni più importanti più importanti di
  una domanda di brevetto concesso perchè definiscono l'ambito di tutela del
  brevetto stesso.
- *Specification (o description)*: si intende la descrizione tecnica della
  domanda o del brevetto concesso.
- Domande di brevetto "particolari":
	1. *Divisional patent application (divisionali)*: domande di brevetto che
	   derivano da una precedente domanda quindi collegate da una data di
	   priorità: probabilmente nella domanda iniziale venivano descritte
	   due o più invenzioni e, siccome viene concesso un brevetto per
	   un'invenzione, l'inventore deve depositare le successive domande di
	   brevetto.
	2. *Provisional patent application (domande provvisorie)*: non sono
	   disponibili in tutte le legislazioni ma è prassi comune nella
	   pratica US e di altri paesi ma non in EU.
- *Search report*: quel documento redatto dall'esaminatore dell'ufficio che sta
  analizzando la domanda di brevetto e che contiene una serie di citazioni di
  documenti anteriori (prior art).
- *Written opinion*: è quella che serve per interpretare il rapporto di ricerca.
  Mentre il rapporto di ricerca è un elenco di documenti di arte nota
  nell'opinione scritta l'esaminatore fornisce le motivazioni. Ci dice perchè è
  citato un documento e se quel documento è citato per mancanza di novità o di
  attività inventiva.

## Slide 3

- Il *brevetto*: documento tecnico/legale che converisce al titolare un diritto
esclusivo, di vietare agli altri la realizzazione di quanto rivendicato e
soprattutto la vendita del dispositivo. è un diritto negativo: non autorizza a
realizzare l'invenzione rivendicata ma serve ad escludere altri (ius escludendi
alios). Perchè non autorizza a realizzare l'invenzione? un inventore puà
realizzare un invenizone anche se non l'ha tutelata con un brevetto. se poi ho
un brevetto devovedere che non ci siano legi che impediscano la
commercializzazione del prodotto. capita spsso con i principi farmaceutici
perchè in quel caso non basta un brevetto ma bisogn vedere anche le
autorizzazioni all'immissione in commercio che in italia è concesso dall'AIFA.
Perchè un titolare non è autorizzato a realizzare un invenzione che ha
realizzato lui stesso? bisogna verificare la libertà di attuazione ossia se non
ci sono brevetti indipendenti e quindi il titolare deve avere una licenza o un
diritto per realizzare un'invenzione che è un miglioramento di un dispositivo
brevettato da altri titolari.

- L'*oggetto del brevetto*: riguarda tutte le caratteristiche techince di un
  prodotto o procedimento e queste caratteristiche tecniche sono poi elencate
  nelle riventicazioni.
- La *soluzione* del problema tecnico non si identifica necessariamente con
  prodotto o procedimento. non esiste una correlazione univoca
  prodotto-brevetto. Questo perchè vengono scritte le rivendicazioni in modo
  più ampio per evitare il design around (cioè che un concorrente
  commercializzi un prodotto con una caratteristica tecnica equivalente) e
  quindi fornisce la possibilità al titolare di avere più margine.

## Slide 4

Questi sono i requisiti inerenti ad un'invenzione:

- *Novità*: reqiusito oggettivo. verifico che non sussista nella tecnica nota un
  prodotto identico. basta solo una caratteristica tecnica differente affinche
  questo requisito sia soddisfatto.
- *inventività*: Requisito soggettivo. Un'invenzione non dev'essere facilmente
  desumibile dallo stato della tencica ossia devono esserci dei documenti che
  combinati insieme  possano dare l'invenzione che sto cercando di brevettare.
  Bisogna quindi vedere come collegare insieme i documenti. Ci deve essere
  sempre un suggereimento nello stato della tecnica che consenta di combinare
  due o più documenti. Altrimenti fa "cherry picking": vedo nella prior art
  quei documenti che descrivono parte della mia invenzione e poi li combino
  insieme.
- *Applicazione industriale*: un'invenzione deve potersi applicare in qualsiasi
  settore industriale ma ci si rivolge soprattutto alla riproducibilità e alla
  fattibilità dell'invenzione. un prodotto deve potersi realizzare e deve
  essere riproducibile. proprio perchè, alla scandenza o dei 20 anni di un
  brevetto o se un brevetto viene abbandonato, i terzi devono avere la
  possibilità di riprodurre l'invenzione.

## Slide 5

Le condizioni di brevettabilità riguardano come viene scritta la domanda di
brevetto:

- *sufficienza di descrizione*: una domanda di brebetto dev'essere scritta in
  modo siufficientemente chiaro affinchè un tecnico del settore possa
  replicarla.
- *Unità d'invenzione*: dipende da cpme scrivo la domanda di brevett. nella
  stessa domanda posso descrivere due o più invenzioni ma ciò non è possibile
  perchè poi l'ufficio competente concede il titolo solo per un'invenzione
  quindi bisogna scorporare la domanda iniziale in tante domande di brevetto
  quante le invenzioni rinvenute dall'esaminatore.
- *Chiarezza*: dipende da come vengono scritte le rivendicazioni. devono essere
  scritte in modo chiaro e deve esserci supporto nella descrizione. Leggendo la
  descrizione devo trovare tutte le caratteristiche tecniche essenziali che ho
  rivendicato nella domanda di brevetto.

## Slide 6

*Ambito di tutela*: è definito da tutte le caratteristiche tecniche della
rivendicazione principale ed è sufficiente che non ne sia presente una sola per
esserne al di fuori.  La descrizione dev'essere redatta in modo
sufficientemente chiaro affinchè un tecnico nel settore possa riprodurla.

Nell'esempio: viene usata la locuzione "comprendente" che lascia aperto ad altri
elementi che sono essenziali a far funzionare un veicolo (volante, cambio etc).

## Slide 7

"Comprendere": serve ad includere l'elenco di elementi riportati al di sotto ma
potrebbero essercene eventualmente degli altri.

## Slide 8

L'ultima variante è rivendicata dalla precedente rivendicazione. Questo
semplice esempio mostra qual è la funzione di un ambito di tutela e come devono
essere scritte le rivendicazioni. Se queste non sono scritte bene è possibile
che altri possano bypassare il brevetto e quindi il titolare ha una tutela
limitata.

## Slide 9

In questa slide viene presentata una prima rivendicazione ed una rivendicazione
che dipende dalla prima. La *prior art* è ciò che è stato trovato
dall'esaminatore. Il documento reperito descrive un'automobile con quttro ruote
con un motore a scoppio con cilindrata a 4L. L'esaminatore affermerà che la
rivendicazione numero 1 rientra nella prior art. Il titolare allora può
accorpare le due rivendicazioni, andando a ridurre l'ambito di tutela. La
funzione dell'esaminatore è quella di verificare quanto è già noto nello stato
della tecnica e limitare l'ambito di tutela di un brevetto per non concedere
brevetti ampi e non concedibili (avrei come titolare un ambito di tutela troppo
elevato che comprende anche della tecnica nota).

Esistono due tipi di rivendicazioni:

- Aperta: accompagnata da locuzioni quali "che comprende ...", "caratterizzata
  dal fatto che ..." o affini. Significa che la rivendicazione potrebbe
  includere un insieme più ampio di elementi.  (quindi tende ad allargare
  l'ambito di tutela)
- Chiusa: accompagnata da locuzioni quale "che consiste di ..." o affini.
  Limita l'ambito di tutela.

## Slide 10

Si possono quindi accorpare le rivendicazioni in questo modo: utilizzando la
locuzione "caratterizzato dal fatto che".

## Slide 11

Quanto può essere importante il supporto della descrizione di ogni variante
possibile. Partendo sempre dalla rivendicazione ampia di prima, la prior art è
la stessa dell'esempio precedente. Nella descrizione però è presente una
variante (riportata sulla slide). Anche se avessi scritto solo questa prima
rivendicaizone (e non quella dipendente di prima) posso modificarla prendendo
spunto dalla descrizione. Se non avessi introdotto nella descrizione questa
variante non avrei potuto modificare il primo ed unico claim e quindi non avrei
avuto la possibilità di ottenere la concessione del titolo. Proprio perchè da
procedura è possibile aggiungere quelle caratteristiche tecniche che o sono
rivendicate in una rivendicazione dipendente o sono descritte nella sezione
desctizione. non è possibile aggiungere delle caratteristiche nuove durante la
fase di esame.

In parole povere posso modificare una rivendicazione mediante una seconda
rivendicazione dipendente oppure una variante elencata nella descrizione.

## Slide 12

Il brevetto ha una validità nazionale. In alcuni casi può avere una validità
regionale perchè utilizzo una procedura comune, esiste un trattato
internazionale che afferma che questi stati si sono organizzati in un certo
modo e hanno adottato una procedura comune di esame di concessione. Di solito
il brevetto ha validità nazionale: vale nello stato in cui viene depositata la
domanda di brevetto, è stata richiesta e ottenuta la concessione del titolo. Ci
sono diversi enti sovraregionali che sono qui elencati:

1. *EPO*: ufficio europeo brevetti in cui ci sono 38 paesi contraenti di cui 27
   membri EU.
2. *EAPO*: fasi regionale euroasiatica.
3. *ARIPO*: fase sovranazionale africana (EN).
4. *OAPI*: fase sovranazionale africana (FR).
5. *GCCPO*: organizzazione che riguarda i paesi del Golfo (Arabia Saudita, Yemen,
   Barhein, ecc.)

In ognuno di questi casi si ha un'unica procedura di deposito e di esame. cio
semplifica la concessione del titolo. in alcuni casi non serve fare una
convalida nelle fasi nazionali (nei paesi aderenti i trattati). Nel caso
dell'ufficio europeo brevetti invece è necessaria questa convalida, quindi il
titolare deve scegliere in uqali paesi devo convalidare poi il titolo. Non
facendo quest'operazione la domanda viene considerata come se fosse stata
ritirata.

## Slide 13

Un brevetto sia a livello di domanda ma anche dopo che è stato concesso deve
essere mantenuto attivo e per fare ciò occorre pagare delle tasse (in alcuni
casi annuali).

- US: le tasse sono calcolate dopo la concessione del brevetto ogni 3.5 anni.
  Se voglio mantenere un brevetto US per tutta la durata di 20 anni dovrò
  pagare 3 tranches.  Se non pago le tasse di mantenimento il titolo decade, il
  brevetto non è più e chiunque può replicare l'invenzione, utilizzarla e
  commercializzarla senza problemi in quel paese. Le tasse vanno pagate
  all'ufficio che sta esaminando quell'invenzione. per esempio in IT: vanno
  pagate al quinto anno. All'Ufficio Europeo Brevetti a partire dal 3 anno dal
  deposito della domanda Europea.

## Slide 14

periodo non presente in tutte le legislazioni ed è un periodo che va da 6 a 12
mesi a seconda della legislazione dello stato considerato che consente
all'inventore di divulgare l'invenzionee successivamente, tenendo presente che
ci sono 6-18 mesi dalla divulgazione, depositare la domanda di brebetto.

Il periodo di grazia non è contemplato dalla convenzione sul brevetto europeo e
quindi non la si può fare. se voglio ottenere la concessione di un brevetto
europeo non devo pubblicare, vendere il prodotto o mostrarlo alle fiere di
settore.

## Slide 16

Esaurimento del diritto può avvenire in queste condizioni:

- il diritto di esclusiva su un prodotto brevettato termina con il primo atto
  di vendita da parte del titolare del diritto con il suo consenso perchè per
  esempio è stato licenziato ad un'altra società e quindi essendo licenziatario
  esclusivo può vendere il prodotto ???.
- oppure con il primo atto di vendita: il titolare vende il prodotto e li ha il
  suo guadagno quindi chiunque abbia acquistato il prodotto lo può rivendere
  senza il consenso del titolare del brevetto. non lo può commercializzare se
  non lo ha acquistato dal titolare o dal licenziatario esclusivo. Questo si
  applica solo nell'unione europea ma non vale per le vendite extra UE.

## Slide 17

- uso privato e non commerciale
- sperimentale → posso fare degli esperimenti su quanto rivendicato in una
  domanda o in un brevetto concesso, l'importante è non commercializzare quel
  prodotto.
- anche le prove e i test che sono necessari per l'ottenimento
  dell'autorizzazione della messa in commercio. Tutte queste cose sono lecite
  indipendentemente da quando vengono realizzate dall'azienda.
- Il deposito di una richiesta di una richiesta non è considerato
  contraffazione se effettuato un anno prima alla scadenza del certificato di
  protezione supplementare che invece ha il titolare del brevetto.
- Esenzione galenica: sulla base di prescrizione medica il farmacista ha il
  diritto di preparare un medicinale coperto da brevetto.
- Preuso: chiunque nei 12 mesi precedenti la data di deposito di una domanda di
  brevetto, abbia fatto uso nella propria azianda dell'invenzione, può
  continuare ad usarla nei limiti del preuso (purchè si possa dimostrare che
  l'ha fatto prima del deposito della domanda del brevetto del concorrente).
  può commercializzare in questo caso ma sempre nei limiti del preuso che
  stabilisce una quantità predefinita.

## Slide 19

Il brevetto è costituito da quattro sezioni (tutte queste sono facilmente
consultabili dalle banche dati):

- frontespizio: dati bibliogradici e riassunto dell'invenzione.
- Descrizione tecnica
- Rivendicazioni
- disegni (per la meccanica) (formule di struttura per la chimica).

Il titolo è scritto di solito in modo molto generico. se sufficientemente
descrittivo potrebbe aiutare nelle ricerche di prior art ma solitamente è
generico. Anche il riassunto dovrebbe aiutare a comprendere gli aspetti
principali dell'invenzione ma di solito non è così: a volte viene riportata la
prima rivendicazione. è molto generico anche questo riassunto. sono generici
perchè non hanno nessuna valenza giuridica. quello che conta davvero sono le
rivendicazioni e la descrizione e poi perchè potrebbe anche essere una
strategia aziendale (non facilitare le ricerche di prior art e quindi la
reperibilità di informazione da parte di concorrenti).

## Slide 20

le slide 20 e 21 sono state lette tali e quali.

## Slide 22

esempio di frontespizio di un brevetto concesso.  Ci sono informazioni
bibliografiche, titolo, data e numeo di priorità. è una domanada tialiana del
2008 estesa a livello europeo. abbiamo altre informazioni ad esempio il numero
di pubblicazione e concessione definito dalla lettera B. ci sono poi codici di
classificazione che servono per facilitare le ricerche perchè classificano un
brevetto concesso in funzione del settore tecnologico di appartenenza.
Application number: numero della domanda che viene attribuita in fase di
deposito dall'EPO.  Data di deposito sempre riferito alla domanda di brevetto.
Data di pubblicazione e di menzione della concessione nel registro ufficiale
dell'EPO (con numero di pubblicazione diverso dal numero di domanda).  Stati
designati: quelli contraenti la convenzione sul brevetto europeo Data
pubblicazione della domanda di brevetto Inventori, Rappresentative: il
mandatario o consulente che ha scritto la domanda di brevetto e che ha seguito
tutta la prosecuzione della domanda fino alla concessione del titolo.
Reference cited: tutti i documenti che sono stati citati nel rapporto di
ricerca. fanno parte anche degli articoli scientifici sia brevetti.


## Slide 23

- A1 con rapporto di ricerca: questo accade perchè l'ufficio è obbligato a
  pubblicare una domanda fdi brevetto al 18 mese dalla data di deposito ma
  magari l'esaminatore non ha fatto in tempo a fare la ricerca di prior art e a
  redigere un rapporto di ricerca quindi la domanda viene pubblicata (info
  biblio, testo e rivendicazione).
- A2: senza rapporto di ricerca
- A3: pubblicato il rapporto di ricerca. Se una domanda europea viene
  pubblicata con A2 troveremo qualche mese dopo la stessa domanda con il codice
  A3, ovviamente non viene ripibblicato tutto il testo ma solo il frontespizio
  e il rapporto di ricerca con l'opinione scritta.
- A4: rapporti di ricerca supplementari: vengono fatti quando un applicant ha
  depositato una doanda di brevetto negli stati uniti oppure ha fatto una
  procedura PCT indicando come receiving office, ufficio che ha comunque fatto
  la ricerca e la valutazione USPTO allora l'ufficio brevetti europeo ripete la
  ricerca e quindi pubblica un rapporto di ricerca supplementare con questo
  codice.
- A8 e A9: ripubblicate domande che hanno errore sul titolo o sulla descrizione

Brevetti europei concessi:

- B1: brevetto concesso
- B2: se il brevetto è stato modificato dopo una procedura di opposizione (si
  ha solo nella fase europea e non nella procedura italiana) nel caso le
  rivendicazioni vengano cambiate avviene la pubblicazione
- B3: se c'è stata una procedura di limitazione (richiesta dal titolare)
  dell'ambito di tutela del brevetto concesso per evitare di essere in
  contraffazione con altri brevetti quindi vengono eliminate delle
  rivendicazioni.
- B8, B9: correzione di titoli oppure di errori nella pubblicazione del
  brevetto e quindi viene ripubblicato in versione corretta.

Brevetti PCT (domande internazionali) che hanno questi codici che sono molto
simili a quelli delle domande europee.

- A1 con rapporto di ricerca.
- A2 senza rapporto di ricerca.
- A3: con frontespizio di dati bibliogradici e rapporto di ricerca.
- A4: domanda ripubblicata dopo la fase preliminare della procedura PCT in cui
  ci sono state delle modifiche alle rivendicazioni durante la procedura di
  esame. le rivendicazioni sono dunque differenti e modificate rispetto alla
  domanda A1 o A2 e quindi avviene una ripubblicaizone.

## Slide 24

Brevetti US:
- A: per tutti i brevetti concessi prima del 2 gennaio 2001.
	- A1, A2 A9: domande di brevetto US pubblicate dopo il 2 gennaio 2001
- B: tutti i brevetti concessi in US dopo il 2 gennaio 2001. La presenza dei
  due codici è causata dal fatto che non tutte le domande di brevetto negli US
  vengono pubblicate. Sono pubblicate le domande di brevetto che poi sono
  estese all'estero mentre non sono pubblicate quelle domande che seguono la
  procedura US e il cui titolare chiede che la domanda rimanga segreta fino
  alla concessione del brevetto. Questo potrebbe essere un problema per quanto
  riguarda la ricerca della libertà di attuazione perchè la concessione di un
  brevetto può essere abbastanza lunga ma comunque meno di 2-3 anni per
  concedere la concessione. potrei poi trovarmi una sopresa quando
  commercializzato un prodotto, saltando fuori un brevetto B1 che non era stato
  pubblicato prima.
	- B1: se la domanda di brevetto US non è stata pubblicata dopo 18 mesi.
	- B2: se la domanda di brevetto US è stata pubblicata nei 18 mesi.

## Slide 25

Una descrizione dello stato della tecnica. questa descrizione riguarda un
metodo con un obiettivo.

## Slide 27

Descrizione di quali sono i vantaggi dell'invenzione (diverso da quello
presente sul frontespizio). qui vengono elencati quali sono i vantaggi
dell'invenzione proposta.

Descrizione dei disegni,

Descrizione dei modi in cui realizzare l'invenzione

## Slide 28

Viene riportata la rivendicazione opportunamente limitata mediante la locuzione
"comprising": gli elementi di novità sono presenti nei due sottopunti della
prima rivendicazione.  Le successive rivendicazioni sono dipendenti.

## Slide 30

ci sono diversi step da esegire prima di depositare una domanda di brevetto:

1. ricerca di prior art per vedere se non esistano dei documenti che
   impediranno di ottenere la concessione del titolo.
2. valutazione dei requisiti di novità e di attività inventiva
3. deposito della domanda di brevetto. il primo deposito viene chiamato
   "domanda di priorità". è un deposito nazionale ma anche europeo o
   internazionale.
4. Nei 12 mesi tra il deposito della prima domanda alla successiva bisogna fare
   un'estensione regionale e/o internazionale (a seconda delle strategie di
   business dell'azienda). Potrei farlo anche entro il 18esimo mese (prima
   della pubblicazione della domanda nazionale) ma non potrei rivendicare nella
   successiva estensione PCT, EU o US la domanda di priorità perchè non ci sono
   più i termini.

## Slide 31

in europa più o meno i costi per ottenere dal deposito, alla concessione, alle
convalide nazionali tenendo conto di convalidare il brevetto europeo in non più
di 5 stati sono all'incìirca 30k euro. in cina sono 20k euro con procedura
molto simile a quella dell'EPO. in US si aggirano attorno ai 18k euro. questi
costi non sono tasse ma dovute alle varie azioni ufficiali e sono onorari per
il consulente brevettuale.

## Slide 32

Trattati internazionali: i più utili nelle strategie brevettuali.

- Convenzione dell'unione di Parigi
- Patent law treaty
- Patent cooperation Treaty (cooperazione in materia di brevetti)
- Trattato di Budapest (come depositare i microorganismi)
- Accordo di strasburgo (introduce classificazione internazionale dei brevetti)

## Slide 33

Principi fondamentali del diritto industrale:

1. Priorità: possibilità del titolare di una domanda di brevetto di avere
   questi 12 mesi per poi estendere all'estero la domanda nazionale. Per 6 mesi
   per quanto riguarda i marchi, disegni, modelli industriali
2. Indipendenza dei brevetti: se depositassi una domanda di brevetto in italia
   e poi nei 12 mesi di priorità lo estendo in germania o francia, ciascuna di
   queste domande di brevetto seguirà la procedura nazionale e potrà essere
   concessa oppure respinta indipendentemente dal fatto che in un altro ufficio
   magari viene concessa. Posso ottenere ad esempio la concessione in italia ma
   in germania dove l'esame è più complicato potrebbe accadere di essere
   respinta. Sono territoriali, quindi vengono fatti valere solo sul territorio
   dello stato in cui vengono concessi.
3. trattamento nazionale: ogni stato che ha aderito alla convenizone di unione
   di parigi accorda i cittadini di tutti gli altri stati membri lo stesso
   trattamento riservato ai nazionali. io italiano posso depositare una domanda
   di brevetto in francia, un'altra in belgio. questi due stati non possono
   impedirmi di depositare una domanda di brevetto perchè non sono cittadino di
   quei particolari stati.

## Slide 34

Esistono due tipologie delle procedure di brevettazione:

- ad esame sostanziale: maggioranza delle procedure adottate negli uffici
  brevettuali
- a semplice registrazione: con procedura amministrativa formale (in italia
  avviene per i modelli di utilità). Non vengono esaminati e dopo la
  pubblicazione il modello di utilità viene pubblicato e concesso dall'ufficio.

La validita di queste due tipologie viene determinata in tribunale, in una
causa (di non contraffazione, di nullità, etc.).  Un brevetto quindi si presume
concesso finchè non c'è opposizione e quindi una causa.

Argomenti a favore dell'esame:

- Riduce un numero di cause
- maggiore certezza per gli investitori (business angels etc)
- maggiore qualità dell'intero iter di brevettazione

## Slide 35

Aspetti generali delle procedure:

è possibile depositare in italia una domanda di brevetto e poi all'estero entro
12 mesi dalla data di priorità.

- *Primo grafico*: ho una domada italiana, entro i 12 mesi deposito una domanda
  di brevetto internazionale PCT e dopo 18 mesi da quest'ultima data posso fare
  una qualsiasi fase nazionale estera e/o regionale. L'ingresso in queste fasi
  si ha dopo 30 mesi dalla data di priorità.
- *Secondo grafico*: Se ho una domanda di brevetto europea ho 12 mesi di tempo
  (periodo di priorità) in cui posso fare una domanda PCT o fasi nazionali
  estere.

La data di priorità fornisce il vantaggio di poter presentare la domanda in
giorni diversi in uffici diversi, andando quindi a dilazionare meglio, a
posticipare i costi già elevati.

## Slide 36

Scadenze temporali:
- T0: primo depositito. La domanda di priorità
- T1: estensione internazionale in priorità
- T2: pubblicazione
- T3: scadenza domanda PCT.

La domanda di brevetto internazionale non è una vera e propria domanda di
brevetto (i.e. non conferisce un brevetto PCT): posso ottenere una tutela
provvisoria nei 153 stati aderenti al Trattato di Cooperazione in materia di
Brevetti ma non ottengo un brevetto concesso a livello internazionale. Alla
scadenza del PCT devo fare necessariamente delle fasi nazionali per avere la
tutela in altri paesi.

- *primo grafico*: a $T_0$ deposito la domanda di brevetto. A partire da oggi
  devo calcolare 12 mesi per poter fare a $T_1$ un'estensione PCT in regime di
  priorità (se non lo faccio perdo la priorità). Dopo 6 mesi, a $T_2$, avremo
  la pubblicazione sia della domanda di priorità che della successiva
  estensione PCT oppure regionale, europea etc. Poi dopo 12 mesi da $T_2$
  avremo la scadenza della domanda PCT, quindi dopo  $T_3$ devo decidere se
  fare oppure no le fasi nazionali.
- *secondo grafico*: Supponiamo di avere una domanda italiana, aspettiamo 12
  mesi e faccio PCT poi entro 30-31 mesi dalla data di priorità della domanda
  italiana $T_0$ devo fare le varie fasi nazionali. Come si calcolano i 20 anni
  della scadenza di un brevetto: per quanto riguarda la domanda di priorità
  (brevetto italiano) li calcolo a partire da $T_0$. Per quanto riguarda la
  fase europea bisogna fare riferimento alla prima estensione internaizonale
  PCT, quindi la durata della domanda europea non va calcolata a partire dalla
  domanda di priorità ma dalla prima estensione internazionale, quindi da
  $T_1$. Per la stessa invenzione ho 20 anni di validità per il brevetto
  italiano e sempre 20 anni per quella europea ma viene calcolata dall'anno
  dopo.
- *terzo grafico*: Supponiamo di avere una domanda italiana e nei 12 mesi
  faccio un'estensione presso l'ufficio europeo brevetti. quindi 20 anni
  vengono calcolati da $T_0$ per la domanda italiana, quelli per la domanda
  europea vengono calcolati da $T_1$. Ho una tutela complessiva di 20+1 anni.

## Slide 37

nelle procedure abbiamo la possibilità di deposito a livello internazionale:

- deposito nazionale (estero):
	- In alcuni casi c'è la possibilità di effettuare l'esame di novità
	  differito: per esempio in germania a 7 anni dal deposito. Viene
	  esaminata la domanda su richiesta del titolare che paga le tasse
	  d'esame oppure un terzo che ha interessi commerciali e paga le tasse
	  di esame el a domanda viene esaminata.
	- Si può avere una ricerca separata dalla fase d'esame (i.e.
	  all'ufficio brevetti si paga una tassa di ricerca al deposito della
	  domanda. questa viene pubblicata dopo 18 mesi dalla data di deposito
	  e si hanno 6 mesi di tempo per richiedere l'esame. Se non viene
	  richiesto la domanda viene considerata come ritirata).
	- In alcuni paesi (i.e. Francia) viene eseguito un esame di novità.
	  Viene preso in considerazione solo il requisito di novità e non di
	  inventività. ma viene messo in rapporto di ricerca e il richiedente
	  deve modificare le rivendicazioni oppure non le modifica ma deve
	  presentare una relazione con un commento sui documenti dicendo che
	  non sono rilevanti. In ogni caso questa domanda viene accolta
	  d'ufficio: è un esame di novità che non ha implicazioni forti sul
	  testo. viene concesso un titolo la cui validità dovrà essere
	  determinata durante un contenzioso in tribunale.
- brevetto europeo (fascio di brevetti nazionali)
- procedura PCT

## Slide 39

rimanda al sito dell'uibm. vi sono tutte le informazioni per depositare il
brevetto. ci sono alcune informazioni importanti. Ci sono due modalità per
depositare un brevetto:

1. deposito telematico: bisogna essere registrati, quindi solo i consulenti in
   proprietà industriale sono soliti utilizzarla
2. deposito cartaceo: che raccoglie la modulistica da dover compilare. Si può
   consegnare la documentazione presso una camera di commercio. il modulo del
   richiedente raccoglie i dati bibliografici, titolo, chi sono gli inventori,
   se propongo una classificazione internazionale etc. Il testo della domanda
   di brevetto deve essere allegato con le rivendicazioni tradotte anche in
   inglese, in quanto l'ufficio europeo brevetti va ad esaminarle.

## Slide 40

*Descrizione della procedura*: Supponiamo di avere il deposito della domanda di
brevetto al tempo $T=0$. Dobbiamo compilare la modulistica semplice. Il
fascicolo comprende anche la traduzione delle rivendicazioni. Entro 9 mesi dal
deposito della domanda se sono stati rispettati i requisiti formali che
richiede UIBM (Ufficio italiano brevetti e marchi) allora viene emesso il
rapporto di ricerca e l'opinione scritta da parte dell'ufficio europeo
brevetti. Questo risale a luglio 2008, quando UIBM e EPO hanno sottoscritto un
rapporto per cui tutte le domande di brevetto nazionali per invenzioni (quindi
a esclusione dei modelli d'utilità) vengono esaminati dall'EPO. quindi la
documentazione dev'essere completa all'inizio e si ha 1 mese di tempo per
ovviare a degli errori nella stesura del testo. Entro 5 mesi dal deposito UIBM
invia il fascicolo all'EPO, altrimenti non sono rispettati i termini: potrei
ricevere il rapporto di ricerca a ridosso dell'anno di priorità. il rapporto di
ricerca è uno strumento utile per poter decidere se fare un'estensione
internazionale: se il rapporto è completamente negativo potrebbe non convenire
e quindi abbandono la domanda. Il rapporto di ricerca costituisce un
fondamentale strumento decisionale. L'UIBM fa una valutazione dei requisiti
formali (ci sono i documenti, sono state pagate le tasse di deposito etc) e a
quel punto emette un verbale di deposito, assegnando alla domanda un numero di
deposito e una data di deposito. Dopo di che il secondo step è la pubblicazione
della domanda di brevetto che avviene dopo 18 mesi. Successivamente avviene la
fase d'esame: quando ricevo da UIBM il rapporto di ricerca e l'opinione scritta
vi è in allegato la cosiddetta lettera ministeriale in cui si dice che la fase
d'esame avrà inizio a partire dalla pubblicazione della domanda di brevetto e
il titolare ha tempo 3 mesi dalla pubblicazione della domanda per fare delle
modifiche e per rispondere alla ministeriale. Se non vi è risposta e il
rapporto di ricerca è negativo la domanda di brevetto viene respinta. Deve
esserci una risposta all'esaminatore. Non essendoci un contraddittorio con
l'esaminatore nella procedura italiana è verosimile ottenere la concessione del
titolo. Se ottengo il rifiuto per un disguido procedurale posso fare ricorso.
Il ricorso può essere fatto solo entro certi termini ed entro 60 giorni dalla
notifica di rifiuto della domanda nazionale.

## Slide 41

Questa slide e la successiva sono state lette così com'erano.

## Slide 43

Come depositare la domanda di brevetto. C'è un elenco di documenti che devono
essere compresi nel fascicolo brevettuale. è necessario allegare la traduzione
in inglese delle rivendicazioni altrimenti si paga una tassa di 200 euro e la
traduzione viene fatta in modo automatico. La traduzione automatica fatta in
questo modo non è consigliabile perchè tradurre le rivendicazioni è
un'operazione difficile e molto delicata.

## Slide 44

Sulle tasse di deposito: se tutta la procedura viene fatta online sono solo 50
euro. gli onorari, i costi dei depositi di una domanda di brevetto sono in
larga parte imputate al consulente che poi scrive la domanda (2-3k euro). Se la
domanda non è in modalità telematica le tasse sono più alte. per ogni
rivendicazione oltre la 10 bisogna pagare 45 euro. Il tariffario è crescente a
seconda del numero di pagine della documentazione cartacea.

## Slide 45

L'EPO è l'autorità competente a dare questo tipo di valutazione. Non tutte le
domande vengono inviate ma vengono effettuate delle preselezioni di domande. La
ricerca di prior art riguarda le domande di brevetto per le quali non è stata
rivendicata la priorità. Non viene fatta alcuna valutazione per quelle domande
nazionali che rivendicano una priorità interna, viene fatta solo per i primi
depositi.

## Slide 46

La slide viene letta tale e quale. solo un punto viene ampliato ulteriormente,
il quarto.  Questo è utile perchè se viene citato un documento in giapponese
magari quello fa parte di una famiglia di documenti di cui fa parte anche la
versione US e posso andare a leggere quella.

## Slide 47

Queste sono le tipologie di documenti:

- X: documenti rilevanti, considerati singolarmente, per la novità oppure per
  l'attività inventiva. Nel secondo caso lo sono quando questo documento X
  viene combinato con quello che viene definito "conoscenze comuni generali"
  (i.e. quelle che troviamo comunemente nei libri scolastici). Posso avere un
  documento X o più.
- Y: sono particolarmente rilevanti se combinati con altri documenti della
  stessa categoria. in un rapporto di ricerca troverò due documenti citati con
  la Y. Non posso avere un solo documento Y perché è la combinazione dei due
  che rende valida la domanda in esame.
- A: documenti che definiscono lo stato della tecnica quindi non inficiano i
  requisiti e sono sicuro che quella domanda mi verrà concessa.
- D: quei documenti che ho scritto nella descrizione, nella cosiddetta
  background art posso citare un paio di questi documenti che l'esaminatore
  ritiene rilevanti. Nel rapporto di ricerca ci saranno dei documenti D,X
  (rilevanti per novità) oppure D,Y (documenti rilevanti se considerati insieme
  ad altri documenti Y).

## Slide 48

- Ci sono due documenti US citato nella categoria Y. sono rilevanti per il
  riassunto e le figure della domanda in esame.  Però le rivendicazioni della
  domanda compromesse sono 1,2, 4-8, 15 Ci sono altri due documenti con
  categoria A che non sono rilevanti per i requisiti.  Viene classificata la
  domanda a livello di sottogruppo (B62J11/00 è la classificazion IPC).  Al di
  sotto ci sono i campi che sono stati ricercati dall'esaminatore: sono a
  livello di sottoclasse quindi l'esaminatore ha fatto una ricerca di prior art
  utilizzando questi due codici di classificazione (B62J, A42B).

Il rapporto di ricerca è uno strumento decisionale ma mi permette di fare delle
ricerche di prior art perchè ho sia la classificazione della domanda che i
campi tecnici ricercati.

## Slide 49

nell'ultima pagina vengono definiti tutte le famiglie brevettuali dei documenti
citati nel rapporto di ricerca. Per esempio i primi 3 sono documenti singoli
(non estesi all'estero). L'ultimo docmento (il brevetto concesso in gran
bretagna GB 1564952) è stato esteso in Hong kong, in JP e in Messico. Potrebbe
accadere la situazione opposta e quindi posso andare a leggere il documento in
una lingua a me comprensibile

## Slide 50

Famiglia brevettuale: insieme di tutti i documenti che rivendicano una precisa
invenzione. su espasnet, ricercando questa domanda di brevetto pibblicata negli
stati uniti vediamo che è pubblicata anche come domanda cinese che poi ha avuto
la concessione in Cina; è stata depositata anche la domanda di brevetto Europea
che è stata concessa ma è stato concesso anche il brevetto US. perchè è utile
le famiglia brevettuale? per valutare la distribuzione geografica
dell'invenzione e per utilizzare i brevetti equivalente come sostitutivi di
quelli tradotti.

## Slide 51

tutte uguali con le stesse informazioni. per ottenere la concessione bisogna
rispondere a quanto scritto nella written opinion e dà delle tempistiche.

## Slide 52

Questo è un esempio di opinione scritta. In questo caso è tutta positiva e si
dichiara che tutte e 9 le rivendicazioni della domanda di brevetto sono nuove
ed invientive ed hanno un'applicazione industriale. è presente anche una parte
più descrittiva in cui l'esaminatore elenca i due documenti trovati nello stato
della tecnica: una domanda US e un articolo scientifico e spiega perchè c'è sia
novità che attività inventiva. Dice per esempio quanto rivendicato nella
rivendicazione 1 differisce da D1 (ossia il documento US) per le seguenti
motivazioni. Dice anche che non si possono neanche combinare D1 e D2 e quindi è
inventiva. L'unica obiezione è sulla chiarezza. il composto tetracloruro di
Germanio che è elencato due volte e poi viene indicato che non è chiaro cosa si
intenda per $Ni(CO_4)$ (che sarebbe un composto di coordinazione). C'è un
rapporto positivo ma anche un'obiezione di chiarezza, quindi si deve rispondere
alla ministeriale per ottenere la concessione del brevetto, indicando che cosa
si intende per il composto.

## Slide 53

Questa slide viene letta tale e quale

## Slide 54

si può presentare ricorso se si riesce a dimostrare che UIBM non ha inviato nei
termini corretti la lettera ministeriale

## Slide 55

un altro esempio invece diche che l'omessa risposta alla ministeriale dà come
esito il rifiuto del ricorso. Se ho un rapporto di ricerca negativo e non ho
risposto alla ministeriale non posso fare ricorso perchè questo mi viene
respinto.

## Slide 56

un altro caso invece: posso proporre la conversione del brevetto nullo in
brevetto di utilità ma devo farlo in certi termini. C'è un'apposita domanda di
conversione da fare. Questa è proposta direttamente da UIBM ma bisogna fare
domanda

## Slide 57

la prima tassa annuale si paga a partire dal quinto anno del deposito e per i
primi dieci anni di deposito le tasse sono molto base (fino a 230 euro, esclusi
gli onorari degli studi di consulenza).

## Slide 58

Il rapporto 2020: si ha avuto un trend positivo dei depositi delle domande di
brevetto. Rispetto allo scorso anno c'è stato un maggior numero di domande
inviate all'ufficio brevetti.

## Slide 59

ci sono state concessioni per la maggiorparte ma ci sono state anche delle
coversioni in modelli di utilità, anche provvedimenti di rifiuto e dei ritiri.
il rapporto domande concesse/domande presentate è molto alto.

## Slide 61

la procedura si basa sulla convenzione di washington del 1970 (trattato
multilaterale  in vigore dal '78 in US e dall' '85 in italia).  Si tratta di
una procedura unificata con un unico deposito si ottiene una tutela provvisoria
in tutti i 153 stati membri del trattato di cooperazione in materia di
brevetti. La domanda internazionale produce in tutti gli stati designati gli
stessi effetti legali di una domanda nazionale. La concessione del brevetto
però è di competenza dell'ufficio nazionale che viene designato, in cui si
procede a convalidare la domanda di brevetto internazionale PCT. Non esiste un
brevetto mondiale. Il vantaggio della procedura PCT è che consente di
posticipare l'ingresso nelle fasi nazionali (da 30 a 32 mesi: 31 per la fase
regionale europea, 32 per la Cina).

## Slide 62

Letta tale e quale. Solo una nota: nell'ultimo punto solo alcuni stati devono
eseguire la procedura euro-PCT (esclusa l'italia, la germania etc). Per i paesi
membri del trattato, allo scadere della domanda PCT posso chiedere una fase
nazionale europea senza dover fare una domanda apposita europea precedente.

## Slide 63

Abbiamo un primo deposito (domanda di priorità). Dopo 12 mesi ho un deposito
PCT. Prima della pubblicazione della domanda PCT il richiedente ottiene il
rapporto di ricerca e l'opinione scritta dell'esaminatore (ovviamente pagando
le tasse di deposito e di ricerca). Al diciottesimo mese viene pubblicata la
domanda PCT. Se il rapporto di ricerca è negativo esiste una possibilità per il
titolare di richiedere un esame opzionale (un esame preliminare) e ottenere una
decisione prima della scadenza al ventottesimo mese ossia prima della scadenza
dell'ingresso nelle fasi nazionali. Questo potrebbe essere utile nei casi in
cui ad esempio è facile modificare la rivendicazione, non ci sono tanti
documenti contrassegnati con X e Y. In ogni caso è opzionale: quando si entra
in una qualsiasi fase nazionale, l'ufficio designato potrebbe anche non tenerne
conto. Ad esempio per le domande italiane che hanno gia avuto un rapporto di
ricerca PCT se non faccio nessuna variazione alle rivendicazione otterrò un
rapporto di ricerca identico a quello precedente e in caso di rapporti di
ricerca positivi quando faccio l'ingresso in fase regionale europea l'esame è
molto semplificato. Negli US l'esaminatore dell'USPTO riprende tutta la
procedura di ricerca e di esame.

## Slide 64

Per poter depositare una domanda PCT almeno un richedente deve avere la
residenza o la nazionalità in uno degli stati membri del PCT. La domanda può
essere presentata in una delle 10 lingue della procedura (in particolare
inglese, francese e tedesco sono lingue della procedura EPO). Il fascicolo PCT
deve includere l'indicazione che si richiede una domanda PCT,la designazione
degli stati (dal 2004 in automatico vengono designati tutti gli stati del
trattato), il nome del richiedente, la descrizione e almeno una rivendicazione
(se non ve n'è neanche una la domanda non viene accettata).

## Slide 65

è possibile fare una procedura particolare, la PCT direct: una comunicazione
che viene allegata in fase di deposito della domanda PCT. Ci sono dei commenti,
degli argomenti sulle obiezioni formulate dall'esaminatore nell'opinione
scritta della domanda di priorità.

Se la domanda italiana ha ricevuto un rapporto di ricerca negativo posso fare
un ingresso in fase PCT cambiando le rivendicazioni, oppure se si ritiene che i
documenti citati dall'esaminatore non siano rilevanti si allega una
comunicazione di questo tipo in cui si spiega all'esaminatore perchè questi
documenti non sono rilevanti. Essendo una procedura opzionale potrebbe non
essere tenuta conto dall'esaminatore. Di solito viene presa in considerazione
se esiste solo un documento contrassegnato con X che non è rilevante. serve per
ottenere poi un rapporto di ricerca positivo. è da redigere in inglese.

## Slide 66

ci sono delle novità procedurali perchè dal 1 luglio 2020 è possibile
effettuare dalla scadenza dei 30 mesi l'ingresso diretto nella fase nazionale
italiana, prima bisognava procedere con la fase regionale europea.  Non è
possibile designare uno dei paesi riportati: se volessi avere la tutela in uno
di questi paesi allo scadere del PCT devo per forza fare una fase regionale
europea.

## Slide 68

Come si ottiene la tutela in europa:

- è possibile depositare tante domande di brevetto presso i vari uffici
  nazionali.
- è possibile depositare un'unica domanda europea e dopo la concessione del
  brevetto fare la convalida negli stati in cui l'interesse (dove ci sono i
  competitor dell'azienda, dove l'azienda ha un mercato potenziale).
- si può utilizzare la national-PCT phase: deposito una domanda PCT e dopo 30
  mesi dalla data di priorità seleziono gli stati in cui richiedere la tutela
  (con alcune eccezioni riportate qui e viste prima).
- Depositare una domanda PCT e dopo 31 mesi dalla data di priorità effettuare
  l'ingresso in fase regionale europea.

## Slide 69

La procedura EPO si applica per tutti i 38 stati membri della convenzione sul
brevetto europeo, in più ci sono 2 stati di estensione (Bosnia ed Herzegovina,
Montenegro) e degli stati di convalida (la decisione di concessione può essere
fatta valere in questi paesi [Cambodia, Marocco, Tunisia] che hanno fatto un
accordo con l'Ufficio Europeo Brevetti). Per cui la procedura EPO non è una
procedura dell'unione europea ma che contempla anche stati extra UE. è una
convenzione internazionale a cui possono partecipare tutti gli stati che lo
desiderano.

## Slide 70

Non si ottiene con la procedura EPO un brevetto valido in tutti gli stati
designati ma un fascio di brevetti nazionali, per cui per ogni stato designato
si devono pagare le tasse di convalida (di ingresso in quello stato e di
mantenimento). Potrà poi essere abbandonato, revocato etc indipendentemente
dagli altri stati sulla base di decisioni nazionali perchè poi essendo un
fascio di brevetti nazionali tutte le varie procedure da quelle cautelari, al
contenzioso etc vengono gestite dai tribunali nazionali perchè non esiste un
brevetto unitario. è una procedura che dopo la concession e è dunque
frammentata.

Il vantaggio della procedira EPO + quello di semplificare alcune delle fasi
(deposito, di esame e di concessione) e non ci sono gli iter burocratici dei
singoli depositi nazionali.

Quando conviene l'ingresso diretto negli stati esteri? quando per esempio
l'azienda ha un business che è solo per un paio di stati.

Qual è a differenza di fare un ingresso diretto in uno stato e non di fare la
procedura EPO: Nella procedura dell'EPO il titolo che ottengo ha lo stesso
ambito di tutela in tutti i paesi designati perchè faccio semplicemente una
traduzione delle rivendicazioni concesse ma il paese designato non continua
l'iter di esame perchè già ultimato con la procedura EPO. Se faccio una
procedura nazionale potrei ottenere un ambito di tutela in Italia e in Francia
(ad esempio, in cui gli esami sono più semplici) e restringere l'ambito di
tutela per altri stati come la Germania. Si tratta dunque di un bilanciamento
tra costi e benefici futuri.

Le rivendicazioni non sono necessarie per ottenere una data di deposito: potrei
depositare una domanda di brevetto all'EPO fornendo semplicemente un testo, una
descrizione. Ovviamente perchè venga poi riconosciuta come domanda e quindi
possa farla valere in modo effettivo devo necessariamente completare la
procedura inviando le rivendicazioni entro 2 mesi dal deposito.

## Slide 71

Descrizione delle varie fasi della procedura:

1. Deposito della domanda di brevetto
2. Trasmissione del rapporto di ricerca (6-9 mesi dal deposito se si tratta di
   un primo deposito all'EPO) solo al richiedente perchè la domanda non è
   ancora pubblica.
3. è necessario richedere l'esame entro 6 mesi da quando si riceve il rapporto
   di ricerca altrimenti la domanda è considerata ritirata.
4. pubblicazione di domanda e rapporto di ricerca dopo 18 mesi dal deposito.
5. la concessione avviene dopo 3-4 anni dal deposito.
6. rispetto alla procedura nazionale si ha l'opposizione: quando si ottiene la
   pubblicazione della concessione sul bollettino dei brevetti europei
   qualsiasi terzo a partire da quella data può fare opposizione alla
   concessione (si ha tempo 9 mesi).
7. Nazionalizzazione: il brevetto europeo è un fascio di brevetti e deve essere
   convalidato negli stati di interessi. Senza la procedura di convalida anche
   se ho ottenuto la concessione del brevetto EPO questa non vale nulla. Se non
   avviene la nazionalizzazione quindi si incombe nell ritiro del brevetto.
8. Limitazione e revoca: questi due ultimi procedimenti li può richiedere il
   titolare.


## Slide 72

è schematizzata la procedura EPO in modo più dettagliata:

1. deposito. dovrei pagare delle tasse di deposito. potrei anche non farlo ma
   in quel caso otterrei solo la rivendicazione della data di priorità.
2. Esame formale in cui si verifica la completezza del fascicolo.
3. Si paga una tassa di ricerca per cui viene emessa un rapporto di ricerca ed
   un'opinione scritta.
4. La domanda viene pubblicata 18 mesi dopo
5. Inizia l'esame sostanziale (quello dei requisiti): il brevetto può essere
   concesso oppure rifiutato. Se concesso viene pubblicata la domanda (il
   testo). Se viene respinta la domanda si può fare appello.
6. Entro 9 mesi dalla pubblicazione del testo del brevetto concesso c'è tutta
   la parte di eventuale opposizione di qualsiasi terzo che abbia un interesse
   economico a far sì che non venga concesso quel brevetto. L'opposizione può
   fornire tre diversi output:
	- il brevetto può essere mantenuto com'è gia stato concesso perchè non
	  c'erano gli estremi di mancanza di novità o di inventività.
	- il brevetto può essere limitato: limitato in termini di ambito di
	  tutela, ad esempio l'applicant deve accorpare le rivendicazioni
	- il brevetto può essere revocato: l'opponente trova un documento che
	  dimostra la mancanza di un requisito fondamentale del brevetto,
	  quindi il brevetto viene revocato.

Per tutti questi output l'opponente e/o il titolare possono fare opposizione.
il titolare può effettuare opposizione per la revoca oppure perchè non
considera corretto il fatto che la domanda sia stata emendata.
7. Il titolare può limitare poi il brevetto perchè si accorge che per esempio
   può essere un documento ostativo (che può ostacolare) sulla validità
   successiva nelle fasi nazionali oppure può essere addirittura revocato.

La procedura è molto più complessa di quella italiana.

## Slide 73

Al' EPO è possibile richiedere una procedura accelerata sia per la fase di
ricerca che per la fase di esame sostanziale. La prima comunicazione (office
action) è emessa entro 3 mesi dalla richiesta di esame. Può essere richesta in
qualsiasi momento, meglio se alla data di deposito.

Viene mostrata la tempistica che viene seguita nella procedura europea. Si ha
un inizio dell'esame sostanziale, dei requisiti entro 24 mesi dal primo
deposito. La prima azione ufficiale dell'esaminatore la si ha il 29-30esimo
mese. in media sono 50 i mesi per ottenere la concessione del brevetto europeo.
Ci sono dei tempi che possono essere anche abbastanza stretti per rispondere a
qualsiasi azione ufficiali dell'esaminatore (4 mesi più un eventuale proroga di
2 mesi). le stesse considerazioni valgono per l'esaminatore se richiedo la
procedura accelerata, altrimenti no.

## Slide 74

per quanto riguarda la concessione: la prima notifica ricevuta dal richiedente
è quella dell'intenzione alla concessione. La comunicazione arriva con un testo
che il richiedente deve approvare. Di solito ci sono anche lì delle modifiche:
se viene approvato entro 4 mesi dalla notifica di concessione, l'applicant deve
depositare la traduzione delle rivendicazioni nelle altre due lingue ufficiali
dall'EPO (i.e. se ho seguito la procedura in inglese dovrò tradurre i claims in
francese e tedesco). Devo anche pagare le tasse di concessione e stampa. Fatto
questo in un mese ho la decisione di concessione.

Finalmente poi ho la data di concessione ossia la menzione della concessione
nel bollettino ufficiale dell'ufficio europeo brevetti.

Dalla data di concessione parte il periodo di opposizione e anche il periodo in
cui il titolare deve effettuare le convalide nazionali. Entro 3 mesi dalla
pubblicazione bisogna pagare le tasse di convalida e probabilmente anche le
tasse annuali nei paesi in cui si è deciso di convalidare il brevetto europeo
concesso.

## Slide 76

si distingue in due fasi:

1. Deposito: è possibile fare una domanda di brevetto provvisoria (Provisional
   Application): semplicemente un testo (i.e. articolo scientifico). nell'arco
   di 12 mesi, avendo depositato una domanda provvisoria oppure un'altra
   domanda provvisoria o una non provisional (detta anche utility) application
   (cioè la domanda di brevetto vera e propria) posso fare un'estensione
   internazionale con procedura PCT. Le domande provvisorie hanno una durata
   limitata (i.e. un anno di validità). Però queste domande provvisorie non
   sono pubblicate nei database brevettuali e se non vengono estese a livello
   internaizonale o come domanda non provvisoria (come utility) decadono, è
   come non aver depositato nulla. Se depositassi una domanda regolare (regular
   application, che corrisponde alla non provisional application) devo anche
   depositare una Declaration of inventorship (dichiarazione d'inventore) e un
   assignment. in questo caso gli inventori (persone fisiche) cedono
   all'azienda i diritti patrimoniali.
2. L'esame: parte circa dopo un anno dal deposito della domanda US. Ci sono
   delle formalità. è un esame formale e bisogna aver sottoscritto una lettera
   di incarico (power of attoney) in cui ho domandato ad un patent agent US di
   seguire la procedura. Ci sono poi delle formalità: il ricevimento del
   deposito ufficiale e la pubblicazione. Successivamente bisogna depositare
   l'IDS (information disclosure statement): questo è un documento in cui il
   titolare deve dichiarare tutti i documenti di prior art a sua conoscenza.
   Deve farlo con diligenza perchè questo non completo può essere una causa di
   nullità da parte dell'USPTO.  Successivamente iniziano le office action che
   nella procedura US sono solamente due:
	1. un'office action non finale
	2. office action finale. questa potrebbe essere una comunicazione di
	   rifiuto.

Nel caso in cui la final office action sia una comunicaizone di rifiiuto quello
che il titolare può fare è replicare depositando anche quella che viene
chiamata "continuation" o RCE (Request Continuing Examining). Si richiede a
USPTO di continuare effettivamente l'esame pagando un ulteriore deposito.
mentre le azioni ufficiali sono sempre due all'USPTO (all'EPO non ci sono
limiti alla numerosità di azioni legali) posso ottenere la notifica di
concessione oppure il rifiuto. se voglio continuare deposito le continuation
(che possono anche essere continuation "in parte" se aggiungo elementi tecnici
che non erano presenti nella domanda di priorità. questo è concesso negli US ma
non in EU). La replica nel caso di una final office action prevede una risposta
all'esaminatore e un deposito della RCE, oppure posso rispondere e fare
l'appello. è meglio depositare la continuation perchè se perdessi l'appello la
domanda sarebbe respinta completamente e definitivamente. Utilizzando il
sistema delle continuation posso andare avanti indefinitamente a seconda delle
finanze di cui dispongo. Le domande divisionali e le varie continuation devono
essere depositate prima della concessione del brevetto. Quindi in una
continuation in un certo senso cambio le rivendicazioni (non aggiungo nuova
materia ma cambio il wording delle rivendicazioni). nella continuation in parte
aggiungo invece nuova informazione tecnica.

## Slide 77

- Provvisoria: 1 anno di tutela e può essere presentata senza rivendicazioni
- Continuation: da la possibilità di aggiungere, di modificare il set di
  rivendicazioni.
- Continuation in part: dà la possibilità di aggiungere degli elementi tecnici
  che non erano presenti nella domanda di priorità.
- Divisionale: non c'è unità di invenzione, allora l'esaminatore invia una
  restriction requirement, dunque devo suddividere la domanda di priorità in
  più domande divisionali.

## Slide 79

Una procedura molto simile a quella dell'ufficio europeo brevetti con la sola
differenza che la concessione del brevetto euroasiatico varrà in tutti questi
stati. non viene considerato come un fascio di brevetti ma è effettivamente
valido per quelli. Se però ottengo un rifiuto posso fare un ingresso dei vari
paesi, ovviamente con una procedura di deposito e di esame. L'unica difficoltà
è che si tratta di un brevetto difficilmente azionabile in questi stati. nel
2019 sono state depositate 3k domande che rispetto alle 10k italiane sono
poche.


# Riferimenti bibliografici

Slides "procedure di brevettazione"
