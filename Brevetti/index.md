# Brevetti

| Lezione                      | Argomento                      | Rielaborata | Difficoltà | Slides comprensibili | Giorni |
| :---                         | :---                           | :---        | :---       | :---                 | :---   |
| [01](Franzoni/01/Lezione.md) | Brevetti e requisiti (1)       | Sbobinata   | Semplice   | Sì                   | 23.02  |
|                              | Brevetti e requisiti (2)       | Sbobinata   | Semplice   | Sì                   | 02.03  |
| [02](Franzoni/02/Lezione.md) | Diritti brevettazione e IP (1) | No          | ?          | ?                    | 02.03  |
| [03](Franzoni/03/Lezione.md) |                                | No          | ?          | ?                    | 09.03  |
| [04](Crippa/04/Lezione.md)   | Testimonianza                  | No          |            |                      | 16.03  |
| [05](Barbieri/05/Lezione.md) | Procedure Brevettuali          | Sì          | Semplice   | Sì                   | 23.03  |
| [06](Barbieri/06/Lezione.md) | Ricerche Brevettuali           | No          |            |                      | 30.03  |
| [07](Barbieri/07/Lezione.md) | Ricerche Brevettuali           | No          |            |                      | 09.04  |


| Giorno | Link                                                                                                |
| :---   | :---                                                                                                |
| 23.02  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=4b6c95ccd6a74574a890df2d7d70da1a |
| 02.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=99a79e3e01454628bffa297890a43a9b |
| 09.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=feaefaa14c704fb68d8ac4ffccd56ebd |
| 16.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=914cdb88256e4ea9ab32646ab2dd5588 |
| 23.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=f633659b97fc406090eb1de799142ca1 |
| 23.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=cd1b77df5c704f1a967e925911acff74 |
| 30.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=084d99aaa70b463ca01cb12643ebe13a |
| 30.03  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=0a1360c007aa44f897d3fba027199679 |
| 09.04  | https://politecnicomilano.webex.com/politecnicomilano/ldr.php?RCID=3c06c136460541bda8e9181201d08550 |

# Modalità d'esame

Esame semplice, a quanto pare può essere scritto oppure orale a seconda del
numero di persone che lo deve svolgere.  C'è il salto d'appello nel caso la
valutazione sia insufficiente.
