# Lezione 1

## Lettura del documento riportato nella cartella.

Articolo molto attuale sul Covid-19 e sulla brevettazione. Scritto a novembre
quando i permessi per il loro utilizzo non erano ancora stati rilasciati.
L'articolo è particolarmente attuale perché aveva già previsto che vi sarebbero
state tensioni relative al fatto che questi vaccini, ancorché esistenti,
sarebbero stati difficili da reperire sul mercato e ciò determina una
distribuzione diseguale anche delle possibilità di essere vaccinati.

Ci sono tanti temi di natura diversi in questo documento:

- *Economici*:
	- Costi della ricerca e sviluppo
	- Scelte dell'impresa legate alla brevettazione o rilascio del
	  brevetto.
	- La criticità di queste scelte in ambito economico che può determinare
	  l'andamento azionario. (una notizia relativa ad un singolo caso di
	  brevetto può determinare dello sbalzo del valore azionario di un
	  impresa drammatico, come quello citato nel documento)
- *Legali*:
	- Bayh-Dole act: legge importante in USA.
	- Cosa è possibile fare o no in riferimento alla normativa dei brevetti
	  (Marching Right)
- *Etici*:
	- Discendono da come si è deciso di gestire la *proprietà
	  intellettuale*: molto spesso che la pi vada a cadere su qualcosa che
	  ha un diretto impatto sul benessere delle persone e quindi assegnare
	  un diritto privato su qualcosa che determina il benessere di
	  tantissime persone è sempre un molto delicato e crea tensioni. La
	  domanda è se per proteggere questo diritto privato è legittimo
	  pregiudicare la salute di tante altre persone.
- *Procedurale/amministrativi*:
	- I Diritti brevettuali sono molto complessi, quindi anche quando sono
	  stati determinati si creano molti contenziosi perchè è difficile
	  dipanare la matassa e ci sono interpretazioni diversi che portano a
	  scontri e difficoltà a far valere dei diritti che apparentemente
	  crediamo di avere. C'è una serie di procedure che può essere seguita
	  tra cui quella della condivisione open dei vaccini (o comunque dei
	  brevetti) e quello dei patent pool (particolari accordi di licenza di
	  royalty che ti consentono di ottenere determinate finalità).

Sono messe in luce tutte le cose che vogliamo trattare in questo corso.
L'obiettivo è cercare di capire esattamente tutte le tematiche che vengono
analizzate in profondità.  Cosa significa un patent pool?  Cosa significa in
nascere dei contenziosi? Come immaginiamo siano risolti etc.  Avere una visione
da insider di ciò che accade nel tema brevettuale.

Prima parte: di taglio economico, poi legale (cosa dicono le normative, che
sono legate ai paesi che le emanano e possono esserci discrepanze tra normative
dei vari paesi.), logiche strategiche con cui le aziende utilizzano i brevetti,
per capire che senso ha fare causa ad un impresa per dei brevetti pregressi,
piuttosto che fare un patent pool o rilasciare in modo open un ... di brevetti.

## Brevetti ed i loro requisiti

### Slide 2

Brevetti, cosa sono dal punto di vista economico.  Brevetto: diritto a
sfruttare economicamente e commercialmente i frutti di un'invenzione (o opera
di ingegno) in via esclusiva e per un periodo limitato di tempo. Il diritto
viene assegnato a chi è titolare del brevetto in via esclusiva per un tempo
limitato. Questo non è ovvio perché, considerando i marchi, questi non decadono
se continuano ad essere utilizzati.

- Per ottenere un brevetto l'invenzione deve soddisfare dei requisiti.
- Il brevetto viene concesso a chi ha compiuto lo sforzo creativo ma talvolta
  non è una singola persona a farlo. Il ricercatore di "AstraZeneca", per
  esempio, fa parte di un pool di ricercatori che comunque sono assunti
  dall'impresa che fornisce laboratori, materiale, formazione, accordi
  commerciali per produrre il brevetto. Quindi non è semplice indicare chi
  effettivamente detiene il diritto di possesso del brevetto. Come si gestisce
  questo complesso insieme di diritti che ci sono? Normalmente il brevetto
  viene concesso all'impresa di questi inventori e a quest'ultimi è
  riconosciuto il diritto di paternità dell'invenzione (il nome verrà ricordato
  sulla normativa brevettuale come se fosse un diritto morale → simile diritto
  d'autore). L'impresa invece possiede il diritto di sfruttamento di questo
  brevetto.
- Il diritto a possedere il brevetto non lo si detiene immediatamente ma si ha
  solo il diritto a chiederlo in quel momento ma il diritto a sfruttare
  commercialmente il brevetto viene concesso dopo una procedura nella quale
  vengono verificati i requisiti per ottenere i brevetti. Il diritto di
  brevetto sorge solo quando l'ente terzo (agenzia di concessione dei brevetti)
  lo assegna. Solo allora e subordinatamente a questa procedura. Questo non
  capita ad esempio con il diritto d'autore che sorge immediatamente con la
  canzone ad esempio. Una delle implicazioni di tutto questo è che possiamo
  avere un'invenzione e di non brevettarla. Può essere quindi circolata
  liberamente o essere utilizzata.
- Alla scadenza del periodo di esclusiva il brevetto diventa libero: decade il
  diritto ad esercitare l'utilizzo in esclusiva da parte del proprietario. Ciò
  significa che il brevetto è un diritto temporaneo perché per sua stessa
  natura sorge in modo temporaneamente limitato. Quando decade allora tutta la
  società potrà utilizzare tutto questo senza dovere nulla all'inventore
  direttamente.


### Slide 4

Alla finfine un brevetto si presenta come un documento testuale, normalmente di
una decina di pagine. Questo è particolarmente rilevante. È depositato il 4
gennaio del 79 quindi già scaduto. È stato rilasciato dalla USPTO (United
States Patents and Trademarks Office). Il rilascio (dicembre 80) è leggermente
successivo (1 anno dopo) rispetto al giorno del deposito (4 gennaio del 79).
- Ci sono i nomi degli inventori: Cohen e Boyer:
	- Stanley Cohen ricevette il premio Nobel insieme alla Montalcini →
	  scoperta del Nerve Growth Factor. Lavorava nella biologia molecolare.
	  Leggi la storia alla pagina seguente. Introduce plasmidi vettori di
	  antibiodici in alcuni tipi di batteri
	- Boyer: scienziato che aveva ideato un metodo per tagliare dei
	  segmenti di DNA che contenevano determinate proteine e per attaccare
	  i segmenti ad altre porzioni di DNA.
- Assignee: Chi detiene il diritto allo sfruttamento commerciale. Boards of
  Trustees of Stanford University.
- La combinazione dei metodi scoperti da questi due scienziati potevano essere
  combinati. Brevettano la tecnica del DNA ricombinante. Un gran balzo per le
  biotecnologie. Il deposito del brevetto è avvenuto solo nel '79, il che è
  particolarmente strano.
- Il loro brevetto non era estendibile fuori dagli stati uniti o nei sistemi in
  cui vigeva la possibilità di avere il grace period. Siccome loro avevano reso
  pubblica la loro invenzione prima	di chiedere il brevetto questa loro
  pubblicità ha reso invalido il brevetto in tutti i paesi in cui il requisito
  della novità è applicato in modo forte ossia anche verso le divulgazioni che
  lo stesso autore fa del suo lavoro. Negli USA c'era la possibilità di
  mantenere un'application brevettuale se la divulgazione che l'autore ne aveva
  fatto era avvenuta di fatto entro i 12 mesi precedenti. Questo brevetto è
  rimasto molto lucrativo per tanti anni (con un incasso di 255 milioni di
  Euro, considerati al valore degli anni '80). Ma questo brevetto non verrà mai
  esteso fuori dagli stati uniti → in Europa abbiamo potuto beneficiare di
  questa invenzione senza dover pagare nulla all'università di Standford perché
  di fatto il brevetto non avrebbe avuto validità fuori dall'Europa.

### Slide 6

Questo è l'ultimo dato disponibile rilasciato dal Worldwide International
Patent Office (Ufficio brevettuale internazionale gestito dalla WTO - World
Trade Organization, che ha sede a Ginevra) che ogni anno stila le statistiche
generali relative a circa 150 uffici brevettuali sparsi all'interno del
pianeta. Quest'anno ha pubblicato i dati relativi al 2019. Questi dati ci
dicono che nel corso del 2019 (pre-Covid) erano stati chiesti n brevetti.
Questo dato non è dissimile da quello precedente, in cui si erano registrati
circa 1000 brevetti in più. Ciò significa che nel 2019 si è registrata una
flessione del 3% circa rispetto al 2018.

Brevetti in uso → un brevetto è valido per circa 20 anni, quindi quelli che
sono in uso in un certo periodo sono di più di quelli che vengono rilasciati in
un solo anno.

La quantità di brevetti in uso è in crescita rispetto al 2018 del 7% circa.


### Slide 7

Questo grafico è stato generato dallo stesso Office di prima.  Sul totale delle
domande di brevetto mondiali come queste si distribuiscono per i vari paesi. La
stragrande maggioranza dei brevetti richiesti ogni anno proviene dalla Cina che
da sola richiede circa 1 milione di brevetti all'anno. La Cina è un paese molto
grande ma si tratta di una cifra spropositata. Il cerchietto trasparente indica
il dato dell'anno precedente (2017). Nel 2019 la Cina ha chiesto meno brevetti,
con una flessione del -11% quindi vicino al pallino trasparente di questo
grafico. La ragione di questa flessione è l'entrata in vigore di una serie di
disposizioni del governo cinese per rendere più rigoroso l'esame e anche la
concessione dei brevetti nel tentativo di aumentare la qualità dei brevetti.
Non è necessario solo avere un brevetto e basta ma che sia di qualità perchè se
venisse portato in tribunale questo potrebbe tenere, altrimenti se si
concedessero brevetti che in tribunale venissero annullati si avrebbero solo
molti aggravi legali per tutti e nessuna certezza per il brevetto. Siccome la
Cina aveva una tradizione di application fatta un po' alla buona, ha dato una
stretta nel corso del 2019 e quindi sta un po' ricontenendosi. Rimane però il
paese che ha in assoluto il maggior numero di application brevettuali. EPO
(tutti i paesi che aderiscono all'European Patent Office, che non coincide
esattamente con l'Europa stessa ma un'insieme di Paesi che vi aderiscono). È al
quinto posto quindi l'europa non è un posto in cui si brevetta molto.



### Slide 8

Ambiti brevettuali dei vari paesi. Questo è un breakdown per tipologia di
disciplina. Per quanto riguarda ad esempio gli apparati elettronici il primo
paese a brevettare è il Giappone, a pari merito con Germania. Nell'ambito
computing il primo paese sono gli Stati Uniti poiché il software è brevettabile
lì.


### Slide 9

Definizione di European Patent Office: l'EPO è un ufficio che gestisce i
diritti brevettuale di una compagine di paesi più o meno ubicati nella zona
dell'Europa. L'EPO esiste dapprima della Commissione Europea e attualmente
aderiscono attraverso degli accordi di carattere bilaterale (del singolo stato
con l'EPO) tutti i paesi che sono evidenziati in rosso su questa slide. Sono
tutti paesi della CE più altri paesi. Si può notare anche la Turchia e il Regno
Unito anche se non fanno parte della UE, così come la Svizzera. Quelli azzurri
hanno rapporti privilegiati con l'EPO senza però farne parte a tutti gli
effetti. La cosa più importante da comprendere è che EPO è diverso da UE. EPO è
un ente sovranazionale a cui tanti dei paesi europei hanno assegnato
dell'operatività sui brevetti. Rimane il nostro ufficio brevettuale di
riferimento dal momento che noi abbiamo l'UIBM (Ufficio Italiano dei Marchi e
dei Brevetti con sede a Roma presso il Ministero dello Sviluppo Economico). Il
nostro ufficio è in grado di relazionarsi direttamente con l'EPO che ha due
sedi (Monaco, Germania e a Rijswijk, sobborgo de L'Aia, Olanda). Attraverso
queste due sedi gestisce i brevetti in collaborazione con gli uffici
brevettuali nazionali dei paesi membri.


### Slide 10

Nel video viene mostrato il trend temporale che evidenzia una lieve crescita
nel tempo. Vediamo poi i vari paesiche applicano nell'EPO. Questi non sono
necessariamente solo paesi europei ma visono anche gli USA, la Cina e la Korea.
L'epo quindi gestisce la proprietà intellettuale dei paesi membri ma riceve
anche estensione di brevetti dagli altri brevetti. I paesi maggiormente
rilevanti dal punto di vista brevettuali anche loro hanno una grossa fetta del
totale dei brevetti rilevati dall'EPO.  I paesi EPO hanno incrementato
dell'1.1% i loro brevetti nel tempo ma simultaneamente questo incremetno è
stato del 5.5%, la Corea +14.1%, il Giappone in flessione (-2.3%) e la Cina le
applicazioni brevettuali all'EPO di origine Cinese sono cresciute del 30%
circa.  Vengono mostrati i field tecnologici più importanti (vince l'IT).  Nel
grafico delle Top Applicant sono mostrate quelle aziende aventi sede legale in
uno dei paesi membri dell'EPO. L'impresa che singolarmente ha depositato più
brevetti e fa parte dell'EPO è Siemens che però è soltanto quinta nei Top 10.
La stragrande maggioranza delle application brevettuali viene effettuata da
imprese grandi e solo il 18% da imprese piccole o inventori individuali e una
non irrilevante percentuale di brevetti di natura universitaria.

Quello che si vuole sottolineare in questo video, in particolare nei grafici in
cui si mostrano anche gli altri paesi non EPO è che esistono delle normative
internazionali che consentono di estendere, una volta posseduto il brevetto,
internazionalmente. Questo può essere esteso solo in paesi che riconoscono il
brevetto originato nel paese d'origine. Esiste una normativa internazionale che
è stata siglata da tutti i paesi membri della WTO. La risposta breve è che se
fai parte di un paese membro della WTO (più o meno tutti tranne tipo la Nord
Corea) allora puoi avere che gli altri paesi della WTO accettino le tue
priorità (se sono Huawei e deposito il brevetto in Cina, con dei requisiti tipo
da fare entro i 12 mesi etc. Chiedo tramite l'ufficio cinese di portare il mio
brevetto anche all'EPO questo tecnicamente si risolve in un nuovo filing che
viene aperto all'EPO. Questa EPO avendo giurisdizione rispetto tutti i suoi
paesi membri a sua volta riesaminerà l'applicazione brevettuale e sarà in grado
di concedere o rifiutare il brevetto.).Dal punto di vista dell'EPO le
application sono sia originali, ossia che partono dai suoi paesi e vanno verso
altri, ma anche application di paesi esterni che chiedono estensioni nei paesi
di copertura dell'EPO.

Come visto nel video è che questa tipologia di clienti è superiore a quella dei
paesi membri dell'EPO. Questo significa che all'estero la brevettazione è oresa
molto seriamente e l'Europa non deve credere di essere perciò all'avanguardia
in questo ambito anzi quello che si vede è che dal punto di vista dei nuovi
investimenti lo sviluppo di nuove tecnologie in questo momento sta avvenendo
primariamente fuori dall'Europa, sempre più in Asia più che negli Stati Uniti.


### Slide 11

Una domanda interessante: se io inventore decidessi di non brevettare, qualcun
altro potrebbe venir fuori con la mia stessa idea, brevettarla e
successivamente ricavare dei guadagni da essa?  _A_: Se l'inventore della
suddetta non la divulgasse per tempo c'è il rischio che accada. Se però si
decidesse di prendersi il carico della diffusione tramite un mezzo di
comunicazione o presentandola a delle conferenze allora questo brevetto non
potrebbe essere più richiesto perché non vi è più il fattore di novità.
Soltanto in alcuni paesi è consentito un ripensamento solo all'inventore (grace
period: diritto al ripensamento per un periodo limitato ossia 12 mesi, ad
esempio negli Stati Uniti. L'EPO non la concede). Quando avviene la
divulgazione prima il diritto è largamente pregiudicato, ilbrevetto è "sporco"
e non è possibile avere una proprietà piena del brevetto.


### Slide 12

Visione economica del brevetto.  Come mai la normativa legale è parsimoniosa,
tende a non concedere il brevetto e a concederlo sempre in forma molto
limitata.  Caratteristiche notevoli dell'invenzione: questa è un'idea, un
artefatto tecnico che avrà un principio di funzionamento. Primariamente è
qualcosa che ti permette di realizzare ciò che non era stato realizzato prima.
L'idea possiede quindi un valore perché riuscire a realizzare qualcosa che
prima era inesistente produce un effetto particolarmente positivo. In fin dei
conti è un'attività inventiva e come tale è di carattere immateriale: posso
raccontarla e qualcun altro può produrla. In questo senso questo bene, l'idea,
ha due caratteristiche molto importanti che gli economisti chiamano:

- non rivalità: Un brevetto e in generale un'idea è non rivale nel senso che
  quando la comunico, quando comunico la mia idea la continuo a possedere. Non
  posso mai trasferirla spogliandomi di questo bene → il bene si duplica
  anziché spostarsi di mano. Diverso è per un bene materiale, il cui consumo è
  rivale (se la uso io è mia, se la passo a qualcuno non è più mia). Se
  possiedo un'idea di come risolvere il problema tecnico posso utilizzarla sia
  io che gli altri a cui l'ho divulgata godendo del beneficio del bene "idea"
  in modo non rivale, non pregiudicando il consumo da parte di altri.
- non escludibilità: il godimento del bene è non escludibile nel senso che se
  avessi una matita e la regalassi non potrei più utilizzarla ma se poi
  riuscissi a riprenderla riuscirei ad escludere nuovamente quello a cui
  l'avevo regalata in primo luogo. Questo non accade per quanto riguarda un
  bene immateriale che non può essere dimenticata se mi pentissi di averla
  detta.

Per queste caratteristiche le idee inventive sono di fatto difficilmente
appropriabili (sono imperfettamente appropriabili, parlando da economista).
Imperfettamente appropriabili: mentre posso avere una proprietà di un oggetto e
gestire il godimento del bene, se questo fosse non materiale il mio godimento
sarebe imperfetto e non riuscirei a farlo mio in modo completo perche se mi
sfuggisse e si diffondesse senza che io riuscissi a controllarlo non riuscirei
a riprendermelo indietro perché è non escludibile. Di fatto succede che se una
persona produce un artefatto quello è suo e quindi è nei chiari diritti di
proprietà su questo. Potrebbe patire il fatto di essere espropriato di
quest'idea senza che nessuno gli dia qualcosa in cambio. È difficile far valere
i propri diritti quando l'oggetto è un'idea immateriale.

### Slide 13

In considerazione di questa difficoltà, di fatto c'è un primo problema
economico: in questa situazione ci sono molte persone disposte a fare come gli
agricoltori (remunerazione certa in quanto il bene che forniscono è materiale
quindi i diritti di proprietà sono forti) ma non ci saranno molte persone
disponibili a fare gli inventori perché di fatto una persona anticipa il
problema che quando arriverà al risultato di un successo questo potrebbe
essergli espropriato senza che gli dia conto. Il problema naturalmente è che
l'inventore spende tempo e denaro per giungere al risultato dell'invenzione, il
costo di fare ricerca e sviluppo è sempre più elevato e non ci sono quasi più
settori in cui si innova com'era possibile farlo all'epoca di Tesla con un po'
di cose nel garage. Innovazione richiede avere metri quadri di laboratorio,
macchinari e reagenti costosi etc... La R&D è un'attività ad alta intensità di
capitale: servono tanti soldi per sviluppare qualcosa di decente. Questi
investimenti rendono ancora più forte questo problema economico perché
l'attività inventiva è il risultato di tanti investimenti e di tanto denaro e
se non sono sicuro di recuperarlo in qualche maniera sarei scoraggiato. Questi
risultati in aggiunta sono pure incerti (se dal punto di vista dell'agricoltore
è possibile assicurarsi contro degli eventi avversi che potrebbero verificarsi,
basti pensare ad alluvioni o siccità) perché la ricerca è completamente ignota
e tipicamente i rischi non sono assicurabili. Non posso trasferire il rischio
ad altri attivando il risarcimento da parte di terzi. Gli economisti dicono in
senso tecnico che non è possibile descrivere una distribuzione di probabilità
in cui questo rischio si verificherà → non esistono compagnie assicurative che
sono disposti ad assicurare da questo rischio.


### Slide 14

Soluzione al primo problema economico: trovata nella pratica: Diritto esclusivo
allo sfruttamento dell'opera di ingegno. In questa maniera il diritto non si
forma da solo perchè il carattere di immaterialità rende difficile gestire
questo diritto ma posso introdurlo per legge. Anche se tu hai sentito parlare
di questa idea e tecnicamente sei in grado di riprodurla faresti un'attività
illegale nel riprodurla perché è stato stabilito di dover dare un diritto in
esclusiva all'ideatore di quest'idea. Viene introdotto artificialmente,
attraverso il sistema normativo, una forzatura all'esclusiva. L'inventore può
ora non temere che se la sua invenzione diventasse pubblica altri possano
utilizzarla perché anche se la conoscessero non possono trarne beneficio ma
devono chiedere a lui se possono legittimamente utilizzarla. La diffusione
avviene senza danno per il suo sfruttamento economico


### Slide 15

Secondo tipo di problema economico: questo sorge come conseguenza della
risoluzione del primo problema. Un diritto in esclusiva ad una persona crea il
problema del monopolio della persona o dell'impresa a questa particolare
tecnologia. Questo è un problema dal punto di vista economico perché il
monopolio di fatto produce in una condizione di prezzo superiore a quella del
prezzo di concorrenza di mercato (il monopolista di solito sceglie un prezzo
alto che massimizza le proprie entrate). Questo prezzo alto fa si che in realtà
si patisca della perdita secca di monopolio (c'è una parte della produzione che
avremmo potuto fare senza perderci ma che non facciamo, perdendo godimento,
perché il monopolista ottiene un sovrapprezzo rispetto al prezzo normale di
mercato).  In concorrenza si ha un prezzo di vendita più basso perchè bisogna
calibrare il prezzo sulla base anche della concorrenza (→ il prezzo sarà più
basso e aumentano le quantità di beni scambiati → profitto maggiore). Essendo
in monopolio questo impone un prezzo più alto vendo meno quantità di beni perdo
quindi il surplus.  Il nocciolo del discorso è che il monopolio, rispetto alla
concorrenza è un sistema più inefficiente. Ogni volta che c'è un monopolio i
consumatori stanno un po' peggio. Se introducessi un monopolio per consentire
il brevetto avrei una perdita generale di efficienza.


### Slide 16

Soluzione: Il monopolio esiste si ma in forma temporanea. Il monopolio non è
per sempre ma sempre limitato nel tempo (mai superiore a 20 anni per i
brevetti). Questa durata può decadere prima (obsolescenza, l'inventore è sazio,
etc...), quindi è bene che il monopolio venga sciolto il prima possibile. In
questo modo si può godere in concorrenza della tecnologia che prima era
brevettata.  Il monopolio viene dato limitato nel tempo ma anche in altri modi,
ad esempio nello scopo: voglio dare l'invenzione solo ed esclusivamente per
l'attività inventiva di quell'inventore. Ad esempio se l'inventore ha
ricombinato delle parti già esistenti non voglio di nuovo remunerare quei
singoli pezzi a lui non dovuti ma solo ciò che è frutto del suo ingegno, ciò
che lui ha aggiunto.  Limitazione geografica: se l'inventore chiede di avere
un'esclusiva in un paese e non in altri significa che il compenso dato da quel
paese è sufficiente a giustificare i suoi costi e non ha senso che io estenda
al di fuori di quel paese il regime inefficiente del monopolio. La meglio per
la collettività è che vi siano abbastanza incentivi da indurre una persona ad
inventare ma una volta inventato il meglio sarebbe "espropriarlo" dal punto di
vista della collettività. Chiaramente espropriandolo porterebbe l'inventore a
non creare più, perdendo dunque in efficienza dinamica.

Quindi risolvo i problemi economici in due tempi:

1. Do un monopolio per incentivare gli inventori ad inventare
2. Lo limito temporalmente, geograficamente e nello scopo per tenere il più
   possibile ridotto il regime di inefficienza generato dal monopolio.



### Slide 17

La normativa brevettuale è tesa a dare un monopolio ma che lo fornisce in modo
molto circoscritto. Fornisce un forte incentivo per l'attività economica ma
dato solo a condizione di essere temporaneo e limitato e che allo scadere
dell'esclusiva immediatamente tutti possano cominciare ad utilizzare
l'invenzione senza ulteriori ritardi. Quando si chiede un brevetto non solo ce
l'abbiamo limitato nel tempo ma dobbiamo fornire una minuta descrizione di come
ottenere l'effetto del brevetto che consente ad una persona esperta sullo stato
dell'arte in teoria semplicemente leggendo il brevetto quello che l'inventore
riesce a fare.  La descrizione avviene sotto forma di rivendicazione che
descrivono cosa c'è di nuovo rispetto allo stato dell'arte.  Si esce
immediatamente dalla condizione sub-ottimale del
monopolio. Il brevetto deve garantire la massima ripetibilità.

### Slide 18

1. Concesso alla prima innovazione: Non alle successive perchè vogliamo
   incentivare solo il primo che ci arriva e che gli altri copino. Non vogliamo
   continuare a reinventare la ruota e non vogliamo ritardi nella divulgazione.
   Solo il primo avrà il brevetto, incentivando le persone a recarsi
   all'ufficio brevetti per depositare quel brevetto, nel timore di non essere
   i primi.
2. Non attribuisce un premio alla scoperta ma da soltanto il diritto ad
   esercitare un'esclusiva temporanea. Se poi quest'escluisiva sarà sufficiente
   ad incassare di più questo lo determinerà il mercato. Non si sta dando dei
   soldi all'inventore ma solo un diritto ad ottenere un privilegio. Se questo
   privilegio vale oppure no dipende dalla risposta del mercato. Questo
   semplifica il tutto perché non devo quantificare quanto è importante
   l'invenzione ma è il mercato a farlo. Vengono scoraggiate quindi le
   innovazioni inutili.
3. Il brevetto è concesso solo a condizione di fornire una spiegazione
   dettagliata dell'innovazione per minimizzare i tempi e costi di copia alla
   scadenza.
4. Non è gratuito ma bisogna pagare delle tasse annuali. Questo obbliga a
   riflettere sulla necessità del diritto ad esclusiva → verrà richiesto solo
   se effettivamente si ha un beneficio.
5. Obbligo di concessione ad un equo prezzo in determinati casi (i.e. Per un
   farmaco essenziale). Se si ritiene che l'invenzione sia un bene essenziale
   di pubblica utilità non voglio, anche se ti ho dato il diritto allo
   sfruttamento in esclusiva potrei anche doverti obbligare ad utilizzarlo
   perchè non voglio che tu impedisca agli altri l'utilizzo dell'invenzione e
   quindi di risolvere un problema essenziale. Il diritto in esclusiva di
   sfruttamento di un invenzione di pubblica utilità, se non utilizzato
   creerebbe dei danni notevoli alla società. I governi nazionali hanno
   normative che affermano il diritto dello Stato (marching right) ad imporrti
   di usarlo e se non lo usi posso dare delle licenze ad un equo prezzo ad
   altri che lo useranno al posto tuo. Tu sarai remunerato lo stesso attraverso
   delle licenze e stabilirà lo Stato l'equo prezzo che quindi obbligherà alla
   produzione.
6. è necessario che il titolare del brevetto richieda l'estensione in altri
   paesi, così da limitare queste estensioni in paesi in cui è inutile vi
   siano.

Q: Le limitazioni territoriali sono sulla produzione o sulla vendita?  A: è un
A: Dipende se si parla di brevetto sul prodotto o un processo?
1. i.e. Ho coperto la molecola che da il vaccino (non il modo di produrla)
   allora sullo stato in cui vige il brevetto solo chi detiene il diritto può
   produrlo e commercializzarlo (nessun altro può).
2. processo ma non sul prodotto finale: quello su cui ho l'esclusiva è la
   produzione in quella determinata maniera ma una volta prodotto questo non è
   protetto dal limitazione. Ciò significa che il processo produttivo è
   proprietario all'impresa che l'ha scoperto ma la commercializzazione no, nel
   caso in cui non si impongono limitazioni sul prodotto.

### Slide 19

Razionale della normativa brevettuale: perché i brevetti vengono dati per
periodi limitati, perché è difficile estenderli etc.  Bisogna contemperare
l'interesse del singolo ad avere dei proventi dalle sue attività inventive ma
anche della collettività a godere dei benefici di qualcosa che si è scoperto e
che può migliorare la condizione di tutti. La politica brevettuale → parte
dell'applicazione del diritto e dell'economia che sceglie come modulare la
normativa per ottenere il massimo beneficio possibile. Si occupa quindi di
quanto dovrebbe durare un brevetto (ad ora 20 anni per i paesi WTO). Ogni tanto
ci sono delle revisioni. La durata corretta dovrebbe essere un numero di anni
sufficienti per far si che l'inventore venga equamente compensato del suo
sforzo. È chiaro che se il prezzo che ottiene è molto elevato la durata
potrebbe essere più bassa; se il suo investimento di R&D è contenuto la durata
potrebbe essere più bassa etc.

### Slide 20

Qual è la durata ottima del brevetto? *Modello Nordhaus*: la durata ottima
dovrebbe essere minore per prodotti con domanda anelastica (quando il consumo
dipende poco dal prezzo); dovrebbe crescere al ridursi dell'ampiezza del
brevetto (più è ampia l'applicazione e più dovrebbe essere breve nel tempo);
crescere con il costo dell'innovazione (i.e. Se faccio un'innovazione in una
cosa che mi è costata molto dovrei avere una durata più lunga); dipende al
tasso di sconto (cresce o cala in base all'inflazione). Sarebbe quindi meglio
avere delle durate variabili ma nella pratica un'applicazione variata è troppo
complessa da gestire e di fatto si ha una durata standard per tutti di 20 anni
ma ci sono alcuni casi (brevetti per modelli d'utilità, per paesi che lo
consentono).

### Slide 21

Il brevetto è l'unico sistema per risolvere il problema economico \#1? No,
esistono altri metodi, ad esempio nella tradizione inglese si utilizzavano dei
premi in denaro forniti a coloro che erano in grado di risolvere dei problemi
di un determinato interesse. Questi funzionano quando è chiara a chi li mette
in palio l'utilità dell'invenzione. Il vincitore incassa e si comincia subito
ad utilizzarla senza preoccuparsi di diritti e contenziosi legali. In molti
casi però non sappiamo ciò che serve. Il sistema brevettuale fornisce la
neutralità necessaria, senza dover stabilire cosa è necessario fare ma lasci
gli inventori liberi. I premi esistono sia fatti da governi nazionali ma anche
su applicazioni di open innovation: kaggle.com etc: ci sono imprese che hanno
dei problemi tecnici e cercano soluzioni in cambio di un determinato premio.
Con questo sistema invii l'idea a chi di interesse senza farla vedere agli
altri e se poi venisse selezionata vi sarà una transazione tra impresa e
singolo ma non viene divulgata.

### Slide 22

Da dove viene la normativa brevettuale? Primo brevetto a Venezia nel 1474 e
quello che è interessante è che si dava la possibilità all'inventore di
sfruttare la propria invenzione ma doveva impegnarsi ad avere garzoni in
bottega che avrebbero necessariamente imparato il mestiere. Il brevetto non
valeva all'infuori della Repubblica di Venezia e veniva concesso con una
valutazione del merito: significa che non solo doveva essere nuovo il brevetto
ma addirittura doveva suscitare interessi da parte del senato della repubblica.
Molte delle concessioni venivano rilasciate a chi risolvevano dei problemi
militari legati alle navi e alla sicurezza sulle navi, oppure nella gestione
idraulica della navigazione

### Slide 23

1625 → primo statuto dei brevetti in Inghilterra. È una normativa più globale a
differenza di quella di Venezia che era più locale. Uno statuto che vale per
più persone. Il primo ufficio brevetti nasce poi nel 1852.  1883 → Prima
convenzione internazionale (Convenzione di Parigi) tra paesi tuttora in vigore
che ha stabilito delle regole comuni per stabilire cosa è brevettabile. Se il
paese aderisce alla convenzione non può più rubare un'idea da un paese e
importarla nel proprio come se fosse sua.

### Slide 25

I brevetti hanno una specifica caratteristica: il loro diritto sorge soltanto
nel momento in cui viene assegnato da un ufficio. Se sono un inventore ed ho
un'idea inventiva e la produco quest'idea non è automaticamente mia in termini
di diritti di proprietà. cio che ho è il diritto a rivendicare la paternità
dell'idea ma è più un diritto morale che non possiede conseguenze economiche.
Questi diritti economici sorgono solo a condizione di rispettare una serie di
requisiti che vanno verificati da un ufficio esterno durante una procedura di
esame che è teso proprio a verificare che vi siano dei requisiti a fronte dei
quali è possibile rilasciare questo diritto. se questi ci sono l'ufficio
rilascia il brevetto e nel momento id rilascio l'assegnarario del diritto di
sfruttamento commerciale dell'idea potra rivendicare i diritti di proprietà
piena su questo artefatto dell'ingegno, quindi nache i suoi diritti economici.
quali sono questi requisiti di brevettazione? Questi requisiti sono
sostanzialmente simili in tutti i paesi che aderiscono ai trattati
internazionali relativi ai brevetti, in particolare alla convenzione di Parigi.
Tutti i paesi che aderiscono al WTO e hanno ratificato la Convenzione di Parigi
hanno accettato di rispettare dei requisiti descritti nella suddetta
convenzione. Questa convenzione ha una descrizione che è stata fatta in tempi
risalenti quando la tecnologia era chiaramente diversa da quella che abbiamo
oggi ed è abbastanza vaga nella descrizione. La normativa internazionale
funziona in questo modo: Un paese ratifica una convenzione. questa non diventa
automaticamente testo di legge nel paese di destinazione ma il paese che
ratifica si impegna a creare una normativa sul suo territorio nazionale che
rispetta le linee guida fissate da questa normativa internazionale. Lo stato
nazionale è sovrano rispetto alla sua possibilità di legiferare però dovrà
essere stilata all'interno delle line guida che ha accettato di ratificare a
livello internazionale. Ne consegue che per grandi tratti la normativa di tutti
i vari paesi è conforme e si rifà sempre allo stesso testo: la convenzione
internazionale; ma ogni paese può avere delle leggi che modificano leggermente
o attuano questa normativa in modi parzialmente diversi già dal pinto di vista
normativo. oppure lanormativa di un paese deve essere interpretata: quando
nascono contenziosi che vengono poi portati in tribunale, c'è in giudice che
deve risolvere una controversia che si situa in una zona grigia della normativa
dov'è necessario per chi fa attuare la legge, il giudice, interpretare le
intezioni del legislatore. deve dare l'interpretazione di un testo che alla
fine è scritto in frasi che talvolta sono parzialmente ambigue o lasciano dei
margini di arbitrarietà. Quello che noi osserviamo sul territorio naizonale è
il frutto di una generale normativa internaizonale che si traduce poi in una
specifica normativa scritta nel linguaggio di ciascun paese da chi ha il potere
legislativo in ciascun paese. in ciascun paese viene attuata dal sistema
giuridico e quindi i giudici possono aver interpretato queste leggi in modi
specifici nei vari paesi. Quando parliamo dei requisiti di brevettazione
possiamo parlare di requisiti generali ma poi ogni signolo paese ha delle
interpretazioni e delle piccole differenze che dipendono da queste
caratteristiche elencate. Tendenzialmente vengono mostrate ora le linee guida
generali e poi in seguito si tratteranno alcuni dettagli della normativa
italiana e di quella internazionale. Verranno anche mostrate quali sono le
principali zone grigie dove le differenze tra un paese ed un altro sono più
evidenti.

La convenzione di parigi indica 3 requisiti di brevettazione indicati
espressamente come tali. Leggendo accuratamente se ne intravede però anche un
quarto descritto nelle pieghe di questa convenzione. In generale bisogna
ricordarsi che i requisiti principali sono 3, un quarto e eventualmente un
quinto (specialmente in base all'interpretazione che è stata data dalla
normativa intaliana). Se qualcuno chiede in prima battuta quanti sono i
requisiti per brevettare la risposta è sempre e solo 3.

1. **Invenzione**: è detto anche di industrialità. La convenzione di parigi non
   fornisce una definizione di invenzione e lo stesso fa la European Patent
   Convention. In realtà la convenzione dice cosa non è un'invenzione. In
   generale il requisito è quello di avere un carattere industriale: si intende
   essere un'attività atta a produrre un effetto di carattere industriale,
   cioè non una creazione artistica o una descrizione del mondo ma un effetto di
   trasformazione o di funzionalità di un oggetto su una serie di cose. Il
   requisito ci dice che l'invenzione è qualcosa che ha una caratteristica
   tecnologica nel senso dell'hardware. è un oggetto in grado di produrre un
   certo effetto desiderato. è quindi necessario che ci sia sempre uno scopo
   ben preciso e desiderato. non possiamo quindi brevettare ciò che è una
   semplice descrizione dello stato della natura che però è una descrizione e
   non è tesa a produrre un effetto desiderato. Questa convenzione di parigi
   dice che tutte le applicazioni di carattere non industriale:
   
   1. Le scoperte scientifiche non possono essere brevettate. La scoperta
      scientifica di per se spesso è una descrizione del mondo, quindi non ha
      un carattere di industrialità perchè non determina un'azione o un effetto
      desiderato. Quando Einstein descrive la teoria della relatività generale,
      di fatto quello che sta facendo è descrivere quello che crede sia il
      funzionamento del mondo del mondo della fisica universale. Questa
      descrizione non ha un carattere industriale perchè non fornisce delle
      indicazioni su quello cosa ci posso fare io con questa conoscenza.
      L'industrialità dice in che modo posso organizzare una conoscenza o una
      tecnologia per produrre ciò che desidero. quindi di per se non si
      potrebbe brevettare la teoria della relatività poiché si tratta della
      descrizione del mondo.  Ciò nondimeno prendendo gli algoritmi GPS che
      consentono di mappare la posizione: questi comunicano con un satellite
      che restituisce poi un segnale. Questi algoritmi incorporano le leggi
      della relatività generale di Einstein e questi algoritmi, nei paesi in
      cui gli algoritmi sono brevettabili, sarebbero brevettabili perchè non
      sono una descrizione del mondo ma un modo per descrivere un fine -->
      essere in grado di muoversi sulla siperficie terrestre cominicando con un
      satellite che è nello spazio e riuscendo ad effettuare le trasmissioni.
      Viceversa la distanza tra la terra e il satellite è tale per cui se non
      venissero iutilizzate le leggi della relatività generale questo satellite
      manderebbe un segnale che non potrebbe essere captato erchè la posizione
      sulla terra sarebbe cambiata. Un algoritmo di quel tipo deve incorporare
      quelle leggi. *Se si parla di un'invenzione che incorpora delle leggi di
      Natura per ottenere un fine che desidera allora quello è brevettabile
      perché ha un carattere industriale, teso ad ottenere un fine ed uno
      scopo*. 
   2. I metodi matematici, inoltre, non sono brevettabili poichè sono una forma
      di descrizione del mondo oppure un modo di rendere in forma matematica
      leggi ed assiomi. Algoritmi software qui sono citati perché questa è la
      convenzione dello European Patent Office. In realtà la convenzione di
      parigi non parla di software perchè, ovviamente è stata stilata nel 1800.
      è una zona grigia, in quanto alcuni considerano gli algoritmi software,
      in quanto di fatto traducibili in codice binario, una rappresentazione
      della realtà similmente a quella riportata dagli algoritmi matematici,
      quindi non brevettabili perché manchevoli del requisito dell'invazione.
      Altri, appellandosi alla definizione di industrialità, affermano che un
      algoritmo software è qualcosa che vuole produrre un effetto quindi, anche
      se si parla di un metodo matematico, questo dovrebbe essere tutelato.
      L'Europa esclude gli algoritmi software dal brevettabile mentre gli USA
      li include per le ragioni sopra indicate. In particolare presso EPO è
      possibile ottenere un brevetto per un algoritmo quando questo è
      incorporato all'interno di un Hardware di una macchina (i.e. avendo un
      circuito elettronico che contiene una scheda che contiene una parte di un
      algoritmo, l'insieme di HW e SW è brevettabile ma solo il SW no).
   
   Q: Se brevettassi insieme all'HW poi l'algoritmo SW non lo posso più
   utilizzare al di fuori di quell'HW?

   A: Si può coprire SW con brevetto solo quando è insieme a HW. Se scorporo il
   SW dall'HW la protezione rimane imperfetta perchè era pensato diversamente.
   il fatto che siano manchevoli del requisito dell'invenzione e che l'europa
   escluda la brevettazione del software non implica che il software non è
   protetto. La gran parte delle cose che non rientrano nel requisito
   dell'invenzione in realtà è proteggivile con copyright. In Europa non si da
   il brevetto ma il copyright, il diritto d'autore, per il software. Il
   copyright è sempre azionabile anche senza avere depositato.
   
   1. Creazioni artistiche: fotografie, arti applicate, letteratura, etc. Una zona grigia però è quella che vede i design degli oggetti. Una sedia ergonomica, o con un design la cui funzionalità è quella di essere bella esteticamente, ha un design che può essere brevettato, anche il copyright è consentito.
   2. Metodi di trattamento chirurgico e terapie: le procedure che un chirurgo
      fa per eseguire un intervento chirurgico. Il protocollo chirurgico e
      terapeutico non è brevettabile.
   3. Specie e varietà di piante ed animali: è sempre a partire da una
      variazione di natura e queste specie di fatto sono sempre presenti in
      natura. Il selezionatore non è un inventore in questo caso. Questo
      requisito si applica solo ai cosiddetti animali "superiori" ossia non
      micro-organismi. Questi ultimi sono brevettabili, quindi se ho un
      batterio che utilizzo in un processo chimico per trasformare una sostana
      in altro questo è brevettabile come processo chimico per ottenere un
      certo risultato. Zona grigia #2: l'ingegneria genetica --> non solo siamo
      in grado di modificare micro-organismi ma anche animali superiori, ad
      esempio i mammiferi e anche qui la normativa è entrata sotto stress.
      Questi animali OGM rientrano nell'esclusione dell'articolo 5 oppure si
      devono considerare l'opera dell'ingegno dell'uomo perchè la natura non
      sarebbe stata in grado di generare da sola questi? Un esempio di
      organismi geneticamente modificati è il "roundup ready" (sementi
      modificate per essere resistenti ad un certo erbicida) di Monsanto e
      l'oncomouse (topi con caratteristiche genetiche predisposte al cancro).
      Primo tema: una persona quindi può appropriarsi di altri esseri viventi?
      si può fare solo se le speci sono inferiori ossia microorganismi, senza
      diritti. Secondo tema: considero appropriabile una cosa che io ho
      direttamente contribuito a produrre, ossia un animale o specie
      geneticamente modificata --> c'è un opera inventiva. L'individuo
      interviene direttamente operando un'azione inventiva. Si pongono però dei
      problemi di liceità? 1) è lecito rivendicare diritti di proprietà su
      organismi viventi 2) Questi organismi viventi potrebbero avere dei
      diritti che limitano i loro uso. Se io induco il cancro in una cavia di
      topo di ratto sto modificando geneticamente un essere vivente per indurre
      sofferenza, quindi è legittima una cosa che induce sofferenza? se si
      trattasse di un umano? la tortura di un essere umano non sarebbe
      brevettabile perché manca del requisito di liceità. Se si tratta di un
      animale l'interpretazione è più dubbia. Questa sofferenza all'animale
      viene inferta per un motivo che in verità è anche meritevole, ossia
      trovare trattamenti per curare effettivamente delle persone.
      **Interpretazione del legislatore**: il motivo della sofferenza
      dell'animale superiore deve essere giustificato da un motivo più che
      buono. il motivo deve più che compensare la sofferenza dell'animale.

Negli US è consentita anche la brevettazione di:
- materiale originale: materiale che portiamo direttamente sottoforma di campione all'ufficio e lo brevettiamo portandone un campione. 
- modelli di business: descrizione di processi (di carattere immateriale) atti a produrre uno scopo, per semempio il 1-click-buy di Amazon. In Europa non hanno il requisito dell'invenzione, quindi non possono essere brevettati.

2. **Novità**: Il criterio più utilizzato per scartare i brevetti. Ciò che non
   era noto e non era stato divulgato al momento del deposito. Non era noto ad
   una persona esperta sullo stato dell'arte. Se le persone esperte non lo
   considerano noto allora il requisito della novità è soddisfatta. La
   divulgazione rende la cosa nota, quindi invalida la novità.  questo
   requisito è stato applicato in modi diversi nel mondo (esiste il grace
   period negli Stati Uniti: l'autore può ancora ravvedersi dall'aver divulgato
   la sua invenzione e può ancora ottenere il brevetto. questo per i 12 mesi).
   Questa divulgazione che invalida la novità non deve essere per forza in
   forma scritta ma può essere fatta in forma orale (ad un convegno ad un
   pubblico) o sotto forma di un prototipo ad una fiera di settore. la
   caratteristica di novità introduce il concetto della priorità è una data a
   cui si può fare riferimento per dire che una certa cosa era nota ad una
   certa persona. Se depositassi oggi un brevetto e qualcuno di noi va in
   un'altra sede brevettuale WTO e deposita qualcosa di analogo in modo
   assolutamente indipendente ad un certo punto quando emergerà l'esistenza di
   due cose uguali si vuole stabilire la priorità, confrontando la data del
   deposito, dandola a quella anteriore. Per i sistemi con il grace period
   questa priorità si può far retrodatare al momento in cui l'inventore ha
   iniziato a divulgare una certa cosa. 
   
   *Eccezioni (valide anche per paesi senza grace period)*:

	2. Furto d'idea: l'azienda che ha subito il furto, a suo carico lo
	   denuncia, avendo prove che affermano la colpevolezza di (ex)
	   collaboratori e in tal caso il brevetto rubato viene invalidato
	   perché manchevole del requisito di novità.
	3. Particolari fiere di settore in cui è possibile divulgare senza
	   perdere il requisito di novità.
	   
3. Originalità (o inventive step o non banalità): mentre la novità dice che la
   cosa deve essere nuova di per sè, nell'originalità quello che può essere
   nuovo è la combinazione di due cose esistenti --> è non banale una cosa che
   combina due pezi esistenti ma in forma nuova. Facciamo un esempio: telefono
   flap in cui lo schermo all'apertura non era uno schermo ma uno specchio che
   rifletteva l'immagine. alla pressione di un tasto lo schermo sarebbe
   diventato uno schermo vero e proprio. In questo caso l'invenzione è la
   combinazione delle due cose già esistenti (telefono flap + specchio). La
   combinazione era così banale o ha richiesto un elemento di originalità tale
   per cui vogliamo dare all'inventore una tutela. La novità e l'originalità
   sono utilizzate anche in ambito del copyright. Nel copyright è dominante il
   criterio di originalità, il quale predomina su quello di novità in quanto
   nell'ambito copyright è difficile avere originalità assoluta.

Gli altri parziali requisiti sono:

4. **Liceità** in termini etici e morali: Il primo tra i requisiti minori. Non è
   possibile brevettare qualcosa che sia contrario alla morale e al buon
   costume. Sono due concetti astratti che vanno calati nella specifica società
   di riferimento e nel momento storico in corso. Non è raro che questi
   requisiti si modifichino nel tempo (oggi è immorale e un domani è
   accettata). è possibile rigettare perché un'invenzione non etica e molto
   spesso tanto gli esaminatori che la corte di giustizia si trovano in
   difficoltà di interpretare nello specifico questa legge per stabilire che
   cosa è lecito e cosa no e ciò pone considerazioni di etica legate alla
   tecnologia. Eugenetica --> selezione di alcuni tratti perché li considero
   migliori di altri.
5. **Esaustività nella descrizione** del brevetto (La sufficiente descrizione):
   bisogna descrivere il contenuto del brevetto minuziosamente, tale da poter
   garantire la replicabilità da parte di esperti di settore. è un criterio
   soggetto ad interpretazione perchè è ovvio che è difficile dterminare cosa
   costituisce una sufficiente descrizione. è difficile però completamente
   rigettare un brevetto per non sufficiente descrizione. Tipicamente si
   verificano solo dei ritardi. Non è obbligatorio però spiegare la scienza
   dietro il brevetto, l'importante è fornire quella descrizione minuziosa dei
   passaggi che portano al risultato. Non è strettamente richiesto al brevetto
   di essere scientificamente valido, così come non viene effettuata un'analisi
   scientifica del contenuto del brevetto da parte dell'esaminatore.

