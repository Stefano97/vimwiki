# Lezione 2

## Come sorgono i diritti brevettuali

Il diritto di sfruttamento economico in esclusiva nasce sempre e solo al
rilascio del brevetto.  Fino a quel momento inventore e assegnatario hanno solo
i "diritti in formazione". e dal momento:
- Dal momento dell'invenzione → diritto a depositare il brevetto
- Dal momento della priorità → diritto a rivendicare la novità. Non è così
  ovvio: se depositassi un brevetto in cui descrivo una scoperta. il documento
  mi da la priorità a oggi di saper fare una cosa. Decido poi però di non
  continuare con l'esame perche magari la tecnologia non sarebbe
  commercializzabile oppure che avrei avuto un'idea ancora migliore etc...
  quindi ci chiedono la fee per continuare a fare l'esame e io non la pago,
  lasciando decadere la mia domanda. questa domanda di brevetto comunque verrà
  pubblicata tra 18 mesi anche se non ho dato seguito al brevetto e da quel
  momento in poi comunque di fatto invaliderà qualunque novità di eventuali
  successivi brevettatori → il fatto di aver depositato di quest'idea mi da il
  diritto a rivendicare la priorità anche se poi non posso più ottenere il
  brevetto perché l'ho lasciato decadere ma in questo modo ho evitato che altri
  brevettino. posso anche decidere strategicamente di fare un'application
  brevettuale e di lasciarla decadere quando intendiamo liberare un'invenzione
  ma vogliamo essere sicuri che nessun altro la brevetterà a seguire. quindi
  per evitare che qualcuno mi impedisca di utilizzarla liberamente faccio ciò.
- 18 mesi dopo il deposito, quando l'ufficio brevettuale pubblica agli altri:
  sorge il diritto di chiedere il risarcimento a chi continua ad usare il
  brevetto.
- Dal momento del rilascio del brevetto: ho il pieno diritto a rivendicare la
  sua esclusiva per tempo limitato e nei paesi in qui è esteso e limitamente ai
  claim all'interno del suo brevetto.

Il diritto naturale: il padre o la madre di un invenzione sorge al momento
della creazione e rimane sempre all'infinito.

Per il copyright il diritto sorge nell'istante preciso della creazione, quando
sorge il diritto morale del ricordare l'autore. Nel caso del software è
comunque possibile depositare l'oggetto per cui si chiede il diritto d'autore
ma questo deposito non è fatto al fine di ottenere il diritto, questo c'è già.
il deposito viene fatto per rendere facile provare l'esistenza del mio diritto.
Se ho depositato una canzone che ho scritto chiunque copi questa canzone può
essere immediatamente sanzionato perché ho una data certa a cui posso
dimostrare che quella canzone era mia. Se io non depositassi la canzone e
qualcuno la copiasse io posso portare in tribunale chi mi sta copiando ma avrò
l'onere di provare che io ho scritto la canzone precedentemente.

---

I diritti conferiti dal brevetto sorgono durante la procedura nell'ordine della
precedente slide e sono sempre in movimento durante il tempo perchè durante la
procedura si accrescono fino ad arrivare al momento del rilascio al diritto.
Anche dopo che il rilascio è avvenuto questi diritti possono essere in
movimento per tante ragioni:
1. potrei aver protetto alcuni paesi ma poi potrei decidere di far cadere il
   brevetto in quei paesi. Il diritto quindi decade in quel paese e tutti
   possono utilizzare il brevetto in modo libero
2. Per tutte le azioni che potrebbero sorgere in tribunale: contenziosi legali
   tra concorrenti a causa di copie di brevetti. Il concorrente in tribunale
   potrebbe affermare che il brevetto esistente potrebbe essere manchevole di
   un determinato diritto, magari quello di novità perché lo stava già
   utilizzando. Se il concorrente quindi riesce a dimostrare questo punto e il
   giudice gli dà ragione il brevetto viene revocato. E mi metterà anche nella
   condizione in cui va a modificare anche ex-post i diritti che io credevo di
   aver maturato.

## Procedura dell'EPO

Application → giorno del deposito. Può avvenire online o in una delle sedi
della camera di commercio in ogni provincia. Pago la fee del deposito e abbiamo
una data di filing e di priorità che di solito coincidono o possono avere uno
scarto di qualche giorno in cui l'ufficio verifica i pagamenti e che i
documenti siano validi.

Dal momento dell'application inizia un periodo in cui il brevetto è segreto.
L'autore può divulgarlo oppure no. In questo periodo di segretezza l'inventore
può pagare una seconda fee per poter sottoporre il brevetto all'esame. Se ad
esempio un azienda in Italia volesse brevettare qualcosa e volesse far
esaminare all'EPO, questo brevetto sarebbe inviato in prima battuta all'ufficio
dell'Aia in Olanda, dove si svolge il primo esame. è un esame non binding
(vengono solo segnalati i problemi che il brevetto può avere): un esaminatore
apre il brevetto e fornisce una prima opinione sul fatto che il brevetto
rispetti i requisiti visti alla lezione precedente. Esaminatore scrive il
search report (1-2 pagine) in cui si segnalano dei documenti che potrebbero
pregiudicare la novità o l'inventive step. Alcune volte in questo documento si
riportano dei documenti che l'esaminatore richiede vengano citati
dall'inventore per rendere la descrizione più completa. questo documento viene
poi spedito all'applicant. Questo viene fatto di solito entro i primi 18 mesi,
quindi quando non è ancora stato pubblicata l'application. Se così avvenisse
allo scadere dei 18 mesi verrebbe pubblicata sia l'application sia il search
report. Altrimenti prima viene pubblicato l'application in prima battuta e,
alla disponibilità del search report, questo viene ripubblicato con l'aggiunta.

Al momento del deposito dell'application l'inventore ha 12 mesi di priorità per
estendere ad altri paesi. Se venisse fatto in questi paesi viene fatto un altro
deposito in questi altri paesi con altri codici brevettuali (jp, kr, us ...)
che si rifà all'application italiana ma che di fatto è un altra domanda di
brevetto. e partono i vari esami per questi altri paesi ma con un anno di
ritardo.

Espletato il primo esame l'applicant (un altro modo per dire inventore) può
chiedere di effettuare l'esame vero e proprio pagando un'altra fee. Riformulerà
la domanda modificando sulla base del search report ricevuto dal primo ufficio.
Si cerca di rendere il documento più solido possibile. Il secondo esame viene
fatto a Monaco. l'esaminatore è diverso dal primo e non ha l'obbligo di
pensarla esattamente come il primo. La sua opinione è binding, cioè vincolante:
scrive una lettera indicando se il brevetto è manchevole di qualche requisito e
che ha intenzione di rifiutarlo a meno che non venga refinito in modi acconci.

Se la questione va avanti il secondo esaminatore può decidere di accettare o
revocare il brevetto. Nel momento del rilascio si apre un periodo di 3 mesi nel
quale terze parti possono decidere di fare opposizioni (questa cosa esiste solo
in Europa): un concorrente utilizza una tecnologia simile e ritiene di non
averla potuta brevettare perchè non la ritiene nuova, quindi quando nota che
qualcuno la sta brevettando fa opposizione perchè eccepisce che alla luce di
alcuni documenti c'è un errore e non dovrebbe essere considerata come nuova.

Se nessuna domanda di opposizione fine, altrimenti si apre la procedura di
opposizione e ci sarà un terzo esaminatore sempre a Monaco (diverso dai primi
2) e valuta se alla luce di quello che è emerso decide ancora di confermare il
brevetto oppure di revocarlo. spesso viene revocato o modificato.

Se il brevetto è stato negato o revocato dopo l'opposizione l'applicant può
addirittura ricorrere in appello → un ulteriore esaminatore diverso dai
precedenti riesamina il caso e decide (ultima possibilità).

Al termine, l'esame sarà vincolante. quindi se un brevetto esiste viene
pubblicato nuovamente ma come brevetto rilasciato, non come domanda di brevetto
e da quel momento in poi i diritti sono perfetti.

Gli altri esami vengono fatti negli altri paesi e quando lo accettereanno lo
pubblicheranno con una data. nel caso rifiutassero non c'è una pubblicazione di
rigetto ma una notizia nello status del brevetto.
