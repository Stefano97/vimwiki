# Robotic Rehab

# Randomized controlled trials

## RCT (pag. 6)
L'allocazione randomica ha da considerarsi in quanto eliminerà le differenze tra i gruppi, consentendo di stabilire l'effetto del trattamento non contaminato da altri fattori potenzialmente concorrenti

## The need of statistical approach

Si ottengono generalmente diversi risultati considerando paziente dopo paziente, nonostante si consideri una popolazione omogenea.
il _Primo passo_ è quello di definire la popolazione target e, soprattutto gli outcome da misurare.
Su queste basi è possibile determinare la numerosità degli individui e le primary outcome measure.
Sono stati proposti algoritmi specifici per definire la dimensione del campione in base alla Primary outcome measure impostata, e la sua variabilità, o minima differenza clinica, e il potere mirato di evidenza.
L'RCT poi analizza il comportamento medio della popolazione target e ne trae delle conclusioni solide.

## Current evidence from clinical trials

Ci sono molti vantaggi nell'utilizzo della riabilitazione mediante robot.
L'evidenza clinica però non è ancora del tutto chiara.

### RATULS studies (Robot-Assisted Training for the Upper Limb after Stroke)
- Considerando gli end-effector: è stata effettuata una comparazione tra la terapia convenzionale e l'insieme di robotic training e terapia convenzionale a partire da soggetti in condizione di hictus subacuto: Il *secondo approccio* si è rivelato essere il più efficace. Ciò significa un impatto positivo degli end-effector devices applicati agli arti superiori in queste condizioni patologiche, pertanto consigliato.
- Pazienti cronici:
	- Nel caso di pazienti in condizioni di infarto "cronico" la terapia con robot e quella convenzionale hanno mostrato, nelle prime 12 settimane, un comportamento *simile*. andando però a studiare una finestra temporale più allargata (dopo altre 24 settimane) i miglioramenti ottenuti mediante *terapia robotica* sono aumentati.
	- Confronto tra *high-intensity* e *low-intensity* robot-assisted training (con rispettivo gruppo di controllo). Un maggior miglioramento è riscontrabile nella tipologia *high-intensity* → l'intensità è un parametro fondamentale nell'esercizio dell'arto superiore.
NB: Un effetto nelle ADL (activity of daily living) è riscontrato solamente nel caso di pazienti con infarto subacuto.
- Uno studio effettuato considerando MIT-manus su pazienti post-infarto in condizioni croniche ha dimostrato miglioramenti per quanto riguarda l'impairment dell'arto superiore ma questi non sono presenti per quanto riguarda le funzioni e ADL (confronto effettuato con terapia usuale).
- EULT (Enhanced upper-limb therapy) in cui il training è orientato alle funzioni motorie e alle attività quotidiane ha dimostrato un miglioramento se comparato alle cure usuali alla fine del periodo di intervento (a 3 mesi).

### Exoskeleton for upper limb recovery
- Pazienti allo stage di infarto cronico. Uno studio riporta degli effetti migliorativi riguardanti la terapia robotica nei confronti della spasticità(se paragonati alla terapia usuale). In contrasto però le ADL sono migliorate di più nei soggetti sottoposti a trattamento usuale.
- Pazienti allo stage subacuto di infarto: non sono riportati studi in letteratura.
Queste considerazioni non sono sufficienti per dimostrare l'efficacia del metodo riabilitativo che impiega l'esoscheletro nel trattamento dei pazienti post infartuati.
Un articolo propone una consolidato punto di vista sull'utilizzo della terapia robotica orientata al trattamento dell'impairment combinato alla terapia convenzionale orientata al riottenimento delle funzioni motorie compromesse.

? Differenza tra effectiveness, efficacy ed efficiency.
