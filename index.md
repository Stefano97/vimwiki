# Courses

| Classes                                                             | CFU | Prof(f).              | Revise              | Follow |
| ---                                                                 | --- | ---                   | ---                 | ---    |
| [Advanced Signal Processing](Advanced/index.md)                     | 10  | Cerutti, Barbieri R.  | 2-3-4 (C)           |        |
| [Brevetti](Brevetti/index.md)                                       | 5   | Franzoni, Barbieri M. |                     | 3-4-5  |
| [MBICAS](MBICAS/index.md)                                           | 10  | Baselli, Baroni       |                     |        |
| [BSP+MI](BSPMI/index.md)                                            | 10  | Signorini             |                     |        |
| [Elettronica Biomedica](Elettronica\ Biomedica/index.md)            | 10  | Dellacà               |                     |        |
| [Motorio](Motorio/index.md)                                         | 5   | Frigo                 |                     |        |
| [Neuroengineering (Pedrocchi)](Neuroengineering/Pedrocchi/index.md) | 10  | Pedrocchi             |                     |        |
